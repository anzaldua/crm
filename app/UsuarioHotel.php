<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioHotel extends Model
{
       protected $table='usuarios_hotel';

    protected $fillable = [

     
      'nombre'           =>'required',
      'paterno'          =>'required',
      'materno'          =>'required',
      'email'            =>'required',
      'confirmacorreo'   =>'required',
      'telefonoficina'   =>'required',
      'ext'              =>'required',
      'celular'          =>'required',
      'hotel'            =>'required',
      'id_perfil'        =>'required',
      'departamento'     =>'required',
      'puesto'           =>'required',        
      'status'           =>'required',
      'usuario'          =>'required',
      ];
}
