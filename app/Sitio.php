<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sitio extends Model
{
    protected $table='usuarios';

    protected $fillable = [

     
      'nombre',
      'paterno',
      'materno',
      'usuario',
      'password',
      'fechanacimiento',
      'departamento',
      'puesto',
      'perfil',
      'email',
      'id:hote',
      'tipo',
      'id_categoria',
      'status',
      'id_corporativo',
    

    ];
}
