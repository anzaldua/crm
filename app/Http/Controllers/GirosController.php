<?php

namespace App\Http\Controllers;
use App\Http\Controllers\PermisosController;
use Illuminate\Http\Request;
use App\Giro;
use App\Segmento;
use DB;

class GirosController extends Controller
{
  public function index()
     {      $giros=Giro::all();
            $permisos = new PermisosController();
            $rutas    = $permisos->rutas(); 
            return view('Giro.index',['giros'=>$giros,"rutas"=>$rutas]);
    }

     public function Tablagiros(Request $request)
    {
        $giros = Giro::all(); 
       return view ('Giro.Tablagiros',compact('giros'));

    }

    public function Agregargiros(Request $request)
    
    {
     $segmentos = Segmento::all();                   
     $giros =Giro::find($request);
    return view('Giro.agregargiros',['giros'=>$giros,"segmentos"=>$segmentos]);
        
    }

    public function Guardaractualizar(Request $request)
        {
            //return $request->all();
      
  
            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
            
            return ($retornar)? "success" : "error";
                    
        }

          private function Agregar($data){


        $affected = DB::table('giros')->insert([

            'nombre'=>$data['nombre'],
            'segmento_id'=>$data['segmento_id'],
         
       
                
                
               
            ]);

            return $affected;
         }

    
        public function Actualizar($data,$id){  
    
          
            $affected = DB::table('giros')->where('id', '=', $id)->update([
                    'nombre'=>$data['nombre'],
                    'segmento_id'=>$data['segmento_id'],
                
                        

                
        ]);

        return $affected;



        }

}
