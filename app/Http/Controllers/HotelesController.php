<?php

namespace App\Http\Controllers;
use  Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\HotelRequest;
use App\Hotel;
use DB;



    class Hotelescontroller extends Controller
    


{
       
    public function index()
            {
                $hoteles =Hotel::all();
                return view('hoteles.index',compact('hoteles'));
            }


            //Metodo de tablaHoteles
            public function tablaHoteles( Request $request)
            {
                $hoteles=Hotel::all(); 
                return view('hoteles.tablaHoteles',compact('hoteles'));
             }

            // Metodo de agregarHoteles
            public function agregarHoteles(Request $request)
            {

            //$id = $request->get("id");

        //$hoteles=Hotel::where($id); 
            $hoteles=Hotel::find($request);
            return view('hoteles.agregarHoteles',['hoteles'=>$hoteles]);
        }

    public function guardarActualizar(HotelRequest $request)
        {
            //return $request->all();
      
  
            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
            
            return ($retornar)? "success" : "error";
            

        
        }

        private function agregar($data){

    //         $arreglo = array();
    //         foreach($data as $k=>$v) $arreglo[$k] = $v;
    //    //Hotel::create($arreglo);
     

         $affected = DB::table('hotel')->insert([
                'nombrehotel'=>$data['nombrehotel'],
                'codigohotel'=>$data['codigohotel'],
                'direccioncomercial'=>$data['direccioncomercial'],
                'codigohotel'=>$data['codigohotel'],
                'colonia'=>$data['colonia'],
                'cp'=>$data['cp'],
                'idciudad'=> $data['idciudad'],
                'idestado'=> $data['idestado'],
                'idpais'=>$data['idpais'],
                'razonsocial'=>$data['razonsocial'],
                'calleynumero'=>$data['calleynumero'],
                'idciudadfiscal'=>$data['idciudadfiscal'],
                'idestadofiscal'=>$data['idestadofiscal'],
                'idpaisfiscal'=>$data['idpaisfiscal'],
                'telefonodirecto'=>$data['telefonodirecto'],
                'telefono2'=>$data['telefono2'],
                'fax'=>$data['fax'],
                'web'=>$data['web'],
                'idmarca'=>$data['idmarca'],
                'idgrupohotelero'=>$data['idgrupohotelero'],
                'logohotel'=>$data['logohotel'],
                'principalcontacto'=>$data['principalcontacto'],
                'idpuesto'=>$data['idpuesto'],
                'notas'=>$data['notas'],
                'nodehabitaciones'=>$data['nodehabitaciones'],
                'impuesto'=>$data['impuesto'],
                'ish'=>$data['ish'],
                'status'=>$data['status'],
                'coloniafiscal'=>$data['coloniafiscal'],
                'rfc'=>$data['rfc'],
                'latitud'=>$data['latitud'],
                'longitud'=>$data['longitud'],
                'zoom'=>$data['zoom'],
                'logoconvenios'=>$data['logoconvenios'],
                'idcorporativo'=>$data['idcorporativo'],
                'incContrato'=>$data['incContrato'],
                'finContrato'=>$data['finContrato'],
                'registrado'=>$data['registrado']
            ]);

            return $affected;
         }




    
        public function actualizar($data,$id){  
              
            //$affected = DB::table('hotel')->where("id",$id)->update([
          


            $affected = DB::table('hotel')->where('id', '=', $id)->update([
                'nombrehotel'=>$data['nombrehotel'],
                'codigohotel'=>$data['codigohotel'],
                'direccioncomercial'=>$data['direccioncomercial'],
                'colonia'=>$data['colonia'],
                'cp'=>$data['cp'],
                'idciudad'=> $data['idciudad'],
                'idestado'=> $data['idestado'],
                'idpais'=>$data['idpais'],
                'razonsocial'=>$data['razonsocial'],
                'calleynumero'=>$data['calleynumero'],
                'idciudadfiscal'=>$data['idciudadfiscal'],
                'idestadofiscal'=>$data['idestadofiscal'],
                'idpaisfiscal'=>$data['idpaisfiscal'],
                'telefonodirecto'=>$data['telefonodirecto'],
                'telefono2'=>$data['telefono2'],
                'fax'=>$data['fax'],
                'web'=>$data['web'],
                'idmarca'=>$data['idmarca'],
                'idgrupohotelero'=>$data['idgrupohotelero'],
                'logohotel'=>$data['logotel'],
                'principalcontacto'=>$data['principalcontacto'],
                'idpuesto'=>$data['idpuesto'],
                'notas'=>$data['notas'],
                'nodehabitaciones'=>$data['nodehabitaciones'],
                'impuesto'=>$data['impuesto'],
                'ish'=>$data['ish'],
                'status'=>$data['status'],
                'coloniafiscal'=>$data['coloniafiscal'],
                'rfc'=>$data['rfc'],
                'latitud'=>$data['latitud'],
                'longitud'=>$data['longitud'],
                'zoom'=>$data['zoom'],
                'logoconvenios'=>$data['logoconvenios'],
                'idcorporativo'=>$data['idcorporativo'],
                'incContrato'=>$data['incContrato'],
                'finContrato'=>$data['finContrato'],
                'registrado'=>$data['registrado'], 

                
        ]);

        return $affected;



        }

   
    public function destroy($id)
        {
            $hotel = Hotel::find($id);
            $hotel->delete();
            return redirect('/hoteles')->with('success', 'Stock has been deleted Successfully');
        }
    //metedo de verusuarios para verlos en un modal
    public function verUsuarios(){

        return view('hoteles.modal');
    } 
       //metodo de guardar y actualizar de cotizaciones
      public function guardarActualizarCotizacion(Request $request)
        {
            //return $request->all();
      
  
            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
            
            return ($retornar)? "success" : "error";
            

        
        }  


    public function actualizarCotizacion($data ,$id){

        $affected=BD::table('hotel')->insert([

      'incContrato'=>$data['incContrato'],
      'plan'=>$data['plan'],
      'finContrato'=>$data['finContrato'],
      'costoxmes'=>$data['costoxmes'],
      'descuento'=>$data['descuento'],
      'total'=>$data['total'],
      'apertura'=>$data['apertura'],
      'mantenimiento'=>$data['mantenimiento'],
      'costo'=>$data['costo'],
      'descuento'=>$data['descuento'],
      'costo'=>$data['descuento'],
      'total'=>$data['total'],


        ]);

       return $affected;

    } 
}
