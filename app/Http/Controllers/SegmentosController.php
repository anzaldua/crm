<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\PermisosController;
use App\Segmento;
use DB;

class SegmentosController extends Controller
{
   
    public function index()
     {      $segmentos=Segmento::all();
            $permisos = new PermisosController();
            $rutas    = $permisos->rutas(); 
            return view('segmentos.index',['segmentos'=>$segmentos,"rutas"=>$rutas]);
    }

     public function Tablasegmentacion(Request $request)
    {
        $segmentos = Segmento::all(); 
       return view ('segmentos.Tablasegmentos',compact('segmentos'));

    }

    public function Agregarsegmentacion(Request $request)
    
    {

            $segmentos=Segmento::find($request);
            $segmentos = Segmento::all();
        return view('segmentos.agregarsegmentos',compact('segmentos'));
        
    }

    public function Guardaractualizar(Request $request)
        {
            //return $request->all();
      
  
            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
            
            return ($retornar)? "success" : "error";
                    
        }

          private function Agregar($data){


        $affected = DB::table('segmentos')->insert([

            
                'nombresegmento'          =>$data['nombresegmento' ],    
                
                
               
            ]);

            return $affected;
         }

    
        public function Actualizar($data,$id){  
    
          
            $affected = DB::table('segmentos')->where('id', '=', $id)->update([
                'nombresegmento'          =>$data['nombresegmento'],            
                
                        

                
        ]);

        return $affected;



        }

   
 
}
