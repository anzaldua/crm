<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\HotelRequest;
use Illuminate\Foundation\Http\FormRequest;
use Yajra\DataTables\Facades\DataTables;
use App\UsuarioHotel;
use DB;



    class UsuariosHotelcontroller extends Controller
    
        {
               public function index()
        {
            $hotelusuarios=UsuarioHotel::all();
            return view('usuarioshotel.index',compact('hotelusuarios'));
        }

        //Metodo de tablaHoteles
        public function Tablausuarioshotel( Request $request)
        {
            $hotelusuarios = UsuarioHotel::all(); 
            return view('usuarioshotel.Tablausuarioshotel',compact('hotelusuarios'));
        }

            //Metodo de agregarHoteles
            public function Agregarusuarios(Request $request)
        
            {

                //$id = $request->get("id");

                //$hoteles=Hotel::where($id); 
            $hotelusuarios=UsuarioHotel::find($request);
            $hotelusuarios = UsuarioHotel::all();
            return view('usuarioshotel.agregarusuarioshotel',['hotelusuarios'=>$hotelusuarios] ,compact('hotelusuarios'));
        }

        public function Guardaractualizar(Request $request)
        {
            //return $request->all();
      
  
            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
            
            return ($retornar)? "success" : "error";
            

        
        }

        private function Agregar($data){

        //     $arreglo = array();

        //         foreach($data as $k=>$v) $arreglo[$k] = $v;

        //          //Hotel::create($arreglo);
     

        $affected = DB::table('usuarios_hotel')->insert([

            
                'nombre'          =>$data['nombre'],
                'paterno'         =>$data['paterno'],
                'materno'         =>$data['materno'],
                'email'           =>$data['email'],
                'confirmacorreo'  =>$data['confirmacorreo'],
                'telefonoficina'  =>$data['telefonoficina'],
                'ext'             =>$data['ext'],
                'celular'         =>$data['celular'],
                'hotel'           =>$data['hotel'],
                'id_perfil'       =>$data['id_perfil'],
                'departamento'    =>$data['departamento'],
                'puesto'          =>$data['puesto'],
                'status'          =>$data['status'],
                'usuario'         =>$data['usuario'],
            
               
            ]);

            return $affected;
         }

    
        public function Actualizar($data,$id){  
              
            //$affected = DB::table('hotel')->where("id",$id)->update([
          
            $affected = DB::table('usuarios_hotel')->where('id', '=', $id)->update([
                'nombre'          =>$data['nombre'],
                'paterno'         =>$data['paterno'],
                'materno'         =>$data['materno'],
                'email'           =>$data['email'],
                'confirmacorreo'  =>$data['confirmacorreo'],
                'telefonoficina'  =>$data['telefonoficina'],
                'ext'             =>$data['ext'],
                'celular'         =>$data['celular'],
                'hotel'           =>$data['hotel'],
                'id_perfil'       =>$data['id_perfil'],
                'departamento'    =>$data['departamento'],
                'puesto'          =>$data['puesto'],
                'status'          =>$data['status'],
                'usuario'         =>$data['usuario'],
                

                
        ]);

        return $affected;



        }
        //validacion del formulario
        public function store(UsuarioHotel $request){
         $validated = $request->validated();
      

        }

   
}
