<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permiso;
use DB;
class PermisosController extends Controller
{
        public function index()
          {
            $permisos = Permiso::all();
    	      $rutas = self::rutas(5);
    	   	  return view('permisos.index',['permisos'=>$permisos,"rutas"=>$rutas]);
          }

        public function index2()
          {
            $permisos = Permiso::all();
            return $permisos;
          }

        public function tablaPermisos()
          {
 	         $permisos=Permiso::all();
           return view('permisos.tablapermisos',compact('permisos'));
          }

        public function agregarPermisos(Request $request)
          {
 	          $permisos=Permiso::find($request);
 	          return view('permisos.agregarpermisos',['permisos'=>$permisos]);
          }

    public function guardarActualizar(Request $request)
{
            //return $request->all();


    $arreglo = array();
    foreach($request as $k=>$v) $arreglo[$k] = $v;
    $id = $request->id;
    unset($request->id);

    $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
    return ($retornar)? "success" : "error";



}

private function agregar($data){

    //         $arreglo = array();
    //         foreach($data as $k=>$v) $arreglo[$k] = $v;
    //    //Hotel::create($arreglo);


   $affected = DB::table('permisos')->insert([

    'nombre'            => $data ['nombre'],



]);

   return $affected;
}

public function actualizar($data,$id){

        //     //$affected = DB::table('hotel')->where("id",$id)->update([

    $affected = DB::table('permisos')->where('id', '=', $id)->update([

    'nombre'          => $data ['nombre'],



        ]);

        return $affected;



}

  static function permisos_rutas(){

    $results = DB::table('permisos')
         ->join("rutas",'rutas.ruta_id','=', 'permisos.ruta_id')
         ->join("perfiles_rutas",'rutas.ruta_id','=',       'perfiles_rutas.ruta_id')
         ->join("categorias",'rutas.categoria_id','=', 'categorias.categoria_id')
     ->get();

    $temp = array();
    foreach($results as $k=>$v){

      if(!array_key_exists($v->categoria_id,$temp)){

        $temp[$v->categoria_id] = array();

        $temp[$v->categoria_id]["categoria_nombre"] = $v->nombre_categoria;
        $temp[$v->categoria_id]["rutas"] = array();

      }

      if(!array_key_exists($v->ruta_id,$temp[$v->categoria_id]["rutas"])){

        $temp[$v->categoria_id]["rutas"][$v->ruta_id] = array();

        $temp[$v->categoria_id]["rutas"][$v->ruta_id]["ruta_nombre"] = $v->nombre_ruta;
        $temp[$v->categoria_id]["rutas"][$v->ruta_id]["permisos"] = array();
      }


      $temp[$v->categoria_id]["rutas"][$v->ruta_id]["permisos"][$v->id] = $v->nombre;


    }

     return $temp;

  }


    static function rutas($all=false){

    	$query = DB::table('rutas')
    	     ->join("perfiles_rutas",'rutas.ruta_id','=', 'perfiles_rutas.ruta_id')
    	     ->join("categorias",'rutas.categoria_id','=', 'categorias.categoria_id');

      if($all){
        $query->where("perfil_id","$all")->where("perfiles_rutas.status","1");
      }

			$results =  $query->get();



		$temp = array();
		foreach($results as $k => $v){


			if(!key_exists($v->categoria_id,$temp)){


				$temp[$v->categoria_id] = array();
				$temp[$v->categoria_id]["categoria"] 	= $v->nombre_categoria;
				$temp[$v->categoria_id]["icono"] 		= $v->icono;
				$temp[$v->categoria_id]["elementos"] 	= array();

			}
			$temp[$v->categoria_id]["elementos"][$v->ruta_id] = array();
			$temp[$v->categoria_id]["elementos"][$v->ruta_id]["nombre"] = $v->nombre_ruta;
			$temp[$v->categoria_id]["elementos"][$v->ruta_id]["ruta"] 	= $v->ruta;
			$temp[$v->categoria_id]["elementos"][$v->ruta_id]["ruta_id"]= $v->ruta_id;



		}
		return $temp;


    }


}