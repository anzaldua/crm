<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class ProgrammerController extends Controller
{
    static function routes(){

        $permisos = new PermisosController();
            $rutas = $permisos->rutas();


        return view('Programmer.routes',["rutas"=>$rutas]);

    }
    static function getCategories($id){
        if($id){

            $results = DB::table('categorias');

            if($id !== "all"){

                $results->where('categoria_id', $id);

            }

            $results = $results->get();

        }else{

            $results = array(
                0 => array(
                    "categoria_id" => 0,
                    "nombre_ruta" => ""
                )
            );

        }

        return json_encode($results);

    }

    static function getRoutes($id){


        if($id){

            $results = DB::table('rutas')
            ->join("categorias",'rutas.categoria_id','=', 'categorias.categoria_id');

            if($id !== "all"){

                $results->where('rutas.ruta_id', $id);
                $categorias = json_decode(self::getCategories("all"),true);

            }else{

                $categorias = array();

            }

            $results = $results->get();

        }else{

            $categorias = json_decode(self::getCategories("all"),true);

            $results = array(
                0 => array(
                    "categoria_id" => 0,
                    "nombre_ruta" => ""
                )
            );

        }

        $return = array("rutas"=>$results,"categorias"=>$categorias);


        return json_encode($return);
    }
    static function saveRoutes(Request $request){

    }

}
