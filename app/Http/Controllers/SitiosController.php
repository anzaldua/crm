<?php

namespace App\Http\Controllers;

use  Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\SitioRequest;
use App\Exports\SitiosExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Sitio;
use DB;

class SitiosController extends Controller
{

   
   public function export(){

     return Excel::download(new SitiosExport,'sitios.xlsx');
   }


    public function index()
    {
        $sitios =Sitio::all();
    return view('sitios.index',compact('sitios'));
    }

    public function tablaSitios(Request $request){

    $sitios=Sitio::all(); 
   return view('sitios.tablasitios',compact('sitios'));

    }

    public function agregarSitios(Request $request){

        $sitios=Sitio::find($request);
    return view('sitios.agregarsitios',['sitios'=>$sitios]);
    }
    public function guardarActualizar(Request $request)
{
            //return $request->all();
  
  
    $arreglo = array();
    foreach($request as $k=>$v) $arreglo[$k] = $v;
    $id = $request->id;
    unset($request->id);

    $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
    
    return ($retornar)? "success" : "error";
    

    
}

private function agregar($data){

    //         $arreglo = array();
    //         foreach($data as $k=>$v) $arreglo[$k] = $v;
    //    //Hotel::create($arreglo);
   

   $affected = DB::table('sitios')->insert([
 
    'nombre'=>$data['nombre'],
    'usuario'=>$data['usuario'],
    'status'=>$data['status'],
    
    
]);

   return $affected;
}





public function actualizar($data,$id){  
  
        //     //$affected = DB::table('hotel')->where("id",$id)->update([
  
           $affected = DB::table('sitios')->where('id', '=', $id)->update([
             'nombre'=>$data['nombre'],
              'usuario'=>$data['usuario'],
              'status'=>$data['status'],
              
        
    
        ]);

        return $affected;



}
}
