<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Departamento;
use App\Puesto;
use App\Http\Controllers\PermisosController;
use DB;

class PuestosController extends Controller
{
 
  public function export(){

    // return Excel::download(new SitiosExport,'sitios.xlsx');
      }

     public function index()
    {  
      $puestos =DB::table('puestos')->get();
      
        $puestos =Puesto::all();
          $permisos = new PermisosController();
        $rutas    = $permisos->rutas();  


      return view('puestos.index',['puestos'=>$puestos,"rutas"=>$rutas]);

    }

    public function tablaPuestos(Request $request){

    $puestos=Puesto::all(); 
      return view('puestos.Tablapuestos',compact('puestos'));

    }

    public function agregarPuestos(Request $request){
     //consulta 'para traerme los resultados de la tabla perfiles
     $departamentos = Departamento::all();                   
     $puestos= Puesto::find($request);
    return view('puestos.agregarpuestos',['puestos'=>$puestos,"departamentos"=>$departamentos]);

    }
    public function guardarActualizar(Request $request)
{
            //return $request->all();
  
  
    $arreglo = array();
    foreach($request as $k=>$v) $arreglo[$k] = $v;
    $id = $request->id;
    unset($request->id);

    $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
    
    return ($retornar)? "success" : "error";
    

    
}

private function agregar($data){
   
   $affected = DB::table('puestos')->insert([

    'nombre'            => $data ['nombre'],
    'departamento_id'   => $data ['departamento_id'],
    
    
]);

   return $affected;
}





public function actualizar($data,$id){  
  
        //     //$affected = DB::table('hotel')->where("id",$id)->update([
  
    $affected = DB::table('puestos')->where('id', '=', $id)->update([
    
    'nombre'          => $data ['nombre'],
    'departamento_id' => $data ['departamento_id'],
  
    
        ]);

        return $affected;



}
}
