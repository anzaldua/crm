<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Usuario;
use App\Perfil;
use App\Departamento;
use App\Puesto;
use App\Http\Controllers\PermisosController;
use DB;

class UsuariosController extends Controller
{

    public function index()

    {
        $usuarios = DB::table('usuarios')->get();
      //  $departamentos = Departamento::lista('nombre','id');
        $permisos = new PermisosController();
        $rutas    = $permisos->rutas();
        $usuarios =Usuario::all();
        return view('usuarios.index',['usuarios'=>$usuarios,'departamentos'=>'$departamentos',"rutas"=>$rutas]);


    }


    public  function getPuestos(Request $request,$id)

    {

        if($request->ajax()){
            $puestos=Puesto::Puesto($id);
            return json_encode($puestos);
        }

    }



    public function tablaUsuarios(Request $request){

    $usuarios=Usuario::all();
      return view('usuarios.tablausuarios',compact('usuarios'));

    }

    public function agregarUsuarios(Request $request){
            //consulta 'para traerme los resultados de la tabla perfiles'
            $perfiles = Perfil::all();
            $departamentos = Departamento::all();
            $puestos = Puesto::all();
            $usuarios=Usuario::find($request);
         return view('usuarios.agregarusuarios',['usuarios'=>$usuarios,"perfiles"=>$perfiles ,"departamentos"=>$departamentos, "puestos"=>$puestos]);

        }

public function guardarActualizar(Request $request)
{
//return $request->all();

    $arreglo = array();
    foreach($request as $k=>$v) $arreglo[$k] = $v;
    $id = $request->id;
    unset($request->id);

    $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);

    return ($retornar)? "success" : "error";

}

private function agregar($data){

    //         $arreglo = array();s
    //         foreach($data as $k=>$v) $arreglo[$k] = $v;
    //    //Hotel::create($arreglo);


   $affected = DB::table('usuarios')->insert([

    'nombre'            => $data ['nombre'],
    'paterno'           => $data ['paterno'],
    'materno'           => $data ['materno'],
    'usuario'           => $data ['usuario'],
    'password'          => $data ['password'],
    'fechanacimiento'   => $data ['fechanacimiento'],
    'departamento_id'   => $data ['departamento_id'],
    'puesto_id'         => $data ['puesto_id'],
    'perfil_id'         => $data ['perfil_id'],
    'email'             => $data ['email'],
    'tipo'              => "1",
    'id_categoria'      => $data ['id_categoria'],
    'status'            => $data ['status'],
    'id_corporativo'    => $data ['id_corporativo'],
    'supervisado_por'   => "1",

]);

   return $affected;
}





public function actualizar($data,$id){

        //     //$affected = DB::table('hotel')->where("id",$id)->update([

    $affected = DB::table('usuarios')->where('id', '=', $id)->update([

    'nombre'          => $data ['nombre'],
    'paterno'         => $data ['paterno'],
    'materno'         => $data ['materno'],
    'usuario'         => $data ['usuario'],
    'password'        => $data ['password'],
    'fechanacimiento' => $data ['fechanacimiento'],
    'departamento_id' => $data ['departamento_id'],
    'puesto_id'       => $data ['puesto_id'],
    'perfil_id'       => $data ['perfil_id'],
    'email'           => $data ['email'],
    'tipo'            => "1",
    'id_categoria'    => $data ['id_categoria'],
    'status'         => $data ['status'],
    'id_corporativo'  => $data ['id_corporativo'],
    'supervisado_por' =>"1",



        ]);

        return $affected;



}


}
