<?php

namespace App\Http\Controllers;
use  Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\AgendaRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\PermisosController;
use App\Agenda;
use DB;

class AgendasController extends Controller
{


   public function index(){
    // $permisos = new PermisosController();
    // $rutas    = $permisos->rutas();


    $agendas = Agenda::all();
    $permisos = new PermisosController();
    $rutas    = $permisos->rutas();  

    return view('Agenda.index',['agendas'=>$agendas,'permisos'=>$permisos,'rutas'=> $rutas]);
}

    //Metodo de tablaHoteles
public function Tablaagenda( Request $request)
         {
         
          $Agendas = Agenda::all();
          return view('Agenda.tablaagenda',compact('agendas'));
        
        }

    // Metodo de agregarHoteles
public function agregarAgenda(Request $request)
{
     // $agendas  = array(-1=>"Seleccione un perfil",1=>"Sistemas",2=>"Administración",3=>"Gerente");
    $agendas =Agenda::find($request);
    return view('Agenda.agregaragenda',['agendas'=>$agendas]);
}

public function guardarActualizar(Request $request)
{
            //return $request->all();


    $arreglo = array();
    foreach($request as $k=>$v) $arreglo[$k] = $v;
    $id = $request->id;
    unset($request->id);

    $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);

    return ($retornar)? "success" : "error";



}

private function agregar($data){

    //         $arreglo = array();
    //         foreach($data as $k=>$v) $arreglo[$k] = $v;
    //    //Hotel::create($arreglo);


   $affected = DB::table('agendas')->insert([

    'nombre'=>$data['nombre'],
    'descripcion'=>$data['descripcion'],
    'tipo'=>$data['tipo'],
    'status'=>$data['status'],

]);

   return $affected;
}





public function actualizar($data,$id){

        //     //$affected = DB::table('hotel')->where("id",$id)->update([

           $affected = DB::table('agenda')->where('agenda_id', '=', $id)->update([
             'nombre'=>$data['nombre'],
              'descripcion'=>$data['descripcion'],
              'tipo'=>$data['tipo'],
              'status'=>$data['status'],


        ]);

        return $affected;



}




public function destroy($id)
{
            // $hotel = Hotel::find($id);
            // $hotel->delete();
            // return redirect('/hoteles')->with('success', 'Stock has been deleted Successfully');
}
    //metedo de verusuarios para verlos en un modal
public function verUsuarios(){

        // return view('hoteles.modal');
}
       //metodo de guardar y actualizar de cotizaciones
public function guardarActualizarCotizacion(Request $request)
{
            // //return $request->all();


            // $arreglo = array();
            // foreach($request as $k=>$v) $arreglo[$k] = $v;
            // $id = $request->id;
            // unset($request->id);

            // $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);

            // return ($retornar)? "success" : "error";



}


public function actualizarCotizacion($data ,$id){

      //   $affected=BD::table('hotel')->insert([

      // 'incContrato'=>$data['incContrato'],
      // 'plan'=>$data['plan'],
      // 'finContrato'=>$data['finContrato'],
      // 'costoxmes'=>$data['costoxmes'],
      // 'descuento'=>$data['descuento'],
      // 'total'=>$data['total'],
      // 'apertura'=>$data['apertura'],
      // 'mantenimiento'=>$data['mantenimiento'],
      // 'costo'=>$data['costo'],
      // 'descuento'=>$data['descuento'],
      // 'costo'=>$data['descuento'],
      // 'total'=>$data['total'],


      //   ]);

      //  return $affected;

}

public function accionescategorias()
  {

return view('categorias.editar');

  }

}
