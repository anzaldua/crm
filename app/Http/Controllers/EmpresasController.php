<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\PermisosController;
use App\Empresa;
use App\Giro;
use App\Segmento;
use DB;

class EmpresasController extends Controller
{
  
    public function index()
    {
            $empresas = Empresa::all();
            $permisos = new PermisosController();
            $rutas    = $permisos->rutas();
            return view('empresas.index',["empresas"=>$empresas ,"rutas"=>$rutas ,]);
    
    }

    public function Tablaempresas(Request $request)
    {   
        $empresas = Empresa::all();
        return view('empresas.tablaempresas', compact('empresas'));
    }

    public function Agregarempresas(Request $request)
        {
            $segmentos = Segmento::all();                   
            $empresas  =Empresa::find($request);
            $giros     = Giro::all(); 


            return view('empresas.agregarempresas',['empresas'=>$empresas,"segmentos"=>$segmentos ,"giros"=>$giros]);
        }


    public function guardarActualizar(Request $request)
    {

         $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
            
            return ($retornar)? "success" : "error";

          
    }


    
        private function Agregar($data){


        // $empresas = new Empresa::all();
        $affected = DB::table('empresas')->insert([

                 'nombreempresa'   =>$data['nombreempresa'],
                  'giro_id'        =>$data['giro_id'],
                  'segmento_id'    =>$data['segmento_id'],
                  'tipoempresas'    =>$data['tipoempresas'],
                  'direccion'      =>$data['direccion'],
                  'colonia'        =>$data['colonia'],
                  'cp'             =>$data['cp'],
                  'coloniafiscal'  =>$data['coloniafiscal'],
                  'cpfiscal'       =>$data['cpfiscal'],
                  'razonsocial'    =>$data['razonsocial'],
                  'rfcfiscal'      =>$data['rfcfiscal'],
                  'telefonoempresas' =>$data['telefonoempresas'],
                  'paginaweb'        =>$data['paginaweb'],
               
            ]);

            return $affected;
            }

    
           public function Actualizar($data,$id){  
        
            $affected = DB::table('empresas')->where('id', '=', $id)->update([
                    'nombreempresa'    =>$data['nombreempresa'],
                    'giro_id'          =>$data['giro_id'],
                    'segmento_id'      =>$data['segmento_id'],
                    'tipoempresas'      =>$data['tipoempresas'],
                    'direccion'        =>$data['direccion'],
                    'colonia'          =>$data['colonia'],
                    'cp'               =>$data['cp'],
                    'coloniafiscal'    =>$data['coloniafiscal'],
                    'cpfiscal'         =>$data['cpfiscal'],
                    'razonsocial'      =>$data['razonsocial'],
                    'rfcfiscal'        =>$data['rfcfiscal'],
                    'telefonoempresas' =>$data['telefonoempresas'],
                    'paginaweb'        =>$data['paginaweb'],

                 
                 ]);

        return $affected;



        }

   public function byEmpresas($id){
    return giro::where('segmento','=' ,$id)->get();
   }

}