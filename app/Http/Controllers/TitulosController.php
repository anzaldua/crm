<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\PermisosController;
use App\Titulo;
use DB;


class TitulosController extends Controller
{
   
    public function index()
     {      

        $titulos = Titulo::all();
        $permisos = new PermisosController();
        $rutas    = $permisos->rutas(); 
        
        return view('titulos.index',['titulos'=>$titulos,"rutas"=>$rutas]);
    }

     public function Tablatitulos(Request $request)
    {
         $titulos = Titulo::all(); 
         return view ('titulos.Tablatitulos',compact('titulos'));
    }


    public function Agregartitulos(Request $request)
    
    {

            $titulos = Titulo::find($request);
            $titulos = Titulo::all();

            return view('titulos.agregartitulos',compact('titulos'));
        
    }

    public function Guardaractualizar(Request $request)
        
        {
            
            //return $request->all();
            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
            
            return ($retornar)? "success" : "error";
                    
        }

          private function Agregar($data){


        $affected = DB::table('titulos')->insert([

            
                'nombretitulo'          =>$data['nombretitulo' ],    
                
                
               
            ]);

            return $affected;
         }

    
        public function Actualizar($data,$id){  
    
          
            $affected = DB::table('titulos')->where('id', '=', $id)->update([

                'nombretitulo'  => $data['nombretitulo'],            
                
                        

                
        ]);

        return $affected;



        }

   
 
}
