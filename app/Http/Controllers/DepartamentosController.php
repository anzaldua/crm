<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\DepartamentoRequest;
use Illuminate\Foundation\Http\FormRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\PermisosController;
use App\Departamento;
use DB;



    class Departamentoscontroller extends Controller
    
        {
               public function index()
        {
            $departamentos=Departamento::all();
            $permisos = new PermisosController();
            $rutas    = $permisos->rutas(); 
            //return view('departamentos.index',compact('departamentos'));
            return view('departamentos.index',['departamentos'=>$departamentos,"rutas"=>$rutas]);
        }


        public function Tabladepartamentos( Request $request)
        {
            $departamentos = Departamento::all(); 
            return view('departamentos.tabladepartamentos',compact('departamentos'));
        }

        public function Agregardepartamentos(Request $request)
        {

            $departamentos=Departamento::find($request);
            $departamentos = Departamento::all();
            return view('departamentos.agregardepartamentos',compact('departamentos'));

        }

        public function Guardaractualizar(Request $request)
        {
            //return $request->all();
      
  
            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
            
            return ($retornar)? "success" : "error";
                    
        }

        private function Agregar($data){


        $affected = DB::table('departamentos')->insert([

            
                'nombre'          =>$data['nombre' ],    
                'status'          =>$data['status' ],
                'detalle'         =>$data['detalle'],
               
            ]);

            return $affected;
         }

    
        public function Actualizar($data,$id){  
    
          
            $affected = DB::table('departamentos')->where('id', '=', $id)->update([
                'nombre'          =>$data['nombre'],            
                'status'          =>$data['status'],
                'detalle'         =>$data['detalle'],
          

                
        ]);

        return $affected;



        }

   
}
