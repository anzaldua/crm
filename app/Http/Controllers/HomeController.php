<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PermisosController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index( Request $request)
    {
         $permisos = new PermisosController();
            $rutas    = $permisos->rutas();
            $empresas  = new EmpresasController();
       return view ('home',["permisos"=>$permisos ,"rutas"=>$rutas]);


    }
}
