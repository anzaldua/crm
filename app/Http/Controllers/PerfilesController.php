<?php

namespace App\Http\Controllers;
use  Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\PerfilesRequest;
use Yajra\DataTables\Facades\DataTables;
use App\Perfil;
use App\permiso;
use App\Tipo;
use App\Departamento;
use App\Http\Controllers\PermisosController;
use App\Http\Controllers\ProgrammerController;
use DB;


   class Perfilescontroller extends Controller



{

    public function index()
        {
            $perfiles =DB::table('perfiles')->get();
            $perfiles =Perfil::all();
            $permisos = new PermisosController();
            $rutas    = $permisos->rutas(1);

            return view('perfiles.index',['perfiles'=>$perfiles,"rutas"=>$rutas]);
        }
    //Metodo de tablaHoteles
    public function tablaPerfiles( Request $request)
        {
            $perfiles=Perfil::all();
            return view('perfiles.tablaPerfiles',compact('perfiles'));
        }

    // Metodo de agregarHoteles
    public function agregarPerfiles(Request $request)
    {
        //$id = $request->get("id");
        //$hoteles=Hotel::where($id);
                $permisos = Permiso::all(); 
                $tipousuarios = Tipo::all();
                $perfiles=Perfil::find($request);

                $permisos = new PermisosController();
                $rutas    = $permisos->rutas();
                return view('perfiles.agregarperfil',['perfiles'=>$perfiles ,"permisos"=>$permisos,'tipousuarios'=>$tipousuarios,"rutas"=>$rutas]);
        }

    public function guardarActualizar(Request $request)
        {
            //return $request->all();


            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);

            return ($retornar)? "success" : "error";



        }

        private function agregar($data){

    //          $arreglo = array();

    //          foreach($data as $k=>$v) $arreglo[$k] = $v;

    //          //Hotel::create($arreglo);


         $affected = DB::table('perfiles')->insert([
                'nombre'=>$data['nombre'],
                'tipo_id'=>$data['tipo_id'],
                'descripcion'=>$data['descripcion'],

            ]);

            return $affected;
         }





        public function actualizar($data,$id){

            //$affected = DB::table('hotel')->where("id",$id)->update([



            $affected = DB::table('perfiles')->where('id', '=', $id)->update([
                'nombre'=>$data['nombre'],
                'tipo_id'=>$data['tipo_id'],
                'descripcion'=>$data['descripcion'],




        ]);

        return $affected;

        }

        public function agregaracciones(){
             $departamentos = Departamento::all();
                $permisos = new PermisosController();
            $rutas    = $permisos->rutas();
      

        return view('perfiles.acciones',["departamentos"=>$departamentos,'rutas'=>$rutas]);

        }

        public function agregar_ruta_perfil(Request $request){
            
            $check = $request["check"];
            $element = $request["element"];
            $perfil = $request["perfil"];

            $ruta_perfil = DB::table('perfiles_rutas')->where('ruta_id', $element)->where('perfil_id', $perfil);
             
           //comprobar cuantos registros me trae
             $count = $ruta_perfil->count();

             if($count){
                $results = $ruta_perfil->get();
                $status  = $results[0]->status ? 0 : 1;

                $id = $results[0]->perfiles_rutas_id;

                $nq = DB::table('perfiles_rutas')
                            ->where('perfiles_rutas_id', $id)
                            ->update(['status' => $status]);

                            echo $status;

             }else{

             }
            
            return "";

        }
        
        public function traerpermisos_perfil(Request $request){
            $results = DB::table('permisos')
                    ->join("rutas",'rutas.ruta_id','=', 'permisos.ruta_id')
                    ->join("perfiles_rutas",'rutas.ruta_id','=',       'perfiles_rutas.ruta_id')
                    ->join("categorias",'rutas.categoria_id','=', 'categorias.categoria_id')
                    ->where("perfil_id",$request["perfil"])
                ->get();
            
            
            if($request["view"] == "true"){
                $temp = array(); 


                return view('perfiles.permisos',["permisos"=>$results]);
                 

            }
            
            
        }

}
