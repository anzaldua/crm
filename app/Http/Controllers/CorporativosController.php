<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CorporativoRequest;
use App\Corporativo;
use DB;

class CorporativosController extends Controller
{
 
   
  public function export(){

     //return Excel::download(new SitiosExport,'sitios.xlsx');
   }

    public function index()
    {  
        $corporativos =DB::table('corporativo')->get();
        $corporativos =Corporativo::all();
        return view('corporativo.index',compact('corporativos'));
    }

    public function tablaCorporativos(Request $request){
        $corporativos=Corporativo::all(); 
        return view('corporativo.tablacorporativo',compact('corporativos'));

    }

    public function agregarCorporativos(Request $request){
            //consulta 'para traerme los resultados de la tabla perfiles
                        
            // $perfiles = array(-1=>"Seleccione un perfil",1=>"Sistemas",2=>"Administración",3=>"Gerente");        
            
            $corporativos=Corporativo::find($request);
            return view('corporativo.agregarcorporativo',compact('corporativos'));

    }
        public function guardarActualizar(Request $request)
            {
                //return $request->all();
  
  
            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
    
            return ($retornar)? "success" : "error";
    

    
            }

private function agregar($data){

    //         $arreglo = array();
    //         foreach($data as $k=>$v) $arreglo[$k] = $v;
    //    //Hotel::create($arreglo);
   

   $affected = DB::table('corporativo')->insert([

    'nombre'            => $data ['nombre'],
    'status'            => $data ['status'],


    
    
]);

   return $affected;
}





public function actualizar($data,$id){  
  
        //     //$affected = DB::table('hotel')->where("id",$id)->update([
  
    $affected = DB::table('corporativo')->where('id', '=', $id)->update([
    
    'nombre'            => $data ['nombre'],
    'status'            => $data ['status'],
        
        ]);

        return $affected;



}
}
