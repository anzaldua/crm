<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Contacto;
use App\Empresa;
use App\Titulo;
use App\Http\Controllers\PermisosController;
use DB;

class ContactosController extends Controller
{
 
  public function export(){

    // return Excel::download(new SitiosExport,'sitios.xlsx');
      }

     public function index()
    {  
      $contactos =DB::table('contactos')->get();
      
        $contactos = Contacto::all();
        $permisos  = new PermisosController();
        $rutas     = $permisos->rutas();



      return view('contactos.index',['contactos'=>$contactos,"rutas"=>$rutas]);

    }

    public function Tablacontactos(Request $request){

      $contactos=Contacto::all(); 
      return view('contactos.tablacontactos',compact('contactos'));

    }

    public function Agregarcontactos(Request $request){
     //consulta 'para traerme los resultados de la tabla perfiles
     $titulos   = Titulo::all();  
     $empresas  = Empresa::all();              
     $contactos = Contacto::find($request);
    return view('contactos.agregarcontactos',['contactos'=>$contactos, 'empresas'=>$empresas ,'titulos'=>$titulos]);

    }
    public function guardarActualizar(Request $request)
{
            //return $request->all();
  
  
    $arreglo = array();
    foreach($request as $k=>$v) $arreglo[$k] = $v;
    $id = $request->id;
    unset($request->id);
    

    $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
    
    return ($retornar)? "success" : "error";
    

    
}

private function agregar($data){
   
   $affected = DB::table('contactos')->insert([

            'titulo' => $data['titulo'],
            'nombre' => $data['nombre'],
            'paterno'=> $data['paterno'],
            'materno' => $data['materno'],
            'estadocivil' => $data['estadocivil'],
            'fechanacimiento' => $data['fechanacimiento'],
            'fechaimportante' => $data['fechaimportante'],
            'puesto' => $data['puesto'],
            'departamento' => $data['departamento'],
            'reserva' =>$data['reserva'],
            'convenios' => $data['convenios'],
            'grupos' => $data['grupos'],
            'banquetes' => $data['banquetes'],
            'telefono' => $data['telefono'],
            'celular' => $data['celular'],
            'emailempresa' => $data['emailempresa'],
            'emailpersonal' => $data ['emailpersonal'],
            'promediocn' => $data ['promediocn'],
            'tarifa' => $data ['tarifa'],
            'nivel' => $data ['nivel'],
            'ejecutivos' =>$data['ejecutivos'],
            'programadelealtad' => $data['programadelealtad'],
            'numeropl' => $data['numeropl'],
            'pasatiempo'=>$data['pasatiempo'],
            'asistente' => $data['asistente'],
            'felicitacion' => $data['felicitacion'],
            'tipofe' => $data['tipofe'],
            'status' => $data['status'],
            'empresa_id' => $data['empresa_id'],
    
    
]);

   return $affected;
}





public function actualizar($data,$id){  
  
        //     //$affected = DB::table('hotel')->where("id",$id)->update([
  
    $affected = DB::table('contactos')->where('id', '=', $id)->update([
    
            'titulo' => $data['titulo'],
            'nombre' => $data['nombre'],
            'paterno'=> $data['paterno'],
            'materno' => $data['materno'],
            'estadocivil' => $data['estadocivil'],
            'fechanacimiento' => $data['fechanacimiento'],
            'fechaimportante' => $data['fechaimportante'],
            'puesto' => $data['puesto'],
            'departamento' => $data['departamento'],
            'reserva' =>$data['reserva'],
            'convenios' => $data['convenios'],
            'grupos' => $data['grupos'],
            'banquetes' => $data['banquetes'],
            'telefono' => $data['telefono'],
            'celular' => $data['celular'],
            'emailempresa' => $data['emailempresa'],
            'emailpersonal' => $data ['emailpersonal'],
            'promediocn' => $data ['promediocn'],
            'tarifa' => $data ['tarifa'],
            'nivel' => $data ['nivel'],
            'ejecutivos' =>$data['ejecutivos'],
            'programadelealtad' => $data['programadelealtad'],
            'numeropl' => $data['numeropl'],
            'pasatiempo'=>$data['pasatiempo'],
            'asistente' => $data['asistente'],
            'felicitacion' => $data['felicitacion'],
            'tipofe' => $data['tipofe'],
            'status' => $data['status'],
            'empresa_id' => $data['empresa_id'],
    
        ]);

        return $affected;



}
}
