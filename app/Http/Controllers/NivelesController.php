<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\PermisosController;
use App\Nivel;
use DB;


class NivelesController extends Controller
{
   
    public function index()
     
     {      

        $niveles = Nivel::all();
        $permisos = new PermisosController();
        $rutas    = $permisos->rutas(); 
        
        return view('niveles.index',['niveles'=>$niveles,"rutas"=>$rutas]);
    }

     public function Tablaniveles(Request $request)
    {
         $niveles = Nivel::all(); 
         return view ('niveles.Tablaniveles',compact('niveles'));
    }


    public function Agregarniveles(Request $request)
    
    {

            $niveles = Nivel::find($request);
            $niveles = Nivel::all();

            return view('niveles.agregarniveles',compact('niveles'));
        
    }

    public function Guardaractualizar(Request $request)
        
        {
            
            //return $request->all();
            $arreglo = array();
            foreach($request as $k=>$v) $arreglo[$k] = $v;
            $id = $request->id;
            unset($request->id);

            $retornar = ($id)? self::actualizar($request,$id) : self::agregar($request);
            
            return ($retornar)? "success" : "error";
                    
        }

          private function Agregar($data){


        $affected = DB::table('niveles')->insert([

            
                'nombrenivel' => $data['nombrenivel'],    
                
                
               
            ]);

            return $affected;
         }

    
        public function Actualizar($data,$id){  
    
          
            $affected = DB::table('niveles')->where('id', '=', $id)->update([

                'nombrenivel' => $data['nombrenivel'],            
                
                        

                
        ]);

        return $affected;



        }

   
 
}
