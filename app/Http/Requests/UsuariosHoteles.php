<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuariosHoteles extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'           =>'required',
            'paterno'          =>'required',
            'materno'          =>'required',
            'email'            =>'required',
            'confirmacorreo'   =>'required',
            'telefonoficina'   =>'required',
            'ext'              =>'required',
            'celular'          =>'required',
            'hotel'            =>'required',
            'id_perfil'        =>'required',
            'departamento'     =>'required',
            'puesto'           =>'required',        
            'status'           =>'required',
            'usuario'          =>'required',
        ];
    }
}
