<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HotelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // ponemos que el campo no llegue vacio y podemos definir que tenga cierto numero de caracteres
                'nombrehotel'=>'required',
                'codigohotel'=>'required',
                'direccioncomercial'=>'required',
                'codigohotel'=>'required',
                'colonia'=>'required',
                'cp'=>'required',    
                'razonsocial'=>'required',
                'calleynumero'=>'required',
                'telefonodirecto'=>'required',
                'telefono2'=>'required',
                'web'=>'required',
                //'logohotel'=>'required',
                'principalcontacto'=>'required',
                'notas'=>'required',
                //'nodehabitaciones'=>'required',
                //'impuesto'=>'required',
                'status'=>'required',
                //'coloniafiscal'=>'required',
                //'codigopostalfiscal'=>'required',
                'rfc'=>'required',
                //'logoconvenios'=>'required',
                //'idcorporativo'=>'required',
                'incContrato'=>'required',
                'finContrato'=>'required',
              

        ];
    }

    public function mensajes(){
      return[
        // mensajes que apareceran en el navegador 
                'nombrehotel.required'=>'el :attribute del hotel es obligatorio',
                'codigohotel.required'=>'el:attribute campo codigohotel es obligatorio',
                'direccioncomercial.required'=>'el:attribute campo direccioncomercial es bligatorio',
                'codigohotel.required'=>'el :attribute codigohotel es obligatorio',
                'colonia.required'=>'el :attribute colonia es obligatorio',
                'cp.required'=>'el :attribute codigopostal es obligatorio',
                'razonsocial.required'=>'el attribute razonsocial es obligatorio',
                'calleynumero.required'=>'el :attribute calleynumero es obligatorio',
                'telefonodirecto.required'=>'el :attribute telefonodirecto es obligatorio',
                'telefono2.required'=>'el :attribute telefono2 es obligatorio',
                'web.required'=>'el :attribute web es obligatorio',
                'logohotel.required'=>'el :attribute logohotel es obligatorio',
                'principalcontacto.required'=>'el :attribute principalcontacto es obligatorio',
                'notas.required'=>'el :attribute notas es obligatorio',
                //'impuesto.required'=>'el :attribute impusto es obligatorio',
                'status.required'=>'el :attribute status es obligatorio',
                //'codigopostalfiscal.required'=>'el :attribute codigopostalfiscal es obligatorio',
                'rfc.required'=>'el :attribute rfc es obligatorio',
                //'logoconvenios.required'=>' el :attribute logoconvenios es obligatorio',
                'incContrato.required'=>'el :attribute inicio de contrato es obligatorio',
                'finContrato.required'=>'el :attribute fincontrato es obligatorio',




        ];


    }

    public function attributes(){

        return[
              // ponemos los alias de  llos mensajes yo los uso igualmente por qye es algo que esta amano para usar.
                'nombrehotel'=>'nombre del hotel',//listo
                'codigohotel'=>'codigo del hotel',//listo
                'nombrecomercial'=>'nombrecomercial',//listo
                'direccioncomercial'=>'direccioncomercial',//listo
                'codigohotel'=>'codigohotel',//listo
                'colonia'=>'colonia',//listo
                'cp'=>'cp',//listo
                'razonsocial'=>'razonsocial',
                'calleynumero'=>'calleynumero',
                'telefonodirecto'=>'telefonodirecto',
                'telefono2'=>'telefono2',
                'web'=>'web',
                //'logohotel'=>'logohotel',
                'principalcontacto'=>'principalcontacto',
                'notas'=>'notas',
                'status'=>'status',
                //'codigopostalfiscal'=>'codigopostalfiscal',
                'rfc'=>'rfc',
                'incContrato'=>'incContrato',
                'finContrato'=>'finContrato',
                 

        ];
    }
       
}
