<?php

namespace App\Exports;

use App\Categoria;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CategoriasExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            
            'nombre',
            'descripcion',
            'tipo',
        ];
    }
    public function collection()
    {
         $categorias = DB::table('categorias')->select('nombre', 'descripcion','tipo')->get();
         return $categorias;
        
    }
}