<?php

namespace App\Exports;

use App\Sitio;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SitiosExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            
            'nombre',
            'usuario',
            
        ];
    }
    public function collection()
    {
         $sitios = DB::table('sitios')->select('nombre', 'usuario')->get();
         return $sitios;
        
    }
}