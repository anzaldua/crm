<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puesto extends Model
{
    protected $table ='puestos';

     protected $fillable =[
     	'nombre',
     	'departamento_id'

     ];

  public function usuarios(){
   	return $this->hasMany('App\Usuario');

	  }

public function departamento(){

	return $this->belongsTo('App\Departamento','departamento_id');
}	  
     
     public static function puesto($id){
     	return Puesto::where('departamento_id','=',$id)->get();
     }
}
