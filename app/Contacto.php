<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
      protected $table='contactos';
      protected $primaryKey = 'contacto_id';
      protected $fillable = [

      		'titulo',
            'nombre',
            'paterno',
            'materno',
            'estadocivil',
            'fechanacimiento',
            'fechaimportante',
            'puesto',
            'departamento',
            'reserva',
            'convenios',
            'grupos',
            'banquetes',
            'telefono',
            'celular',
            'emailempresa',
            'emailpersonal',
            'promediocn',
            'tarifa',
            'nivel',
            'ejecutivos',
            'programadelealtad',
            'numeropl',
            'pasatiempo',
            'asistente',
            'felicitacion',
            'tipofe',
            'status',
            'nombreempresa',


    ];
}
