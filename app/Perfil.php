<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
	public function User(){

	return  $this->belongToMany('App\User');
	}
    protected $table ='perfiles';

    protected $fillable = [

    	'nombre',
    	'tipo_id',
        'descripcion',
   
    ];

    public function usuarios(){

        // return $this->hasMany('App\Usuario');
    }
}
