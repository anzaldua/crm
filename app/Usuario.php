<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
      protected $table='usuarios';
      protected $fillable = [

     
      'nombre',
      'paterno',
      'materno',
      'usuario',
      'password',
      'fechanacimiento',
      'departamento_id',
      'puesto_id',
      'perfil_id',
      'email',
      'tipo',
      'id_categoria',
      'status',
      'id_corporativo',
      'supervisado_por',
    

    ];

    public  function departamento(){

      return $this->belongsTo('App\Departamento','departamento_id');
    }

   public  function puesto(){

      return $this->belongsTo('App\Puesto','puesto_id');
    }
 
 public function perfil(){

  return $this->belongsTo('App\Perfil','perfil_id');
 }
}
