<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{

	  protected $table = 'hotel';

	 protected $fillable = [
        //permisos de campós para los modales de hoteles
     'foto',
     'nombrehotel',
     'codigohotel',
     'nombrecomercial',
     'direccioncomercial',
     'colonia',
     'cp',
     'idciudad',
     'idestado',
     'idpais',
     'razonsocial',
     'calleynumero',
     'idciudadfiscal',
     'idestadofiscal',
     'idpaisfiscal',
     'telefonodirecto',
     'telefono2',
     'telefonocelular',
     'fax',
     'web',
     'idmarca',
     'idgrupohotelero',
     'logohotel',
     'principalcontacto',
     'idpuesto',
     'notas',
     'nodehabitaciones',
     'impuesto',
     'ish',
     'status',
     'coloniafiscal',
     'rfc',
     'latitud',
     'longitud',
     'zoom',
     'logoconvenios',
     'idcorporativo',
     'incContrato',
     'finContrato',
     'registrado',
	 ];


	 public function scopeSearch($query,$nombre)
{    
	//buscamos por nombre  en la tabla 
	return $query->where('nombre','LIKE',"%nombre%");

}


   
}

