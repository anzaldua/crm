<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
        use Notifiable;

        public function perfiles()
    {
        return $this->belongsToMany('App\Perfil');
    }

        public function authorizePerfil($perfil)
    {
        //validamos si es un array (si son muchos)
        if($this->hasAnyPerfil($perfil))
    {
        return true;


    }//mandamos un mensaje con la funcion abortar donde dice que el usuario no esta permitido para esa accion 
        abort(401, 'no estas autorizado para esta accion');
    }

        public  function hasAnyPerfil($perfiles){

        
        if (is_array($perfiles)){

            foreach ($perfiles as $perfil) {
                if($this->hasPerfil($perfil)){
                    return true;
                }
            }

        }else{
            if ($this->hasPerfil($perfiles)){

                return true;
            }
        }

        return  false;
        }







        //validamos que el rol tenga ese rol asignado que me regrese el primerp que en cuentre
        public function hasPerfil($role){

        if ($this->perfiles()->where('name',$role)->first()){
            return true;//si lo tiene regresamos que si
        }

        return false;//y si no me regresa que no 
        }
    

        /**
        * The attributes that are mass assignable.
        *
        * @var array
        */
        protected $fillable = [
        'name','email','password',
        ];

        /**
        * The attributes that should be hidden for arrays.
        *
        * @var array
        */
         protected $hidden = [
        'password', 'remember_token',
        ];

        /**
         * The attributes that should be cast to native types.
        *
        * @var array
        */
        protected $casts = [
        'email_verified_at' => 'datetime',
        ];
}
