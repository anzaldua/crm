<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
     protected $table = 'departamentos';

     protected $fillable = [
     	'nombre',
     	'status',
      'detalle'
     ];


   public function usuarios(){

   	return $this->hasMany('App\Usuario');
   }
   
   

   public function puestos(){

   	return $this->hasMany('App\Puesto');
   }
}
