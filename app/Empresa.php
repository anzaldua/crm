<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    
	 protected $table = 'empresas';

	 protected $fillable = [
        'nombreempresa',   
        'giro_id',
        'segmento_id',           
        'direccion',     
        'colonia',        
        'cp',                   
        'razonsocial',
        'direccionfiscal', 
        'coloniafiscal',
        'cpfiscal',
        'rfc',
        'telefonoempresa',
        'paginaweb',
        'logoempresa',
        'tipoempresas',
        'notas',   
	 ];



        public function segmento(){

       return $this->belongsTo('App\Segmento','segmento_id');        
      }

      public function giro(){
        return $this->belongsTo('App\giro_id','giro_id');
      }


   
}
