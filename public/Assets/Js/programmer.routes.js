var csrf = $("meta[name='csrf-token']").attr("content");
var $dr = $("#divresponse");
var contenido;
function routes(){

    $.ajax({
            type: "GET",
            url: "getRoutes/all",
            success: function (rp) {
                $data = JSON.parse(rp);
                contenido = ""
                +"<table>"
                    +"<thead>"
                        +"<tr>"
                            +"<th>Nombre</th>"
                            +"<th>Ruta</th>"
                            +"<th>Categoria</th>"
                            +"<th>Estado</th>"
                            +"<th>Editar</th>"
                        +"</tr>"
                    +"</thead>"
                    +"<tbody>"
                $.each($data.rutas,function(index,object){
                    contenido += ""
                        +"<tr k='"+object.ruta_id+"'>"
                            +"<td>"+object.nombre_ruta+"</td>"
                            +"<td>"+object.ruta+"</td>"
                            +"<td>"+object.nombre_categoria+"</td>"
                            +"<td> </td>"
                            +"<td><a class='mb-6 btn-floating waves-effect waves-light purple lightrn-1 btn-edit'><i class='fas fa-edit' style='font-size: 14px!important;'></i></a></td>"
                        +"</tr>"
                })
                contenido += ""
                    +"</tbody>"
                +"</table>"

                $dr.html(contenido);
                $(".btn-edit").on("click",function(){
                    var tv = $(this).parents("tr").attr("k");
                    editRoutes(tv)
                })
            }
        });
}

function editRoutes(tv){
    var modal= $("#modal1")
        modal.modal("open");

        $(".modal-content",modal).html("")

    $("#modal1").modal("open");
    $.get("getRoutes/"+tv,"",function(data,status){

        $data   = JSON.parse(data);
        $ruta   = $data.rutas[0];
        $categorias = $data.categorias;


        var active =  tv !== 0 ? "active" : "";
        var seld   =  tv !== 0 ? "" : "selected";

        contenido = ""
        +"<h4>Editar Ruta</h4>"
        +'<div class="row" style="margin-top:20px">'
            +'<div class="input-field col s6">'
                +'<input id="nombre_ruta" type="text" value="'+$ruta.nombre_ruta+'">'
                +'<label for="nombre_ruta" class="light-blue-text text-darken-4 '+active+'">Nombre de la ruta</label>'
            +'</div>'
            +'<div class="input-field col s6">'
                +'<input id="last_name" type="text" value="'+$ruta.ruta+'">'
                +'<label for="last_name" class="light-blue-text text-darken-4 '+active+'">Ruta (URL)</label>'
            +'</div>'
        +'</div>'
        +'<div class="row" style="margin-top:20px">'
            +'<div class="input-field col s6">'
                    +'<select>'
                    +'<option value="" disabled '+seld+'>Escoge una opción</option>'
        $.each($categorias,function(ic,categoria){

            selected = "";
            if(!seld && categoria.categoria_id == $ruta.ruta_id) selected = "selected";

            contenido  += '<option value="'+categoria.categoria_id+'" '+selected+'>'+categoria.nombre_categoria+'</option>'

        });
        contenido  += ""
                    +'</select>'
                +'<label for="" class="light-blue-text text-darken-4 active">Categoria</label>'
            +'</div>'

        $(".modal-content",modal).html(contenido)

        $('select').formSelect();

    })
}

$(window).on("load",function(){

    $('.modal').modal();

    routes();

})





//    $(document).ready(function(){



//     function funcionx(id, el) {
//   var statusCheck = ($("#status").is(":checked")) ? 1 : 0;
//   var datos = $("form#corporativos").serialize() + "&status=" + statusCheck;
//   $.ajax({
//     type: "POST",
//     data: datos + "&id=" + id,
//     url: "guardarCorporativos",
//     success: function (rp) {

//       if (rp == "success") {

//         tabla();
//         el.modal("close");

//       }
//     },
// error: function (data) {
//   if (data.status === 422) {
//     var errors = $.parseJSON(data.responseText);
//     $.each(errors, function (key, value) {
//           // console.log(key+ " " +value);
//           //$('#response').addClass("alert alert-danger");

//           if ($.isPlainObject(value)) {
//             $.each(value, function (key, value) {
//               console.log(key + " " + value);
//               alert(+ " " + value


//                   )

//           });
//         } else {
//             //$('#response').show().append(value + "<br/>"); //this is my div with messages
//         }
//         el.modal("close");
//     });
// }
// }
// })
// }

// // peticion de la tablaCategorias
// function tabla() {


//   $.ajax({

//       type: "POST",
//       url: "tablaCorporativos",
//       data: "",
//       success: function (r) {


//           $('#tabla-corporativos').html(r);
//               $('table#corporativos .btn-status').each(function(){
//               text = $(this).text();

//               clase = (text.toUpperCase() == "ACTIVO")? "mb-6 btn waves-effect waves-light green darken-1 black" : "disabled";

//               $(this).addClass(clase)

//           })
//             $('table#corporativos').DataTable({
//                 paging: false,
//                 stateSave: true,
//                 "columnDefs": [ {
//                 "targets": 2,
//                 "orderable": false,
//                 "searchable": false
//               } ]


//           });

//               $('.editarcorporativo').click(function () { //---BOTON  DE EDITAR---
//                   var id = $(this).parents("tr").attr("k");
//                   var el = $('#moda9').modal("open");
//                   $('#modal9 #contenido-modal').html("");


//                   $.ajax({
//                       type: "POST",
//                       url: "agregarCorporativos",
//                       data: {
//                           id
//                       },
//                       success: function (r) {

//                           $('#modal9 #contenido-modal').html(r);
//                           $("#guardarCorporativos").click(function ()
//                            {funcionx(id,el)})


//                       }
//                   });

//               });
//           }
//       });


// }
// tabla()
// $('.modal').modal();

// // peticion de la tabla

//  $('.modal').modal();

//  $('#agregarcorporativos').click(function () {

//       var el = $('#modal9').modal("open");

//       $('#modal9 #contenido-modal').html("");
//       $.ajax({

//           type: "POST",//tipo de envio
//           url: "agregarCorporativos",//url de donde enviamos los datos
//           data: "id=0",
//           success: function (r) {


//               $('#modal9 #contenido-modal').html(r);
//               $("#guardarCorporativos").click(function () {
//                   funcionx(0,el)
//               })

//           }
//       });

//   });
// });
