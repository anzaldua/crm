<?php
$count = count($hotelusuarios);
$active = ($count)? "active" : "";

$nombre   =($count)? $hotelusuarios[0]->nombre  : "" ;
$paterno  =($count)? $hotelusuarios[0]->paterno : "" ;
$materno  =($count)? $hotelusuarios[0]->materno : "";
$email    =($count)? $hotelusuarios[0]->email   : "";
$confirmacorreo  = ($count)? $hotelusuarios[0]->confirmacorreo  : "";
$telefonoficina = ($count)? $hotelusuarios[0]->telefonoficina : "";
$ext      =($count)? $hotelusuarios[0]->ext      : "";
$celular  =($count)? $hotelusuarios[0]->celular  : "";
$hotel    =($count)? $hotelusuarios[0]->hotel    : ""; 
$Id_perfil=($count)? $hotelusuarios[0]->Id_perfil: "";
$puesto   =($count)? $hotelusuarios[0]->puesto   : "";
$departamento=($count)? $hotelusuarios[0]->departamento : ""; 
$status   =($count)? $hotelusuarios[0]->status   : "";
$usuario  =($count)? $hotelusuarios [0]->usuario : "";







?>

<div class="modal-content">
          @csrf
           <h5>{{ __('Nuevo UsuarioHotel') }}</h5>
            <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
            <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
            <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>  
           <div class="row">
            <div class="col s10">
              <ul class="tabs">
                <li class="tab col s3"><a href="#usuario">Usuario</a></li>
                <li class="tab col s3"><a href="#detalle">Detalle</a></li>
                <li class="tab col s3"><a href="#Accesso">Datos</a></li>
              </ul>
          
    
  <form id='usuarioshotel' class="formValidate" id="formValidate" method="get">
    <br>
      @csrf
        <div class="row" id="usuario">
            <div class="input-field col s6">
              <input id="icon_prefix1" type="text" id="nombre" class="validate" name="nombre" 
              value='{{$nombre}}' required aria-required="true"  required autocomplete="nombre" required="
              ">
              <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('Nombre') }}</h6></label>
              <p class='errornombre text-center alert alert-danger hidden'></p>
              </div>


            <div class="input-field col s6">
                <input id="icon_prefix2" type="text" id="paterno" class="validate" name="paterno" 
                value='{{$paterno}}' required aria-required="true"  required autocomplete="paterno">
                <label for="icon_prefix2" class="center-align {{$active}}"><h6>{{ __('Apellido Paterno') }}</h6></label>
                <p class='errordescripcion text-center alert alert-danger hidden'></p>
            </div>

        <div class="input-field col s6">
            <input id="icon_prefix3" type="text" id="materno" class="validate" name="materno" 
            value='{{$materno}}' required aria-required="true"  required autocomplete="materno">
            <label for="icon_prefix3" class="center-align {{$active}}"><h6>{{ __('Apellido Materno') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>


         <div class="input-field col s6">
            <input id="icon_prefix4" type="text" id="email" class="validate" name="email" 
            value='{{$email}}' required aria-required="true"  required autocomplete="email">
            <label for="icon_prefix4" class="center-align {{$active}}"><h6>{{ __('E-mail') }}</h6></label>
            <p class='erroremail text-center alert alert-danger hidden'></p>
        </div>

         <div class="input-field col s6">
            <input id="icon_prefix5" type="text" id="confirmacorreo" class="validate" name="confirmacorreo" 
            value='{{$confirmacorreo}}' required aria-required="true"  required autocomplete="confirmacorreo">
            <label for="icon_prefix5" class="center-align {{$active}}"><h6>{{ __('Confirma Email') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>

         <div class="input-field col s6">
            <input id="icon_prefix6" type="text" id="telefonoficina" class="validate" name="telefonoficina" 
            value='{{$telefonoficina}}' required aria-required="true"  required autocomplete="materno">
            <label for="icon_prefix6" class="center-align {{$active}}"><h6>{{ __('Telefono Oficina') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>

         <div class="input-field col s6">
            <input id="icon_prefix7" type="text" id="ext" class="validate" name="ext" 
            value='{{$ext}}' required aria-required="true"  required autocomplete="materno">
            <label for="icon_prefix7" class="center-align {{$active}}"><h6>{{ __('Ext') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>

         <div class="input-field col s6">
            <input id="icon_prefix8" type="text" id="celular" class="validate" name="celular" 
            value='{{$celular}}' required aria-required="true"  required autocomplete="celular">
            <label for="icon_prefix8" class="center-align {{$active}}"><h6>{{ __('Celular') }}</h6></label>
            <p class='errorcelular text-center alert alert-danger hidden'></p>
        </div>

                    <div class="form-group">
                       <label for="">hotel</label>
                       <select name="hotel_id" id="hotel_id" class="form-control"></select>
                        @foreach ($hotelusuarios as $hotelusuario)
                        <option value="{{ $hotelusuario['id'] }}">{{$hotelusuario['nombre']}}</option>
                        @endforeach
                    </div>


         <div class="input-field col s6">
            <input id="icon_prefix10" type="text" id="idperfil" class="validate" name="idperfil" 
            value='{{$Id_perfil}}' required aria-required="true"  required autocomplete="materno">
            <label for="icon_prefix10" class="center-align {{$active}}"><h6>{{ __('Id_perfil') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>


          <div class="input-field col s6">
            <input id="icon_prefix11" type="text" id="departamento" class="validate" name="departamento" 
            value='{{$departamento}}' required aria-required="true"  required autocomplete="departamento">
            <label for="icon_prefix11" class="center-align {{$active}}"><h6>{{ __('Departamento') }}</h6></label>
            <p class='errordepartamento text-center alert alert-danger hidden'></p>
          </div>


         <div class="input-field col s6">
            <input id="icon_prefix12" type="text" id="puesto" class="validate" name="puesto" 
            value='{{$puesto}}' required aria-required="true"  required autocomplete="puesto">
            <label for="icon_prefix12" class="center-align {{$active}}"><h6>{{ __('Puesto') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>


        <div class="col s6">
            <div class="switch">
                <label>
                        Status
                        <input id="status" type="checkbox" >
                        <span class="lever"></span>
                    </label>
                </div>
            </div>
        </div>        
    </div>
</div>
   
                  <!-- DIV del detalle para los usuarios -->
               <div class="row" id="detalle">
                    <div class="row">
                    
                      <div class="input-field col s6" >
                        <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                        <label for="#elegir"></label>
                        <input id="elegir" name="uploadedfile" type="file" /><br />
                      </div>
                 
            
                        <div class="input-field col s6" >
                          <input type="hidden" name="MAX_FILE_SIZE" value="100000" />
                          <label for="#elegir"></label>
                          <input id="elegir" name="uploadedfile" type="file" /><br />
                        </div>

                    </div>
                    </div>


               <div class="row" id="Accesso">


                  <div class="input-field col s3">
                    <input id="icon_prefix13" type="text" id="usuario" class="validate" name="usuario" 
                      value='{{$usuario}}' required aria-required="true"  required autocomplete="usuario" pattern="[a-z]{20}" title="solo acepto letras">
                      <label for="icon_prefix13" claxxss="center-align {{$active}}"><h6>{{ __('Usuario') }}</h6></label>
                      <p class='errordescripcion text-center alert alert-danger hidden'></p>
                  </div>

                  <div class="input-field col s3">
                    <input id="icon_prefix14" type="password" id="password" class="validate" name="password" 
                      value='' required aria-required="true"  required autocomplete="password">
                      <label for="icon_prefix14" class="center-align {{$active}}"><h6>{{ __('Password') }}</h6></label>
                      <p class='errordescripcion text-center alert alert-danger hidden'></p>
                  </div>

                  <div class="input-field col s3">
                    <input id="icon_prefix15" type="password" id="confirmapassword" class="validate" name="confirmapassword" 
                      value='' required aria-required="true"  required autocomplete="confirmapassword">
                      <label for="icon_prefix15" class="center-align {{$active}}"><h6>{{ __('Confirma-Password') }}</h6></label>
                      <p class='errorconfirmapassword text-center alert alert-danger hidden'></p>
                  </div>
               </div>



    </form>
</div>
<hr>
<div class ="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='Guardarusuarioshotel' value="agregar">

<label>
  <a href ="{{url('UsuariosHotel')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
</label>

</div>

