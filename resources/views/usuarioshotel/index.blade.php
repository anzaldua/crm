@extends('layouts.app2')
@section('container')

 <div class="row">
  <div class="col s12">
   <div id="icon-prefixes" class="card card-tabs">
      <div class="card-content">
    
        <!-- Botton del Modal-->
          <h4>{{ __('Usuarios Hotel') }}</h4>
            
            <a class="waves-effect waves-light btn  modal-trigger #1a237e indigo darken-4  lighten-2 right" data-target="modal" id="agregarusuariohotel" style='margin-top:15px;'><i class ="  fa fa-plus-circle" ></i>&nbsp;<span>{{ __('Usuario') }}</span></a>
  
        

        <!-- Modal Structure -->
          <div id="modal1" class="modal modal-fixed-footer">
          <div id="contenido-modal">
  
          </div>
          <!--Termina modal -->

          <!--Footer del modal-->    
          </div>
        
          <h4 class="card-title">{{ __('Lista de Usuarios') }}</h4>
               <div class="container">
              <div class="col s12" id="hotelusuarios">
            </div>
          </div>
        </div>
        </div>
      </div>
      </div>  
      
    @endsection
     @push('custom-scripts')
<script>
   $(document).ready(function(){
  
     

      function funcionx(id, el) {
      


    var statusCheck = ($("#status").is(":checked")) ? 1 : 0;
    var datos = $("form#usuarioshotel").serialize() + "&status=" + statusCheck;
    $.ajax({
      type: "POST",
      data: datos + "&id=" + id,
      url: "Guardarusuarioshotel",
      success: function (rp) {
         
         
        if (rp == "success") 
        {
            $('.tabs').tabs();

          tabla();
          el.modal("close");

        }
      
        },
  error: function (data) {
    if (data.status === 422) {
      var errors = $.parseJSON(data.responseText);
      $.each(errors, function (key, value) {
            // console.log(key+ " " +value);
            //$('#response').addClass("alert alert-danger");

            if ($.isPlainObject(value)) {
              $.each(value, function (key, value) {
                console.log(key + " " + value);
                alert(+ " " + value


                    )

            });
          } else {
              //$('#response').show().append(value + "<br/>"); //this is my div with messages
          }
          el.modal("close");
      });
  }
}
})
}

// peticion de la tablaCategorias
  function tabla() {


    $.ajax({

        type: "POST",
        url: "Tablausuarioshotel",
        data: "",
        success: function (r) {
           
            $('#hotelusuarios').html(r);
                //codigo de cambiar el color del status
               $('table#hotelusuarios .btn-status').each(function(){
                text = $(this).text();

                clase = (text.toUpperCase() == "ACTIVO")? "mb-6 btn waves-effect waves-light green darken-1 black" : "disabled";

                $(this).addClass(clase)

            })
            $('table#hotelusuarios').DataTable({
              paging: false,
              stateSave: true,
              "columnDefs": [ {
              "targets":4,
              "orderable": false,
              "searchable": false

                }]
           
            });

            $('.editarhotelusuarios').click(function () { //---BOTON  DE EDITAR---
              var id = $(this).parents("tr").attr("k");
              var el = $('#modal1').modal("open");
              $('#modal1 #contenido-modal').html("");
                    
                        $.ajax({
                        type: "POST",
                        url: "Agregarusuarioshotel",
                        data: {
                            id
                        },
                        success: function (r) {
                            $('#modal1 #contenido-modal').html(r);
                            $("#Guardarusuarioshotel").click(function ()
                             {funcionx(id,el)})
                                 
 

                        }


                    });

                });
            }
        });

       
}
tabla()
$('.modal').modal();

  // peticion de la tabla 

      $('.modal').modal();  

        $('#agregarusuariohotel').click(function () {

          var el = $('#modal1').modal("open");

            $('#modal1 #contenido-modal').html("");
              $.ajax({

                type: "POST",//tipo de envio
                url: "Agregarusuarioshotel",//url de donde enviamos los datos
                data: "id=0",
                success: function (r) {
              

                $('#modal1 #contenido-modal').html(r);
                $("#Guardarusuarioshotel").click(function () {
                    funcionx(0,el)
                  })
                
              }

             });

             });


           });

</script>

@endpush