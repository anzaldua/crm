 
<br>
<div class="col s12 ">
  <div class="row">

    <table id="hotelusuarios" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table "  >

      <thead>
        <tr bgcolor="#000080">
          <th><span style="color: #FFFFFF;">{{ __('#id') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Nombre') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Hotel')  }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('status') }} </span></th>
          <th><span style="color: #FFFFFF;"><i class="  fa fa-gear"></i>&nbsp;</th>

          </tr>
        </thead>
        <tbody> 
            @foreach($hotelusuarios as $usuariohotel)
              <tr k="{{$usuariohotel->id}}">
                <td>{{$usuariohotel->id }}   </td>
                <td>{{$usuariohotel->nombre}}</td>
                <td>{{$usuariohotel->hotel }}</td>
                <td> <button type="submit" name="usuario" value="agree" disabled id="usuario" class="btn btn-status"><span>{{$usuariohotel->status? 'Activo' : 'Inactivo'}}</span></button></td>

                <td>
                <a class="btn editarhotelusuarios waves-effect #01579b #d50000 red accent-4 btn-small"><i class="fa fa-pencil"></i></a> 
                <!-- Modal Structure -->
                <div id="modal2" class="modal">
                <div id="contenido-modal">
                </div>
                </div>
                </td>
              </tr>
        @endforeach
      </tbody>
    </table>
    
  </div>
</div>