@extends('layouts.app2')
@section('container')


 <div class="row">
  <div class="col s12">
   <div id="icon-prefixes" class="card card-tabs">
      <div class="card-content">
    
        <!-- Botton del Modal-->
          <h4>{{ __('Permisos') }}</h4>
            
            <a class="waves-effect waves-light btn  modal-trigger #1a237e indigo darken-4  lighten-2 right" data-target="modal" id="agregarpermisos" style='margin-top:15px;'><i class ="  fa fa-plus-circle" ></i>&nbsp;<span>{{ __('Usuario') }}</span></a>
  
        

        <!-- Modal Structure -->
          <div id="modal1" class="modal modal-fixed-footer">
          <div id="contenido-modal">
  
          </div>
          <!--Termina modal -->
   
          <style type="text/css">
          .modal { 
            width: 40% !important 
             }

             .modal 
             { width: 50% !important ;
              max-height: 50% !important
               } 

               .modal { width: 50% !important ; max-height: 40% !important ; }
        </style> 
          <!--Footer del modal-->    
          </div>
        
          <h4 class="card-title">{{ __('Lista de Usuarios') }}</h4>
               <div class="container">
              <div class="col s12" id="tabla-permisos">
            </div>
          </div>
        </div>
        </div>
      </div>
      </div>  
      


@endsection
   @push('custom-scripts')
<script>
   $(document).ready(function(){



      function funcionx(id, el) {



    var statusCheck = ($("#status").is(":checked")) ? 1 : 0;
    var datos = $("form#permisos").serialize() + "&status=" + statusCheck;
    $.ajax({
      type: "POST",
      data: datos + "&id=" + id,
      url: "guardarpermisos",
      success: function (rp) {



        if (rp == "success") {

          tabla();
          el.modal("close");

        }


      },
  error: function (data) {
    if (data.status === 422) {
      var errors = $.parseJSON(data.responseText);
      $.each(errors, function (key, value) {
            // console.log(key+ " " +value);
            //$('#response').addClass("alert alert-danger");

            if ($.isPlainObject(value)) {
              $.each(value, function (key, value) {
                console.log(key + " " + value);
                alert(+ " " + value


                    )

            });
          } else {
              //$('#response').show().append(value + "<br/>"); //this is my div with messages
          }
          el.modal("close");
      });
  }
}
})
}

// peticion de la tablaCategorias
  function tabla() {


    $.ajax({

        type: "POST",
        url: "tablapermisos",
        data: "",
        success: function (r) {

            $('#tabla-permisos').html(r);
            $('table#permisos').DataTable({
              paging: false,
              stateSave: true,
              "columnDefs": [ {
                "targets":2 ,
                "orderable": false,
                "searchable": false
                } ]
            });

                $('.editarpermisos').click(function () { //---BOTON  DE EDITAR---
                    var id = $(this).parents("tr").attr("k");
                    var el = $('#modal1').modal("open");
                    $('#modal1 #contenido-modal').html("");


                    $.ajax({
                        type: "POST",
                        url: "agregarpermisos",
                        data: {
                            id
                        },
                        success: function (r) {
                            $('#modal1 #contenido-modal').html(r);
                            $("#guardarpermisos").click(function ()
                             {funcionx(id,el)})



                        }


                    });

                });
            }
        });


}
tabla()
$('.modal').modal();

// peticion de la tabla

   $('.modal').modal();

   $('#agregarpermisos').click(function () {

        var el = $('#modal1').modal("open");

        $('#modal1 #contenido-modal').html("");
        $.ajax({

            type: "POST",//tipo de envio
            url: "agregarpermisos",//url de donde enviamos los datos
            data: "id=0",
            success: function (r) {


                $('#modal1 #contenido-modal').html(r);
                $("#guardarpermisos").click(function () {
                    funcionx(0,el)
                })

            }
        });

    });

});

</script>

@endpush