<?php
$count = count($corporativos);

$active = ($count)? "active" : "";

$nombre    = ($count)? $corporativos[0]->nombre    : "" ;

?>

<div class="modal-content">

      <table class="striped">
        <thead>
          <tr bgcolor="#000080">
              <th><span style="color: #FFFFFF;">{{ __('Nuevo Corporativo') }}</span></th>
              <th><span style="color: #FFFFFF;">Acciones</span></th>
              </tr>
          </thead>
      </table>
 <br>
 <br>
 
<form id='corporativos' class="formValidate" id="formValidate" method="get">
    <div class="input-field col s6">
            <input id="icon_prefix1" type="text" id="nombre" class="validate" name="nombre" 
            value='{{$nombre}}' required aria-required="true" placeholder="nombre" required autocomplete="nombre">
            <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('Nombre') }}</h6></label>
            <p class='errornombre text-center alert alert-danger hidden'></p>
            </div>

        <div class="col s6">
                <div class="switch">
                    <label>
                        Status
                        <input id="status" type="checkbox" >
                        <span class="lever"></span>
                    </label>
                </div>
            </div>
            
</form>
</div>

</div>
<hr>
<div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardarCorporativos' value="agregar">

<label>
  <a href ="{{url('Corporativo')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
</label>

</div>
