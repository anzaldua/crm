       <br>
              <div class="col s12 ">
                  <div class="row">
                    <table id="contactos" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">

                      <thead>
                        <tr bgcolor="#000080">
                        
                          <th><span style="color: #FFFFFF;">{{ __('Empresa') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Nombre')  }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Paterno') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Materno') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Email')}}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Direccion')}}</span></th>
     
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($contactos as $contacto)
                        <tr k="{{$contactos->contacto_id}}">
                          <td>{{ $contacto->empresa}}</td>
                          <td>{{ $contacto->nombre}}</td>
                          <td>{{ $contacto->paterno}}</td>
                          <td>{{ $contacto->materno}}</td>
                          <td>{{ $contacto->email}}</td>
                          <td>{{ $contacto->direccion}}</td>                
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                

