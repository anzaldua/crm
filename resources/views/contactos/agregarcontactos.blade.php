<?php
$count = count($contactos);

$active = ($count)? "active" : "";

$nombre    = ($count)? $contactos[0]->nombre  : "";
$titulo    = ($count)? $contactos[0]->titulo  : "";
$nombre    = ($count)? $contactos[0]->nombre  : "";
$paterno   = ($count)? $contactos[0]->paterno : "";
$materno   = ($count)? $contactos[0]->materno : "";
$estadocivil = ($count)? $contactos[0]->estadocivil : "";
$fechanacimiento = ($count)?  $contactos[0]->fechanacimiento : "";
$fechaimportante =  ($count)? $contactos[0]->fechaimportante : "";
$puesto          =  ($count)? $contactos[0]->puesto         : "";
$departamento    =  ($count)? $contactos[0]->departamento   : "";
$reserva         =  ($count)? $contactos[0]->reserva        : "";
$convenios       =  ($count)? $contactos[0]->convenios      : "";
$grupos          =  ($count)? $contactos[0]->grupos         : "";
$banquetes       =  ($count)? $contactos[0]->banquetes      : "";
$telefono        =  ($count)? $contactos[0]->telefono       : "";
$celular         =  ($count)? $contactos[0]->celular        : "";
$emailempresa    =  ($count)? $contactos[0]->emailempresa   : "";
$emailpersonal   =  ($count)? $contactos[0]->emailpersonal  : "";
$promediocn      =  ($count)? $contactos[0]->promediocn     : "";
$tarifa          =  ($count)? $contactos[0]->tarifa         : "";
$nivel           =  ($count)? $contactos[0]->nivel          : "";
$ejecutivos      =  ($count)? $contactos[0]->ejecutivos     : "";
$programadelealtad =($count)? $contactos[0]->programadelealtad : "";
$numeropl          = ($count)? $contactos[0]->numeropl         : "";   
$pasatiempo        = ($count)? $contactos[0]->pasatiempo       : "";
$asistente         = ($count)? $contactos[0]->asistente        : "";
$felicitacion      = ($count)? $contactos[0]->felicitacion     : "";
$tipofe            = ($count)? $contactos[0]->tipofe           : "";
$status            = ($count)? $contactos[0]->status           : "";
$empresa_id          = ($count)? $contactos[0]->empresa_id         : "";

?>

<div class="modal-content">

  <table class="striped">
    <thead>
      <tr bgcolor="#000080">
        <th><span style="color: #FFFFFF;">{{ __('Nuevo Contacto') }}</span></th>
  
      </tr>
    </thead>
  </table>
  <br>
  <br>

  <form id='contactos' class="formValidate" id="formValidate" method="get">
    <div class="input-field col s3">
      <select name="empresas" id="empresas" class="form-control">
        <option value="">Nombre Empresas</option>
        @foreach($empresas as $empresa)
        <option value="{{ $empresa['id'] }}">{{$empresa['nombreempresa']}}</option>
        @endforeach
      </select>
    </div>
<div class="row">
    <div class="input-field col s2">
      <input id="nombre" type="text" class="validate">
      <label for="nombre">Nombre</label>
  </div>

  <div class="input-field col s2">
      <input  id="paterno" type="text" class="validate">
      <label for="paterno">paterno</label>
  </div>

  <div class="input-field col s2">
      <input  id="materno" type="text" class="validate">
      <label for="materno">materno</label>
  </div>


    <div class="input-field col s2">
            <select id="titulo" name="titulo_id" data-error=".errorTxt6">
                <option value="">Titulo</option>
                @foreach($titulos as $titulo)
                <option value="{{ $titulo['id'] }}">{{$titulo['nombretitulo']}}</option>
                @endforeach
            </select>
        </div>


<div class="input-field col s2">
    <select id="estadocivil" name="estadocivil">
      <option value="Soltero(a)">Soltero(a)</option>
      <option value="Casado(a)">casado (a)</option>
      <option value="Divorciado(a)">Divorciado (a)</option>
      <option value= "Viudo(a)">Viudo (a)</option>
  </select>
  <label></label>
</div>

<div class="input-field col s2">
    <input  id="fechanacimiento" type="date" class="validate">
    <label for="fechanacimiento"></label>
</div> 



<div class="input-field col s2">
    <input  id="fechaImportante" type="date" class="validate">
    <label for="fechaImportante"></label>
</div>

<div class="input-field col s2">
    <input  id="tipofecha" type="date" class="validate">
    <label for="tipofecha"></label>
</div>

<div class="input-field col s2">
    <input id="puesto" type="text" class="validate">
    <label for="puesto">Puesto</label>
</div>

<div class="input-field col s2">
    <input id="departamento" type="text" class="validate">
    <label for="departamento">Departamento</label>
</div>

</div>

<div class="row">
    <p>
        <label>
            <input type="checkbox" />
            <span>Actividades</span>
        </label>

        <label>
            <input type="checkbox" />
            <span>Reservaciones</span>
        </label>

        <label>
            <input type="checkbox" />
            <span>Firma Convenios</span>
        </label>

        <label>
            <input type="checkbox" />
            <span>Cotiza Grupos</span>
        </label>

        <label>
            <input type="checkbox" />
            <span>Banquetes</span>
        </label>
    </p>
</div>

<div class="row">
  <div class="input-field col s2">
      <input id="telefono" type="text" class="validate">
      <label for="telefono">Telefono</label>
  </div>

  <div class="input-field col s2">
    <input id="ext" type="text" class="validate">
    <label for="ext">Exterior</label>
</div>

<div class="input-field col s2">
    <input id="celular" type="text" class="validate">
    <label for="celular">Celular</label>
</div>

<div class="input-field col s2">
    <input id="emailempresa" type="text" class="validate">
    <label for="emailempresa">E-mail Empresa</label>
</div>

<div class="input-field col s2">
    <input id="E-mail" type="text" class="validate">
    <label for="departamento">E-mail Personal</label>
</div>

<div class="input-field col s2">
    <input id="asistente" type="text" class="validate">
    <label for="Asistente">Asistente</label>
</div>

</div>

<div class="row">

   <div class="input-field col s2">
       <select>
          <option value="" disabled selected></option>
          <option value="1">Option 1</option>
          <option value="2">Option 2</option>
          <option value="3">Option 3</option>
      </select>
      <label>Promedio</label>
  </div>

  <div class="input-field col s2">
    <input id="prosupuesto" type="text" class="validate">
    <label for="prosupuesto">Prosupuesto/Tarifa</label>
</div>

<div class="input-field col s2">
    <select>
        <option value="" disabled selected></option>
        <option value="1">Option 1</option>
        <option value="2">Option 2</option>
        <option value="3">Option 3</option>
    </select>
    <label>Nivel</label>
</div>


<div class="input-field col s2">
   <select>
      <option value="" disabled selected></option>
      <option value="1">Option 1</option>
      <option value="2">Option 2</option>
      <option value="3">Option 3</option>
  </select>
  <label>Ejecutivos</label>
</div>

<div class="input-field col s2">
   <select>
      <option value="" disabled selected></option>
      <option value="1">Option 1</option>
      <option value="2">Option 2</option>
      <option value="3">Option 3</option>
  </select>
  <label>Programa de Lealtad</label>
</div>

<div class="input-field col s2">
    <input id="No.PL" type="text" class="validate">
    <label for="No.PL">No.PL</label>
</div>

<div class="input-field col s2">
    <input id="pasatiempo" type="text" class="validate">
    <label for="pasatiempo">pasatiempo</label>
</div>
</div>

  </form>
  </div>
  <hr>
  <div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardarCorporativos' value="agregar">
  <label>
  <a href ="{{url('Corporativo')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
  </label>
  </div>


<script type="text/javascript">
  
  $(document).ready(function(){
    $('select').formSelect();
  });
      
</script>