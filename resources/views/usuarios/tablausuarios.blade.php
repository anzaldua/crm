 
           <br>
              <div class="col s12 ">
                  <div class="row">
                    <table id="usuarios" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">

                      <thead>
                        <tr bgcolor="#000080">
                        
                          <th><span style="color: #FFFFFF;">{{ __('#id') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Nombre') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Paterno')}}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Materno')}}</span></th> 
                          <th><span style="color: #FFFFFF;">{{ __('Puesto')}}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Departamento')}}</span></th>                   
                          <th><span style="color: #FFFFFF;">{{ __('Perfil')}}</span></th>                   
                          <th><span style="color: #FFFFFF;">{{ __('Status')}}</span></th>
                          <th><span style="color: #FFFFFF;"><i class="fa fa-wrench"></i></span></th>                   
                   
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($usuarios as $usuario)
                        <tr  k="{{$usuario->id}}">
                          <td>{{ $usuario->id }}</td>
                          <td>{{ $usuario->nombre }}</td>
                          <td>{{ $usuario->paterno}}</td>
                          <td>{{ $usuario->materno}}</td>
                          <td>{{ $usuario->puesto->nombre }}</td>
                          <td>{{ $usuario->departamento->nombre }}</td>
                          <td>{{ $usuario->perfil->nombre}}</td>
                          <td>
                        
                           <button type="submit" name="usuario" value="agree" disabled id="usuario" class="btn btn-status"><span>{{$usuario->status? 'Activo' : 'Inactivo'}}</span></button></td>
                             
                          <td>
                          <a class="waves-#d50000 #ffd600 #ff6d00 orange accent-4-light btn"><i class="fa fa-gear"></i></a>
                           
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                

<style>

  #form1{
  margin-left:200px;
}
  .side-nav li a {
  text-decoration:none;
}
  #searchResults2{
  background-color:#666666;
}
  #searchResults{
  background-color:#666666;
}
  .right li a{
  padding-right:2em;
  text-decoration:none
}
</style>
