<?php

$count = count($usuarios);

$active = ($count)? "active" : "";

$nombre          = ($count)? $usuarios[0]->nombre  : "" ;
$paterno         = ($count)? $usuarios[0]->paterno : "" ;
$materno         = ($count)? $usuarios[0]->materno : "" ;
$usuario         = ($count)? $usuarios[0]->usuario : "" ;
$password        = ($count)? $usuarios[0]->password : "" ;
$fechanacimiento = ($count)? $usuarios[0]->fechanacimiento : "" ;
$departamento_id = ($count)? $usuarios[0]->departamento_id : "" ;
$puesto_id       = ($count)? $usuarios[0]->puesto_id : "" ;
$perfil_id       = ($count)? $usuarios[0]->perfil_id : "";
$email           = ($count)? $usuarios[0]->email : "" ;
$tipo            = ($count)? $usuarios[0]->tipo : "" ;
$id_categoria    = ($count)? $usuarios[0]->id_categoria : "" ;
$status          = ($count)? $usuarios[0]->status : "" ;
$id_corporativo  = ($count)? $usuarios[0]->id_corporativo : "" ;
$supervisado_por = ($count)? $usuarios[0]->supervisado_por : "" ;


?>


<div class="modal-content">
  @csrf
  <table class="striped">
        <thead>
          <tr bgcolor="#000080">
              <th><h5><span style="color: #FFFFFF;">{{ __('Nuevo Usuario') }}</span></h5></th>
              </tr>
          </thead>
      </table>
  <form id='usuarios' class="formValidate" id="formValidate" method="get">
    <br>


    @csrf
    <div class="row">
        <div class="input-field col s6">
            <input id="icon_prefix1" type="text" id="nombre" class="validate" name="nombre"
            value='{{$nombre}}' required aria-required="true"  required autocomplete="nombre">
            <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('Nombre') }}</h6></label>
            <p class='errornombre text-center alert alert-danger hidden'></p>
        </div>


        <div class="input-field col s6">
            <input id="icon_prefix2" type="text" id="paterno" class="validate" name="paterno"
            value='{{$paterno}}' required aria-required="true"  required autocomplete="paterno">
            <label for="icon_prefix2" class="center-align {{$active}}"><h6>{{ __('Apellido Paterno') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>

    </div>

    <div class="row">

        <div class="input-field col s6">
            <input id="icon_prefix3" type="text" id="materno" class="validate" name="materno"
            value='{{$materno}}' required aria-required="true"  required autocomplete="materno">
            <label for="icon_prefix3" class="center-align {{$active}}"><h6>{{ __('Apellido Materno') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>



        <div class="input-field col s6">
            <input id="icon_prefix222" type="text" id="usuario" class="validate" name="usuario"
            value='{{$usuario}}' required aria-required="true"  required autocomplete="usuario">
            <label for="icon_prefix222" class="center-align {{$active}}"><h6>{{ __('Usuario') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>
     </div>
     <div class="row">

            <div class="input-field col s6">
              <input id="icon_prefix6" type="date" id="fechanacimiento" class="vaidate" name="fechanacimiento"
             value='{{$fechanacimiento}}' required aria-required="true"  required autocomplete="fechanacimiento" >
             <label for="icon_prefix6" class="center-align {{$active}}"><h6>{{ __('fecha Nacimiento') }}</h6></label>
             <p class='errordepartamento text-center alert alert-danger hidden'></p>
        </div>

        <div class="input-field col s6">
            <select id="departamento" name="departamento_id" data-error=".errorTxt6">
                <option value="">Departamento</option>
                @foreach($departamentos as $departamento)
                <option value="{{ $departamento['id'] }}">{{$departamento['nombre']}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s6">
            <select id="puesto" name="puesto_id" data-error=".errorTxt6">
                <option value="">Puesto</option>
                @foreach($puestos as $puesto)
                <option value="{{ $puesto['id'] }}">{{$puesto['nombre']}}</option>
                @endforeach
            </select>
        </div>

        <div class="col s6">
            <select  id="crole" name="supervisado_por" data-error=".errorTxt6">
                <option value="" disabled selected>{{ __( 'Superivisado por') }}</option>
                <option value="1">{{ __('Alejandro Lozano') }}</option>
                <option value="2">{{ __('Ofelia Mendez') }} </option>
                <option value="3">{{ __('Serafin Ruiz  ') }}</option>
            </select>
        </div>

    </div>

    <div class="row">


        <div class="input-field col s6">
            <input id="icon_prefix29" type="text" id="email" class="validate" name="email"
            value='{{$email}}' required aria-required="true"  required autocomplete="email">
            <label for="icon_prefix29" class="center-align {{$active}}"><h6>{{ __('E mail') }}</h6></label>
            <p class='errorpuesto text-center alert alert-danger hidden'></p>
        </div>

        <div class="input-field col s6">
            <input id="icon_prefix227" type="text" id="" class="validate" name=""
            value='' required aria-required="true"  required autocomplete="">
            <label for="icon_prefix227" class="center-align {{$active}}"><h6>{{ __('Tel Oficina:') }}</h6></label>

            <p class='errorsupervisado_por text-center alert alert-danger hidden'></p>
        </div>

    </div>
    <div class="row">



        <div class="input-field col s6">
            <input id="icon_prefix228" type="text" id="" class="validate" name=""
            value='' required aria-required="true"  required autocomplete="">
            <label for="icon_prefix228" class="center-align {{$active}}"><h6>{{ __('Ext:') }}</h6></label>
            <p class='errorsupervisado_por text-center alert alert-danger hidden'></p>
        </div>


        <div class="input-field col s6">
            <select id="crole" name="perfil_id" data-error=".errorTxt6">
                <option value="">Perfil</option>
                @foreach($perfiles as $perfil)
                <option value="{{ $perfil['id'] }}">{{$perfil['nombre']}}</option>
                @endforeach
            </select>
        </div>
    </div>

</form>
</div>
<div class="modal-footer">
  <input type="submit" class="waves-effect #00e676 green accent-3 btn-small" id='guardarusuarios' value="agregar">

  <label>

      <a href ="{{url('usuarios.index')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
  </label>
</div>



<!-- escript para los select  anidados -->


<script>


  $(document).ready(function(){
    $('select').formSelect();
  });


    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, options);
    });


    $('#departamento').change(function(event){
        departamento = $(this).val();
        $.get("puestos/"+departamento+"",function(data,status){

            $response = JSON.parse(data);
            var contenido = "";

            $.each($response,function(index,object){

                contenido += "<option value ='"+object.id+"'>"+object.nombre+"</option>";

            })

            $("#puesto").html(contenido)

        });
    });

</script>
