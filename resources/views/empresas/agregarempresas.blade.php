
<?php
$count = count($empresas);

$active = ($count)? "active" : "";

$nombreempresa  = ($count)? $empresas[0]->nombreempresa : "" ;
$giro_id        = ($count)? $empresas[0]->giro_id : "";
$segmento_id    = ($count)? $empresas[0]->segmento_id : "";
$tipoempresas    = ($count)? $empresas[0]->tipoempresas : "";
$direccion      = ($count)? $empresas[0]->direccion   : "";
$colonia        = ($count)? $empresas[0]->colonia     : "";
$cp             = ($count)? $empresas[0]->cp          : "";
$direccionfiscal= ($count)? $empresas[0]->direccionfiscal: "";
$coloniafiscal  = ($count)? $empresas[0]->coloniafiscal  : "";
$cpfiscal       = ($count)? $empresas[0]->cpfiscal       : "";
$razonsocial    = ($count)? $empresas[0]->razonsocial    : "";
$rfcfiscal      = ($count)? $empresas[0]->rfcfiscal      : "";
$telefonoempresas = ($count)? $empresas[0]->telefonoempresas : "";
$paginaweb     = ($count) ? $empresas[0]->paginaweb      : "" ;
?>


<div class="modal-content">

  <table class="striped">
    <thead>
      <tr bgcolor="#000080">
        <th><span style="color: #FFFFFF;">{{ __('Nuevo Empresa') }}</span></th>
      </tr>
    </thead>
  </table>
  <br>
  <br>


  <form id='empresas' class="formValidate" id="formValidate" method="get">
    <div class="row">
      <div class="input-field col s3">
       <input id="icon_prefix1" type="text" id="nombreempresa" class="validate" name="nombreempresa" 
       value="{{$nombreempresa}}" required aria-required="true"placeholder="" required autocomplete="nombreempresa">
       <label for="icon_prefix1" class="center-align "><h6>{{ __('Nombre Empresa') }}</h6></label>
       <p class='errornombreempresa text-center alert alert-danger hidden'></p>
     </div> 

     <div class="input-field col s3">
      <select name="segmento" id="segmento" class="form-control">
        <option value="">segmento</option>
        @foreach($segmentos as $segmento)
        <option value="{{ $segmento['id'] }}">{{$segmento['nombresegmento']}}</option>
        @endforeach
      </select>
    </div>


    <div class ="input-field col s3">
      <select id="giro" name="giro_id">
        <option value ="">Giro</option>
        @foreach($giros as $giro)
        <option value ="{{$giro['id'] }}">{{$giro['nombre']}}</option>
        @endforeach
      </select>
    </div>


    <div class="input-field col s3">
     <select>
      <option value="" disabled selected>Seleccione Ejecutivo</option>
      <option value="1"></option>
      <option value="2"></option>
    </select>
  </div>
</div>

<!-- Termina el row de la primera parte  -->

<!-- Empieza el segundo row -->

<div class="row">
  <div class="input-field col s3">
    <select name="tipoempresas" id="tipoempresas">
      <option value="" disabled selected>Tipo Empresa</option>
      <option value="1">Cliente</option>
      <option value="0">Prospecto</option>
    </select>
  </div>

  <div class="input-field col s3">
    <input id="icon_prefix2" type="text" id="direccion" class="validate" name="direccion" 
    value="{{$direccion}}" required aria-required="true"placeholder="" required autocomplete="direccion">
    <label for="icon_prefix2" class="center-align "><h6>{{ __('Dirección') }}</h6></label>
    <p class='errordireccion text-center alert alert-danger hidden'></p>
  </div>


  <div class="input-field col s3">
   <input id="icon_prefix3" type="text" id="colonia" class="validate" name="colonia" 
   value="{{$colonia}}" required aria-required="true"placeholder="" required autocomplete="colonia">
   <label for="icon_prefix3" class="center-align "><h6>{{ __('Colonia') }}</h6></label>
   <p class='errorcolonia text-center alert alert-danger hidden'></p>
 </div>

 <div class="input-field col s3">
   <input id="icon_prefix4" type="text" id="cp" class="validate" name="cp" 
   value="{{$cp}}" required aria-required="true"  required autocomplete="cp">
   <label for="icon_prefix4" class="center-align "><h6>{{ __('CP') }}</h6></label>
   <p class='errorcp text-center alert alert-danger hidden'></p>
 </div>

 <!-- Termina el segundo row -->
</div>

<!-- empieza el tercer row -->

<div class="row">
   <div class="input-field col s3">
   <select>
    <option value="" disabled selected>País</option>
    <option value="1"></option>
    <option value="2"></option>
  </select>
</div>

<div class="input-field col s3">
 <select>
  <option value="" disabled selected>Estado</option>
  <option value="1"></option>
  <option value="2"></option>
</select>
</div>

<div class="input-field col s3">
  <select>
    <option value="" disabled selected>Ciudad</option>
    <option value="1"></option>
    <option value="2"></option>
  </select>
</div>
<!-- Temrina el row -->
</div>

<!-- Divicion del datos fiscales  -->
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6" style="background-image: url(images/barritahotel.png); background-repeat: repeat-x;" >Datos Fiscales</td></tr><!-- DATOS FISCALES -->
<tr><td align="left" colspan="2"><fieldset>Seleccione esta opci&oacute;n si los datos fiscales son iguales</td><td align="left"><input type="checkbox" onclick="javascript:repitedomicilio()"/></fieldset></td></tr>

<!-- Empieza el Cuarto row -->

<div class="row">
    <div class="input-field col s3">
    <input id="icon_prefix5" type="text" id="direccionfiscal" class="validate" name="direccionfiscal" 
    value="{{$direccionfiscal}}" required aria-required="true"  required autocomplete="direccionfiscal">
    <label for="icon_prefix5" class="center-align "><h6>{{ __('Direccion Fiscal') }}</h6></label>
    <p class='errordireccionfiscal text-center alert alert-danger hidden'></p>
  </div>

   <div class="input-field col s3">
    <input id="icon_prefix6" type="text" id="coloniafiscal" class="validate" name="coloniafiscal" 
    value="{{$coloniafiscal}}" required aria-required="true"  required autocomplete="coloniafiscal">
    <label for="icon_prefix6" class="center-align"><h6>{{ __('Colonia Fiscal') }}</h6></label>
    <p class='errorcoloniafiscal text-center alert alert-danger hidden'></p>
  </div>

  <div class="input-field col s3">
    <input id="icon_prefix7" type="text" id="cpfiscal" class="validate" name="cpfiscal" 
    value="{{$cpfiscal}}" required aria-required="true"  required autocomplete="cpfiscal">
    <label for="icon_prefix7" class="center-align"><h6>{{ __('CP Fiscal') }}</h6></label>
    <p class='errorcpfiscal text-center alert alert-danger hidden'></p>
  </div>


  <div class="input-field col s3">
    <input id="icon_prefix8" type="text" id="razonsocial" class="validate" name="razonsocial" 
    value="{{$razonsocial}}" required aria-required="true"  required autocomplete="razonsocial">
    <label for="icon_prefix8" class="center-align"><h6>{{ __('Razón Social') }}</h6></label>
    <p class='errorrazonsocial text-center alert alert-danger hidden'></p>
  </div>
   <!-- Temrina el row -->
</div>

<!-- Empieza el row -->
<div class="row">

   <div class="input-field col s3">
    <input id="icon_prefix9" type="text" id="razonsocial" class="validate" name="razonsocial" 
    value="{{$razonsocial}}" required aria-required="true"  required autocomplete="razonsocial">
    <label for="icon_prefix9" class="center-align"><h6>{{ __('Razón Social') }}</h6></label>
    <p class='errorrazonsocial text-center alert alert-danger hidden'></p>
  </div>

  <div class="input-field col s3">
    <input id="icon_prefix10" type="text" id="rfcfiscal" class="validate" name="rfcfiscal" 
    value="{{$rfcfiscal}}" required aria-required="true"  required autocomplete="rfcfiscal">
    <label for="icon_prefix10" class="center-align"><h6>{{ __('RFC') }}</h6></label>
    <p class='errorrfcfiscal text-center alert alert-danger hidden'></p>
  </div>


  <div class="input-field col s3">
   <select>
    <option value="" disabled selected> Seleccione País</option>
    <option value="1"></option>
    <option value="2"></option>
  </select>
</div>



<div class="input-field col s3">
  <select>
    <option value="" disabled selected> Seleccione Estado</option>
    <option value="1"></option>
    <option value="2"></option>
  </select>
</div>
  <!-- termina el row -->
</div>

 <!-- Nievo row -->
 <div class="row">

  <div class="input-field col s3">
  <select>
    <option value="" disabled selected> Seleccione Ciudad</option>
    <option value="1"></option>
    <option value="2"></option>
  </select>
</div>
 </div>
  
  <!-- Barra de division del contacto -->

<!-- Divicion del datos fiscales  -->
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6" style="background-image: url(images/barritahotel.png); background-repeat: repeat-x;" >Datos del Contacto</td></tr><!-- DATOS FISCALES -->
<tr><td align="left" colspan="2"><fieldset>Seleccione esta opci&oacute;n si los datos fiscales son iguales</td><td align="left"><input type="checkbox" onclick="javascript:repitedomicilio()"/></fieldset></td></tr>

<div class="row">
  <div class="input-field col s3">
    <input id="icon_prefix11" type="text" id="telefonoempresas" class="validate" name="telefonoempresas" 
    value="{{$telefonoempresas}}" required aria-required="true"  required autocomplete="telefonoempresas">
    <label for="icon_prefix11" class="center-align"><h6>{{ __('Telefono') }}</h6></label>
    <p class='errortelefono text-center alert alert-danger hidden'></p>
  </div>

  <div class="input-field col s3">
    <input id="icon_prefix12" type="text" id="paginaweb" class="validate" name="paginaweb" 
    value="{{$paginaweb}}" required aria-required="true"  required autocomplete="paginaweb">
    <label for="icon_prefix12" class="center-align"><h6>{{ __('Pagina Web') }}</h6></label>
    <p class='errorpaginaweb text-center alert alert-danger hidden'></p>
  </div>
<!-- termina el row -->
</div>
<!-- Divicion del datos fiscales  -->
<tr><td colspan="6">&nbsp;</td></tr>
<tr><td colspan="6" style="background-image: url(images/barritahotel.png); background-repeat: repeat-x;" >Gelocalizacion</td></tr><!-- DATOS FISCALES -->
<tr><td align="left" colspan="2"><fieldset>Seleccione esta opci&oacute;n si los datos fiscales son iguales</td><td align="left"><input type="checkbox" onclick="javascript:repitedomicilio()"/></fieldset></td></tr>
<br>
<br>


<div class="row">

  <tr><td align="left">Latitud:</td><td align="left"><input type="text" name="latitud" id="latitud" /></td><td align="left">Longitud:</td><td align="left"><input type="text" name="longitud" id="longitud" /></td><td align="left">Zoom</td>


  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3594.6414157665176!2d-100.20820348504142!3d25.71629398365745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662ea5a8d5bd18b%3A0x600adedb44021d96!2sEnlaceForte%20S.A.%20de%20C.V.!5e0!3m2!1ses-419!2smx!4v1578344478695!5m2!1ses-419!2smx" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

</div>

</form>
</div>
<hr>
<div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardargiros' value="agregar">

  <label>
    <a href ="{{url('segmentos')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
  </label>

</div>

<script>

  $(document).ready(function(){
    $('select').formSelect();
  });

</script>

