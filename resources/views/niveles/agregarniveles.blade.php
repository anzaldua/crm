
<?php
$count = count($niveles);

$active = ($count)? "active" : "";

$nombrenivel  = ($count)? $niveles[0]->nombrenivel : "" ;
?>


<div class="modal-content">
  <table class="striped">
    <thead>
      <tr bgcolor="#000080">
        <th><span style="color: #FFFFFF;">{{ __('Nuevo Empresa') }}</span></th>
      </tr>
    </thead>
  </table>
  <br>
  <br>


  <form id='empresas' class="formValidate" id="formValidate" method="get">
    <div class="row">
      <div class="input-field col s3">
       <input id="icon_prefix1" type="text" id="nombrenivel" class="validate" name="nombrenivel" 
       value="{{$nombrenivel}}" required aria-required="true"placeholder="" required autocomplete="nombrenivel">
       <label for="icon_prefix1" class="center-align "><h6>{{ __('Nombre  Nivel') }}</h6></label>
       <p class='errornombrenivel text-center alert alert-danger hidden'></p>
     </div> 
   </div>
</form>
</div>
<hr>
<div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardarniveles' value="agregarniveles">

  <label>
    <a href ="{{url('segmentos')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
  </label>

</div>

<script>

  $(document).ready(function(){
    $('select').formSelect();
  });

</script>

