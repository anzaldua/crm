     <br>
              <div class="col s12 ">
                  <div class="row">
                    <table id="niveles" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">

                      <thead>
                        <tr bgcolor="#000080">
                        
                          <th><span style="color: #FFFFFF;">{{ __('#ID') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Nombre Nivel')  }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Estrellas') }}</span></th>
     
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($niveles as $nivel) 
                        <tr k="{{$nivel->id}}">
                          <td> {{ $nivel->id}}</td>
                          <td> {{ $nivel->nombrenivel }}</td>
                          <td> {{ $nivel->estrellas   }}  </td>               
                        </tr>
                        @endforeach 
                      </tbody>
                    </table>
                  </div>
                </div>
                
