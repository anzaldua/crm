@extends('layouts.app2')
@section('container')

 <div class="row">
  <div class="col s12">
   <div id="icon-prefixes" class="card card-tabs">
      <div class="card-content">

        <!-- Botton del Modal-->
          <h4>{{ __('Departamentos') }}</h4>
            <a class="waves-effect waves-light btn  modal-trigger #1a237e indigo darken-4  lighten-2 right" data-target="modal" id="agregardepartamentos" style='margin-top:15px;'><i class ="  fa fa-plus-circle" ></i>&nbsp;<span>{{ __('Departamentos') }}</span></a>
            <a href=""></a>
  

        <!-- Modal Structure -->
          <div id="modal7" class="modal  modal-fixed-footer animated bounceInDown">
          <div id="contenido-modal">
  
          </div>

    <style type="text/css">
          .modal { 
            width: 40% !important 
             }

             .modal 
             { width: 50% !important ;
              max-height: 50% !important
               } 

               .modal { width: 50% !important ; max-height: 40% !important ; }
        </style> 
          <!--Termina modal -->

          <!--Footer del modal-->    
          </div>
        
          <h4 class="card-title">{{ __('Lista de departamentos') }}</h4>
          <div class="col s12 " id="tabla-departamentos">

                
                
                

            
            </div>
          </div>
        </div>
      </div>
      </div>  
      
      @endsection


@push('custom-scripts')
<script>
   $(document).ready(function(){
   
     

      function funcionx(id, el) {
    var statusCheck = ($("#status").is(":checked")) ? 1 : 0;
    var datos = $("form#departamentos").serialize() + "&status=" + statusCheck;
    $.ajax({
      type: "POST",
      data: datos + "&id=" + id,
      url: "Guardardepartamentos",
      success: function (rp) {

        if (rp == "success") {

          tabla();
          el.modal("close");

        }
      },
  error: function (data) {
    if (data.status === 422) {
      var errors = $.parseJSON(data.responseText);
      $.each(errors, function (key, value) {
            // console.log(key+ " " +value);
            //$('#response').addClass("alert alert-danger");

            if ($.isPlainObject(value)) {
              $.each(value, function (key, value) {
                console.log(key + " " + value);
                alert(+ " " + value


                    )

            });
          } else {
              //$('#response').show().append(value + "<br/>"); //this is my div with messages
          }
          el.modal("close");
      });
  }
}
})
}

// peticion de la tablaCategorias
  function tabla() {


    $.ajax({

        type: "POST",
        url: "Tabladepartamentos",
        data: "",
        success: function (r) {


            $('#tabla-departamentos').html(r);
                $('table#departamentos .btn-status').each(function(){
                text = $(this).text();

                clase = (text.toUpperCase() == "ACTIVO")? "mb-6 btn waves-effect waves-light green darken-1 black" : "disabled";

                $(this).addClass(clase)

            })
              $('table#departamentos').DataTable({
                  paging: false,
                  stateSave: true,
                  "columnDefs": [ {
                  "targets": 3,
                  "orderable": false,
                  "searchable": false
                } ]
            
        
            });
               
                $('.editardepartamentos').click(function () { //---BOTON  DE EDITAR---
                    var id = $(this).parents("tr").attr("k");
                    var el = $('#modal7').modal("open");
                    $('#modal7 #contenido-modal').html("");


                    $.ajax({
                        type: "POST",
                        url: "Agregardepartamentos",
                        data: {
                            id
                        },
                        success: function (r) {

                            $('#modal7 #contenido-modal').html(r);
                            $("#guardardepartamentos").click(function ()
                             {funcionx(id,el)})


                        }
                    });

                });
            }
        });

       
}
tabla()
$('.modal').modal();

// peticion de la tabla 

   $('.modal').modal();  

   $('#agregardepartamentos').click(function () {

        var el = $('#modal7').modal("open");

        $('#modal7 #contenido-modal').html("");
        $.ajax({

            type: "POST",//tipo de envio
            url: "Agregardepartamentos",//url de donde enviamos los datos
            data: "id=0",
            success: function (r) {
              

                $('#modal7 #contenido-modal').html(r);
                $("#guardardepartamentos").click(function () {
                    funcionx(0,el)
                })
                
            }
        });

    });
});

</script>

@endpush