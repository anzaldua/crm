<?php
$count = count($departamentos);

$active = ($count)? "active" : "";

$nombre = ($count)? $departamentos[0]->nombre  : "" ;
$status = ($count)? $departamentos[0]->status  : "" ;
$detalle = ($count)? $departamentos[0]->detalle : "" ;


?>

<div class="modal-content">

      <table class="striped">
        <thead>
          <tr bgcolor="#000080">
              <th><span style="color: #FFFFFF;">{{ __('Nuevo Departamento') }}</span></th>

              </tr>
          </thead>
      </table>
 <br>
 <br>
 
<form id='departamentos' class="formValidate" id="formValidate" method="get">
    <div class="input-field col s6">
            <input id="icon_prefix1" type="text" id="nombre" class="validate" name="nombre" 
            value='{{$nombre}}' required aria-required="true" placeholder="nombre" required autocomplete="nombre">
            <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('Nombre') }}</h6></label>
            <p class='errornombre text-center alert alert-danger hidden'></p>
            </div>

              <div class="input-field col s6">
            <input id="icon_prefix1" type="text" id="detalle" class="validate" name="detalle" 
            value='{{$detalle}}' required aria-required="true" placeholder="detalle" required autocomplete="detalle">
            <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('Detalle') }}</h6></label>
            <p class='errordetalle text-center alert alert-danger hidden'></p>
            </div>


      <div class="col s6">
                <div class="switch">
                    <label>
                        Status
                        <input id="status" type="checkbox" >
                        <span class="lever"></span>
                    </label>
                </div>
            </div> 

</form>
</div>

</div>

<div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardardepartamentos' value="agregar">

<label>
  <a href ="{{url('Departamento')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
</label>

</div>
