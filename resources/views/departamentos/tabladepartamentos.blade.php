 
           <br>
              <div class="col s12 ">
                  <div class="row">
                    <table id="departamentos" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">

                      <thead>
                        <tr bgcolor="#000080">
                        
                          <th><span style="color: #FFFFFF;">{{ __('#id') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Nombre Departamento') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Detalle') }}</span></th>     
     
                          <th><span style="color: #FFFFFF;">{{ __('Status') }}</span> </th>           
                          </tr>
                      </thead>

                      <tbody>
                          @foreach($departamentos as $departamento)
                        <tr  k="{{$departamento->id}}">
                          <td>{{ $departamento->id }}</td>
                          <td>{{ $departamento->nombre }}</td>
                          <td>{{ $departamento->detalle}}</td>
                        
                          <td>
                        
                           <button type="submit" name="departamento" value="agree" disabled id="departamento" class="btn btn-status"><span>{{$departamento->status? 'Activo' : 'Inactivo'}}</span></button></td>
                             

                          <td>
                                                     <!-- Modal Structure -->
                             <div id="modal12" class="modal">
                              <div id="contenido-modal">

                              </div>

                            </div>
                           
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                

