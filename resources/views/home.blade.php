@extends('layouts.app2')

@section('container')

<div class="slider">
    <ul class="slides">
      <li>
        <img src="../../images/gallery/crm.jpg"> <!-- random image -->
        <div class="caption center-align">
        	<header>
          <h3>Hola Bienvenido a Enlaceforte Bussines!</h3>
          <h5 class="light grey-text text-lighten-3">Tenemos gran variedad de paquetes .</h5>
      		</header>
        </div>
      </li>
      <li>
        <img src="https://lorempixel.com/580/250/nature/2"> <!-- random image -->
        <div class="caption left-align">
          <h3>paquetes ah! la playa!!</h3>
          <h5 class="light grey-text text-lighten-3">Que esperas reserva con Nosotros.!</h5>
        </div>
      </li>
      <li>
        <img src="https://lorempixel.com/580/250/nature/3"> <!-- random image -->
        <div class="caption right-align">
          <h3>Empresa con experiencia en el ramo hotelero</h3>
          <h5 class="light grey-text text-lighten-3">Contactanos .</h5>
        </div>
      </li>
      <li>
        <img src="https://lorempixel.com/580/250/nature/4"> <!-- random image -->
        <div class="caption center-align">
          <h3>siguenos en nuestras redes sociales !</h3>
          <h5 class="light grey-text text-lighten-3">.</h5>
        </div>
      </li>
    </ul>
  </div>
      

@endsection
