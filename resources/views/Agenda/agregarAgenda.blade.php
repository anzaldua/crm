<?php
$count = count($agendas);

$active = ($count)? "active" : "";
$active = ($count)? "active" : "";
$active = ($count)? "active" : "";


$nombre = ($count)? $categorias[0]->nombre : "" ;
$descripcion = ($count)? $categorias[0]->descripcion : "" ;
$tipo = ($count)? $categorias[0]->tipos : "" ;

?>
<div class="modal-content">
  @csrf

  <astrong><h4>{{ __('Grupo Enlaceforte') }}</h4>
           <h5>{{ __('Agregar Categoria') }}</h5>
  </astrong>

  <hr>

  <form id='agendas' class="formValidate" id="formValidate" method="get">
    <br>

    
    @csrf
  <div class="row">
            <div class="input-field col s6">
            <input id="icon_prefix1" type="text" id="nombre" class="validate" name="nombre" 
            value='{{$nombre}}' required aria-required="true" placeholder="nombre" required autocomplete="nombre">
            <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('Nombre') }}</h6></label>
            <p class='errornombre text-center alert alert-danger hidden'></p>
            </div>

    


        <div class="input-field col s6">
            <input id="icon_prefix2" type="text" id="descripcion" class="validate" name="descripcion" 
            value='{{$descripcion}}' required aria-required="true" placeholder="descripcion" required autocomplete="descripcion">
            <label for="icon_prefix2" class="center-align {{$active}}"><h6>{{ __('Descripcion') }}</h6></label>
            <p class='errordescripcion text-center alert alert-danger hidden'></p>
        </div>
        <!-- agregaremos la programacion del select -->
       <div class="input-field col s6">
            <input id="icon_prefix3" type="text" id="tipo" class="validate" name="tipo" 
            value='{{$tipo}}' required aria-required="true" placeholder="tipo" required autocomplete="tipo">
            <label for="icon_prefix3" class="center-align {{$active}}"><h6>{{ __('Tipo') }}</h6></label>
            <p class='errortipo text-center alert alert-danger hidden'></p>
        </div>

        <div class="col s6">
                <div class="switch">
                    <label>
                        Status
                        <input id="status" type="checkbox" >
                        <span class="lever"></span>
                    </label>
                </div>
            </div>
 </div>

</form>
</div>
<hr>
<div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardarcategorias' value="agregar">

<label>
  <a href ="{{url('categorias')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
</label>

</div>
