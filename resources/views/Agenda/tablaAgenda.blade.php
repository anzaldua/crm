  <div class="row">
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col s3"><a href="#test1">Test 1</a></li>
        <li class="tab col s3"><a class="active" href="#test2">Test 2</a></li>
        <li class="tab col s3 disabled"><a href="#test3">Disabled Tab</a></li>
        <li class="tab col s3"><a href="#test4">Test 4</a></li>
      </ul>
    </div>
    <div id="test1" class="col s12">  <table id="categorias" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">

    <thead>
        <tr bgcolor="#000080">

          <th><span style="color: #FFFFFF;">{{ __('#id') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Hora') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Fecha') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Contacto') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Empresa') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Tipo') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Vencimiento') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Status') }}</span>
          <th><span style="color: #FFFFFF;"><i class="fa fa-wrench"></i></span></th>


        </tr>
    </thead>

      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
           <td> <button type="submit" name="usuario" value="agree" disabled id="usuario" class="btn btn-status"><span></span></button></td>
          <td></td>

          <td>


           <a class="btn editarcategorias waves-effect #039be5 light-blue darken-1s btn-small"><i class="fa fa-pencil"></i></a> 
           <!-- Modal Structure -->
           <div id="modal12" class="modal">
            <div id="contenido-modal">

            </div>

          </div>

          <!-- Modal Trigger -->
          <a class="waves-effect waves-light #311b92 deep-purple darken-4 btn modal-trigger" data-target="modal" id="accion"><i class="fa fa-plus-circle"></i>Acciones</a>

          <!-- Modal Structure -->
          <div id="modal19" class="modal">
            <div id="contenido-modal">
             
            </div>
            <div class="modal-footer">
              <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
            </div>
          </div>
        </td>
      </tr>

    </tbody>
  </table>
</div>
    <div id="test2" class="col s12">Test 2</div>
    <div id="test3" class="col s12">Test 3</div>
    <div id="test4" class="col s12">Test 4</div>
  </div>
        

<script>
$(document).ready(function(){
$('.tabs').tabs();
});
     
</script>


