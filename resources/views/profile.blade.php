@extends('layouts.auth2')

@section('container')
<div class="container__inner content__inner--sm">
    <header class="container__title">
        <h1>{{ Auth::user()->name }}</h1>
        <small>{{ Auth::user()->position }}</small>
    </header>

    <div class="card profile">
        <div class="profile__img">
            <img src="demo/img/contacts/2.jpg" alt="">

            <a href="" class="zmdi zmdi-camera profile__img__edit"></a>
        </div>

        <div class="profile__info">
            <ul class="icon-list">
                <li><i class="zmdi zmdi-phone"></i> {{ Auth::user()->phone }}</li>
                <li><i class="zmdi zmdi-email"></i> {{ Auth::user()->email }}</li>
                <li><i class="zmdi zmdi-twitter"></i> {{ Auth::user()->twitter }}</li>
            </ul>
        </div>
    </div>

    <div class="card">
        <div class="card-body">

            <h4 class="card-body__title mb-4">Contact Information</h4>

            <ul class="icon-list">
                <li><i class="zmdi zmdi-phone"></i>{{ Auth::user()->phone }}</li>
                <li><i class="zmdi zmdi-email"></i>{{ Auth::user()->email }}</li>
                <li><i class="zmdi zmdi-facebook"></i>{{ Auth::user()->facebook }}</li>
                <li><i class="zmdi zmdi-twitter"></i>{{ Auth::user()->twitter }}</li>
                <li><i class="zmdi zmdi-pin"></i>{{ Auth::user()->address }}</li>
            </ul>
        </div>
    </div>
</div>
@endsection