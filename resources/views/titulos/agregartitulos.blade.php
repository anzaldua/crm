<?php
$count = count($titulos);

$active = ($count)? "active" : "";

$nombretitulo = ($count)? $titulos[0]->nombretitulo : "" ;

?>

<div class="modal-content">

  <table class="striped">
    <thead>
      <tr bgcolor="#000080">
        <th><span style="color: #FFFFFF;">{{ __('Nuevo titulo') }}</span></th>
      </tr>
    </thead>
  </table>
  <br>
  <br>

  <form id='titulos' class="formValidate" id="formValidate" method="get">
    <div class="row">
      <div class="input-field col s6">
        <input id="icon_prefix1" type="text" id="nombretitulo" class="validate" name="nombretitulo" 
        value='{{$nombretitulo}}' required aria-required="true"  required autocomplete="nombretitulo">
        <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('Nombre Titulo') }}</h6></label>
        <p class='errornombretitulo text-center alert alert-danger hidden'></p>
      </div>
    </div>

  </form>
</div>

</div>
<hr>
<div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardartitulos' value="agregar">

  <label>
    <a href ="{{url('titulos')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
  </label>

</div>

<script type="text/javascript">

  $(document).ready(function(){
    $('select').formSelect();
  });

</script>