      <br>
              <div class="col s12 ">
                  <div class="row">
                  
                    <table id="titulos" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">
      
                      <thead>
                        <tr bgcolor="#000080">
                          <th><span style="color: #FFFFFF;">{{ __('#id Titulo') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Nombre Titulo ') }}</span></th>
                         
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($titulos as $titulo)
                        <tr  k="{{$titulo->id}}">
                          <td>{{ $titulo->titulos_id}}</td>
                          <td>{{ $titulo->nombretitulo}}</td>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
    
                  </div>
                </div>
                