 
<br>
<div class="col s12 ">
  <div class="row">
    <table id="categorias" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">

    <thead>
        <tr bgcolor="#000080">

          <th><span style="color: #FFFFFF;">{{ __('#id') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Categorias') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Detalle') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Status') }}</span></th>
          <th><span style="color: #FFFFFF;">{{ __('Tipo')}}</span></th>
          <th><span style="color: #FFFFFF;"><i class="fa fa-wrench"></i></span></th>


        </tr>
    </thead>

      <tbody>
        @foreach($categorias as $categoria)
        <tr  k="{{$categoria->id}}">

          <td>{{ $categoria->id }}</td>
          <td>{{ $categoria->nombre }}</td>
          <td>{{ $categoria->descripcion }}</td>
           <td> <button type="submit" name="usuario" value="agree" disabled id="usuario" class="btn btn-status"><span>{{$categoria->status? 'Activo' : 'Inactivo'}}</span></button></td>
          <td>{{ $categoria->tipo}}</td>

          <td>


           <a class="btn editarcategorias waves-effect #039be5 light-blue darken-1s btn-small"><i class="fa fa-pencil"></i></a> 
           <!-- Modal Structure -->
           <div id="modal12" class="modal">
            <div id="contenido-modal">

            </div>

          </div>

          <!-- Modal Trigger -->
          <a class="waves-effect waves-light #311b92 deep-purple darken-4 btn modal-trigger" data-target="modal" id="accion"><i class="fa fa-plus-circle"></i>Acciones</a>

          <!-- Modal Structure -->
          <div id="modal19" class="modal">
            <div id="contenido-modal">
             
            </div>
            <div class="modal-footer">
              <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
            </div>
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>

<script>
  $(document).ready(function(){
    $('.modal').modal();
    $('#accion') .click(function(){
       var el = $('#modal19').modal("open");

       $('#modal19 #contenido-modal').html("");
           $.ajax({

            type: "POST",//tipo de envio
            url: "accionescategorias",//url de donde enviamos los datos
            data: "",
            success: function (r) {
              

                $('#modal19 #contenido-modal').html(r);
                $("#").click(function () {
                    funcionx(0,el)
                })
                
            }
        });

    });
  });
     
</script>


