@extends('layouts.app2')
@section('container')

 <div class="row">
  <div class="col s12">
   <div id="icon-prefixes" class="card card-tabs">
      <div class="card-content">

        <!-- Botton del Modal-->
          <h4>{{ __('Puestos') }}</h4>
            <a class="waves-effect waves-light btn  modal-trigger #1a237e indigo darken-4  lighten-2 right" data-target="modal" id="agregarpuestos" style='margin-top:15px;'><i class ="  fa fa-plus-circle" ></i>&nbsp;<span>{{ __('Puestos') }}</span></a>
            <a href=""></a>
  

        <!-- Modal Structure -->
          <div id="modal8" class="modal modal-fixed-footer animated bounceInDown">
          <div id="contenido-modal">
  
          </div>

          <style type="text/css">
          .modal { 
            width: 40% !important 
             }

             .modal 
             { width: 50% !important ;
              max-height: 50% !important
               } 

               .modal { width: 50% !important ; max-height: 40% !important ; }
        </style> 
          <!--Termina modal -->

          <!--Footer del modal-->    
          </div>
        
          <h4 class="card-title">{{ __('Lista de Puestos') }}</h4>
          <div class="col s12 " id="tabla-puestos">


            </div>
          </div>
        </div>
      </div>
      </div>  
      
      @endsection


@push('custom-scripts')
<script>
   $(document).ready(function(){
   
     

      function funcionx(id, el) {
    var statusCheck = ($("#status").is(":checked")) ? 1 : 0;
    var datos = $("form#puestos").serialize() + "&status=" + statusCheck;
    $.ajax({
      type: "POST",
      data: datos + "&id=" + id,
      url: "Guardarpuestos",
      success: function (rp) {

        if (rp == "success") {

          tabla();
          el.modal("close");

        }
      },
  error: function (data) {
    if (data.status === 422) {
      var errors = $.parseJSON(data.responseText);
      $.each(errors, function (key, value) {
            // console.log(key+ " " +value);
            //$('#response').addClass("alert alert-danger");

            if ($.isPlainObject(value)) {
              $.each(value, function (key, value) {
                console.log(key + " " + value);
                alert(+ " " + value


                    )

            });
          } else {
              //$('#response').show().append(value + "<br/>"); //this is my div with messages
          }
          el.modal("close");
      });
  }
}
})
}

// peticion de la tablaCategorias
  function tabla() {


    $.ajax({

        type: "POST",
        url: "Tablapuestos",
        data: "",
        success: function (r) {


            $('#tabla-puestos').html(r);
                $('table#puestos .btn-status').each(function(){
                text = $(this).text();

                clase = (text.toUpperCase() == "ACTIVO")? "mb-6 btn waves-effect waves-light green darken-1 black" : "disabled";

                $(this).addClass(clase)

            })
              $('table#puestos').DataTable({
                  paging: false,
                  stateSave: true,
                  "columnDefs": [ {
                  "targets": 2,
                  "orderable": false,
                  "searchable": false
                } ]
            
        
            });
               
                $('.editardepartamentos').click(function () { //---BOTON  DE EDITAR---
                    var id = $(this).parents("tr").attr("k");
                    var el = $('#modal8').modal("open");
                    $('#modal8 #contenido-modal').html("");


                    $.ajax({
                        type: "POST",
                        url: "Agregarpuestos",
                        data: {
                            id
                        },
                        success: function (r) {

                            $('#modal8 #contenido-modal').html(r);
                            $("#guardarpuestos").click(function ()
                             {funcionx(id,el)})


                        }
                    });

                });
            }
        });

       
}
tabla()
$('.modal').modal();

// peticion de la tabla 

   $('.modal').modal();  

   $('#agregarpuestos').click(function () {

        var el = $('#modal8').modal("open");

        $('#modal8 #contenido-modal').html("");
        $.ajax({

            type: "POST",//tipo de envio
            url: "Agregarpuestos",//url de donde enviamos los datos
            data: "id=0",
            success: function (r) {
              

                $('#modal8 #contenido-modal').html(r);
                $("#guardarpuestos").click(function () {
                    funcionx(0,el)
                })
                
            }
        });

    });
});

</script>

@endpush