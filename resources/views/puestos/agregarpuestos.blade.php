<?php
$count = count($puestos);

$active = ($count)? "active" : "";

$nombre = ($count)? $puestos[0]->nombre : "" ;
$departamento_id =($count)? $puestos[0]->departamento_id : "";

?>

<div class="modal-content">

      <table class="striped">
        <thead>
          <tr bgcolor="#000080">
              <th><span style="color: #FFFFFF;">{{ __('Nuevo Puesto') }}</span></th>
              </tr>
          </thead>
      </table>
 <br>
 <br>
 
<form id='puestos' class="formValidate" id="formValidate" method="get">
    <div class="row">
          <div class="input-field col s6">
            <input id="icon_prefix1" type="text" id="nombre_puesto" class="validate" name="nombre" 
            value='{{$nombre}}' required aria-required="true" placeholder="nombre_puesto" required autocomplete="nombre">
            <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('Nombre') }}</h6></label>
            <p class='errornombre_puesto text-center alert alert-danger hidden'></p>
            </div>
             
          <div class="input-field col s6">
  
                <select  id="select-project" name="departamento_id" >
                    <option value="">Departamento</option>
                    @foreach($departamentos as $departamento)
                    <option value="{{ $departamento['id'] }}">{{$departamento['nombre']}}</option>
                    @endforeach
                </select>
                  
          </div>
</div>

</form>
</div>

</div>
<hr>
<div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardarpuestos' value="agregar">

<label>
  <a href ="{{url('Puestos')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
</label>

</div>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('select').formSelect();
  });
      
</script>