
           <br>
              <div class="col s12 ">
                  <div class="row">
                  
                    <table id="puestos" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">
      
                      <thead>
                        <tr bgcolor="#000080">
                          <th><span style="color: #FFFFFF;">{{ __('#id') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Nombre ') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Departamento')  }}</span></th>
                         
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($puestos as $puesto)
                        <tr  k="{{$puesto->id}}">
                          <td>{{ $puesto->id }}</td>
                          <td>{{ $puesto->nombre}}</td>
                          <td>{{ $puesto->departamento->nombre }}</td>

                

                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
    
                  </div>
                </div>
                