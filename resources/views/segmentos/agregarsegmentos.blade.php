<?php
$count = count($segmentos);

$active = ($count)? "active" : "";
$nombresegmento = ($count)? $segmentos[0]->nombresegmento : "" ;

?>

<div class="modal-content">

      <table class="striped">
        <thead>
          <tr bgcolor="#000080">
              <th><span style="color: #FFFFFF;">{{ __('Nuevo segmento') }}</span></th>
              </tr>
          </thead>
      </table>
 <br>
 <br>
 
<form id='segmentos' class="formValidate" id="formValidate" method="get">
    <div class="row">
          <div class="input-field col s6">
            <input id="icon_prefix1" type="text" id="nombresegmento" class="validate" name="nombresegmento" 
            value='{{$nombresegmento}}' required aria-required="true"  required autocomplete="nombresegmento">
            <label for="icon_prefix1" class="center-align"><h6>{{ __('Nombre') }}</h6></label>
            <p class='errornombre_puesto text-center alert alert-danger hidden'></p>
            </div>
             
  
</div>

</form>
</div>

</div>
<hr>
<div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardarsegmentacion' value="agregar">

<label>
  <a href ="{{url('segmentos')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
</label>

</div>

