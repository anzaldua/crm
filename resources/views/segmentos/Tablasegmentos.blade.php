
           <br>
              <div class="col s12 ">
                  <div class="row">
                  
                      
                      <table id="segmentos" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">
                      <thead>
                        <tr bgcolor="#000080">
                          <th><span style="color: #FFFFFF;">{{ __('#idsegmento') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Nombre Segmento ') }}</span></th>
                          <th><span style="color: #FFFFFF;"><i class="fa fa-wrench"></i></span></th>
                           
                        </tr>
                      </thead>
                      <tbody>
                       @foreach($segmentos as $segmento)
                        <tr  k="{{$segmento->id}}">
                          <td>{{$segmento->id}}</td>
                          <td>{{$segmento->nombresegmento}}</td>
                          <td><a class="btn editarpermisos waves-effect #01579b #d50000 #00b0ff light-blue accent-3 btn-small"><i class="  fa fa-edit"></i></a>
                              <a class="btn  waves-effect #01579b #d50000 red accent-4 btn-small"><i class="fa fa-trash"></i></a></
                          </td>
                          
                        </tr>
                      
                      </tbody>
                      @endforeach

                    </table>
    
                  </div>
                </div>
                