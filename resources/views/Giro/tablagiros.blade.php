
           <br>
              <div class="col s12 ">
                  <div class="row">
                  
                    <table id="giros" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">
      
                      <thead>
                        <tr bgcolor="#000080">
                          <th><span style="color: #FFFFFF;">{{ __('#id Giro') }}</span></th>
                          <th><span style="color: #FFFFFF;">{{ __('Nombre Giro ') }}</span></th>
                           <th><span style="color: #FFFFFF;">{{ __('Segmento') }}</span></th>
                       
                           
                        </tr>
                      </thead>
                      <tbody>
                      @foreach($giros as $giro)
                        <tr  k="{{$giro->id}}">
                          <td>{{ $giro->id}}</td>
                          <td>{{ $giro->nombre}}</td>
                          <td>{{ $giro->segmento->nombresegmento}}</td>
                          
                          
                        </tr>
                      
                      </tbody>
               @endforeach

                    </table>
    
                  </div>
                </div>
                