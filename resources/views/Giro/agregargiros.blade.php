<?php
$count = count($giros);

$active = ($count)? "active" : "";

$nombre = ($count)? $giros[0]->nombre : "" ;


?>

<div class="modal-content">

      <table class="striped">
        <thead>
          <tr bgcolor="#000080">
              <th><span style="color: #FFFFFF;">{{ __('Nuevo segmento') }}</span></th>
              </tr>
          </thead>
      </table>
 <br>
 <br>
 
<form id='giros' class="formValidate" id="formValidate" method="get">
    <div class="row">
                <div class="input-field col s6">
                <input id="icon_prefix1" type="text" id="nombre" class="validate" name="nombre" 
                value='{{$nombre}}' required aria-required="true" required autocomplete="nombre">
                <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('Nombre') }}</h6></label>
                <p class='errornombre text-center alert alert-danger hidden'></p>
                </div>

              <div class="input-field col s3">
                  <select  id="select-project" name="segmento_id" >
                    <option value="">segmento</option>
                    @foreach($segmentos as $segmento)
                <option value="{{ $segmento['id'] }}">{{$segmento['nombresegmento']}}</option>
                @endforeach
            </select>

          </div>
  
</div>

</form>
</div>

</div>
<hr>
<div class="modal-footer">
  <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardargiros' value="agregar">

<label>
  <a href ="{{url('segmentos')}}"  class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
</label>

</div>


<script>
$(document).ready(function(){
$('select').formSelect();
});
</script>