@extends('layouts.auth')

@section('container')
<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 1-column register-bg  blank-page blank-page" data-open="click" data-menu="vertical-modern-menu" data-col="1-column">
    <div class="row">
      <div class="col s12">
        <div class="container"><div id="register-page" class="row" >

            
         <div class="row">
    <div class="col s12 m12 l12">
      <div id="gradient-button-with-shadow" class="card bg-opacity-8t" style="right:90px">
        <div class="card-content">
          <div class="row">
            <div class="col s12 mb-2">
                        </div>
            <div class="col s12">
              <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">CRM</a>
             <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">Nuestro Grupo</a>
                <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">Cotizaciones</a>
                   <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">Contacto</a>
                      <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">Soporte</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class=" col l6 m10 ms12">
      <div class="slider">
    <ul class="slides">
      <li>
        <img src="../../images/gallery/crm.jpg"> <!-- random image -->
        <div class="caption center-align">
          <header>
          <h3>Hola Bienvenido a Enlaceforte Bussines!</h3>
          <h5 class="light grey-text text-lighten-3">Tenemos gran variedad de paquetes .</h5>
          </header>
        </div>
      </li>
      <li>
        <img src="https://lorempixel.com/580/250/nature/2"> <!-- random image -->
        <div class="caption left-align">
          <h3>paquetes ah! la playa!!</h3>
          <h5 class="light grey-text text-lighten-3">Que esperas reserva con Nosotros.!</h5>
        </div>
      </li>
      <li>
        <img src="https://lorempixel.com/580/250/nature/3"> <!-- random image -->
        <div class="caption right-align">
          <h3>Empresa con experiencia en el ramo hotelero</h3>
          <h5 class="light grey-text text-lighten-3">Contactanos .</h5>
        </div>
      </li>
      <li>
        <img src="https://lorempixel.com/580/250/nature/4"> <!-- random image -->
        <div class="caption center-align">
          <h3>siguenos en nuestras redes sociales !</h3>
          <h5 class="light grey-text text-lighten-3">.</h5>
        </div>
      </li>
    </ul>
  </div>
          </div>

</div>

            <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 register-card bg-opacity-8" style=>

                <div class="row">
                    <div class="input-field col s12">
                     <h5 class="ml-4">Nuevo Admin</h5>
                    <p class="ml-4">¡Únete a nuestra comunidad ahora!</p>
                </div>

                </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                                @csrf

                            <div class="row margin">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix pt-2">person_outline</i>
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                        <label for="name" class="center-align">{{ __('name') }}</label>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                </div>


                             </div>

                                <div class="row margin">
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix pt-2">mail_outline</i>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                        <label for="email">{{ __('email') }}</label>
                                            @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                         @enderror
                                    </div>
                                </div>


                            <div class="row margin">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix pt-2">lock_outline</i>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    <label for="password">{{ __('password') }}</label>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="row margin">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix pt-2">lock_outline</i>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    <label for="password-confirm">{{ __('password-confirm') }}</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <button type="submit" class="btn waves-effect waves-light #6200ea deep-purple accent-4  col s12" >
                                    {{ __('Iniciar Sesión') }}
                                    </button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="input-field col s12">
                                    <p class="margin medium-small"><a href="{{ route('login') }}">¿Ya tienes una cuenta? Inicia sesión</a></p>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
