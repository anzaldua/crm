
@extends('layouts.auth')

@section('container')
<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 1-column login-bg  blank-page  blank-page" data-open="click" data-menu="orizontal-modern-menu" data-col="1-column">
          <br>
          <br>
             

      <div class="row">
        <div class="col s12">
          <div class="container"><div id="login-page" class="row">
                          <!-- Bottones de entrada  -->

         <div class="row">
    <div class="col s12 m12 l12">
      <div id="gradient-button-with-shadow" class="card bg-opacity-8t" style="right:90px">
        <div class="card-content">
          <div class="row">
            <div class="col s12 mb-2">
                        </div>
            <div class="col s12">
              <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">CRM</a>
             <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">Nuestro Grupo</a>
                <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">Cotizaciones</a>
                   <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">Contacto</a>
                      <a class="waves-effect waves-light btn gradient-45deg-light-blue-cyan z-depth-4 mr-1 mb-2">Soporte</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class=" col l12 m10 ms12" >
      <div class="slider l12"style="right: 90px">
    <ul class="slides">
      <li>
        <img src="{{asset ('app-assets/images/avatar/intro-features.png') }}"> <!-- random image -->
        <div class="caption center-align">
          <header>
          <h3>Hola Bienvenido a Enlaceforte Bussines!</h3>
          <h5 class="light grey-text text-lighten-3">Tenemos gran variedad de paquetes .</h5>
          </header>
        </div>
      </li>
      <li>
        <img src="{{asset ('app-assets/images/avatar/intro-features.png') }}"> <!-- random image -->
        <div class="caption left-align">
          <h3>paquetes ah! la playa!!</h3>
          <h5 class="light grey-text text-lighten-3">Que esperas reserva con Nosotros.!</h5>
        </div>
      </li>
      <li>
        <img src="{{asset ('app-assets/images/avatar/avatar-1.png') }}"> <!-- random image -->
        <div class="caption right-align">
          <h3>Empresa con experiencia en el ramo hotelero</h3>
          <h5 class="light grey-text text-lighten-3">Contactanos .</h5>
        </div>
      </li>
      <li>
        <img src="{{asset ('app-assets/images/avatar/avatar-1.png') }}"> <!-- random image -->
        <div class="caption center-align">
          <h3>siguenos en nuestras redes sociales !</h3>
          <h5 class="light grey-text text-lighten-3">.</h5>
        </div>
      </li>
    </ul>
  </div>
          </div>

</div>


            <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">

       
            <!--FORMULARIO DE LOGIN -->
              <form method="POST" action="{{ route('login') }}">
                  @csrf
                <div class="row">
                  <div class="input-field col s12">
                    <h5 class="ml-4">Inicia Session</h5>
                  </div>
                  <div class="row margin">
                    <div class="input-field col s12">
                      <i class="material-icons prefix pt-2">person_outline</i>
                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        <label for="email"  class="center-align">{{ __('Correo') }}</label>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                  </div>

                  <div class="row margin">
                    <div class="input-field col s12">
                      <i class="material-icons prefix pt-2">lock_outline</i>
                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                          <label for="password">{{ __('Contraseña') }}</label>
                          @error('password')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                          </span>
                            @enderror
                    </div>
                  </div>
                          
                  <div class="row">
                    <div class="col s12 m12 l12 ml-2 mt-1">
                      <p>
                      <!--<label>
                      <input type="checkbox" />
                      <span>Recuérdame</span>
                      </label>-->
                      </p>
                    </div>
                   </div>

                       
              <div class="row">
                <div class="input-field col s12">
                  <button type="submit" class="btn waves-effect waves-light border-round  #6200ea deep-purple accent-4 col s12">
                    {{ __('Iniciar Sesión') }}
                  </button>
                </div>
              </div>

              <div class="input-field col s6 m6 l6">
                <p class="margin medium-small"><a href="{{ route('register') }}">{{ __('Agregar Admin') }}</a></p>
                  </div>
                    <div class="input-field col s6 m6 l6">
                      @if (Route::has('password.request'))
                        <a class="margin right-align medium-small" href="{{ route('password.request') }}">
                          {{ __('¿Olvidaste tu contraseña?') }}
                        </a>
                       @endif
                    </div>

              </form> 
              <!--TERMINA EL FORMULARIO DEL LOGIN-->
             </div>
          </div>
        </div>
      </div>
    </div>
  </body>


  @endsection