@extends('layouts.auth')

@section('container')

<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 1-column forgot-bg  blank-page blank-page" data-open="click" data-menu="vertical-modern-menu" data-col="1-column">
        <br>
          <br>
    <div class="row">
      <div class="col s12">
        <div class="container"><div id="email-password" class="row">
          <div class="col s12 m6 l4 z-depth-4 offset-m4 card-panel border-radius-6 email-card bg-opacity-8">
          <div class="row">
          <div class="input-field col s12">
          <h5 class="ml-4">Se te olvidó tu contraseña</h5>
          <p class="ml-4">Puedes restablecer tu contraseña</p>
        </div>
      </div>

                        <div class="row">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                 
                            <form  class="logout" method="POST" action="{{ route('password.email') }}">
                          @csrf


                            <div class="row">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix pt-2">person_outline</i>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    <label for="email" class="center-align">{{ __('correo') }}</label>

                                      @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                       <div class="row">
                                        <div class="input-field col s12">
                                          <button type="submit" class="btn waves-effect waves-light border-round #6200ea deep-purple accent-4 col s12">
                                            {{ __('Enviar enlace de restablecimiento de contraseña') }}
                                                </button>
                                        </div>
                                    </div>
                            <div class="row">
                            <div class="input-field col s6 m6 l6">
                            <p class="margin medium-small"><a href="{{ route('login') }}">{{ __('Iniciar Sesión') }}</a></p>
                            </div>
                    
                          <div class="input-field col s6 m6 l6">
                            <p class="margin right-align medium-small"><a href="{{ route('register') }}">{{ __('Registrate') }}</a></p>
                            </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection