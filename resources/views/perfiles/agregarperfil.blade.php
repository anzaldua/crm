

<?php
$count = count($perfiles);

$active = ($count)? "active" : "";

$nombre      = ($count)? $perfiles[0]->nombre : "" ;
$descripcion = ($count)? $perfiles[0]->descripcion : "";
?>




<div class="modal-content">




  <form id='perfiles' class="formValidate" id="formValidate" method="get">
    
    <div class="row">
      <div class="col s12">
        <ul class="tabs">
          <li class="tab col s4"><a href="#test0">Información general</a></li>
          <li class="tab col s4"><a href="#test1">Rutas</a></li>
          <li class="tab col s4"><a class="active" href="#test2">Permisos Adicionales</a></li>
          
          
        </ul>
      </div>
      <div id="test0" class="col s12">
          <div class="row">
              <div class="input-field col s6">
                <input id="icon_prefix101" type="text" id="nombre_puesto" class="validate" name="nombre" 
                  required aria-required="true" required autocomplete="nombre">
                <label for="icon_prefix101" class="center-align ">
                  
                </label>
                <p class='errornombre_puesto text-center alert alert-danger hidden'></p>
              </div>


            </div>

                  <div class="row">
              <div class="input-field col s6">
                <input id="icon_prefix101" type="text" id="nombre_puesto" class="validate" name="nombre" 
                  required aria-required="true" required autocomplete="descripcion">
                <label for="icon_prefix101" class="center-align ">
                  
                </label>
                <p class='errornombre_puesto text-center alert alert-danger hidden'></p>
              </div>


            </div>
      </div>
      <div id="test1" class="col s12">

          <ul class="collapsible">
            @foreach ($rutas as $categoria)
                
            <li>
              <div class="collapsible-header"><i class="material-icons">filter_drama</i>{{$categoria["categoria"]}}</div>
              <div class="collapsible-body">
                @foreach ($categoria["elementos"] as $item)
                  
                <p>
                    <label>
                      <input type="checkbox" class="filled-in checkEvent" checked="checked" value='{{$item["ruta_id"]}}' />
                      <span>{{$item["nombre"]}}</span>
                      </label>
                </p>
                
              @endforeach
            </div>
          </li>
          @endforeach
        </ul>

       </div>
      <div id="test2" class="col s12">



     </div>
    </form>


</div>
<hr>
<div class="modal-footer">
  <input type="submit" class="waves-effect #00e676 green accent-3 btn-small" id='guardarperfiles' value="agregar">

  <label>
    <a href="{{url('perfil')}}" class="waves-effect #01579b light-blue darken-4 btn-small">{{ __('Cancelar') }}</a>
  </label>

</div>



<script>
  $(document).ready(function () {
    $('select').formSelect();

    
    $('.tabs').tabs();
    $('.collapsible').collapsible();

 
    
  });
</script>