


<table class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table" >
    <thead>
        <tr bgcolor="#000080">
            <th><span style="color: #FFFFFF;">{{ __('ID Permiso') }}</span></th>
            <th><span style="color: #FFFFFF;">{{ __('Permiso') }}</span></th>
            <th><span style="color: #FFFFFF;">{{ __('Ruta') }}</span></th>
            <th><span style="color: #FFFFFF;">{{ __('Categoria') }}</span></th>
            <th><span style="color: #FFFFFF;"><i class="fa fa-wrench"></i></span></th>
        </tr>
    </thead>
    <tbody>
        @foreach($permisos as $permiso)

            <tr>
                <td>{{$permiso->id}}</td>
                <td>{{$permiso->nombre}}</td>
                <td>{{$permiso->nombre_ruta}}</td>
                <td>{{$permiso->nombre_categoria}}</td>
                <td> <p>
      <label>
        <input type="checkbox" />
        <span>Red</span>
      </label>
    </p></td>
                
            </tr>

        @endforeach
    </tbody>
</table>

<script>

    $(document).on("ready",function(){
        $("#collapse2, #collapse3").collapsible();
    })

</script>