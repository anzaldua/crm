@extends('layouts.app2')
@section('container')

 <div class="row">
  <div class="col s12">
   <div id="icon-prefixes" class="card card-tabs">
      <div class="card-content">




        <!-- Botton del Modal-->
          <h4>{{ __('Perfil') }}</h4>

            <a class="waves-effect waves-light btn  modal-trigger #1a237e indigo darken-4  lighten-2 right" data-target="modal" id="agregarperfil" style='margin-top:15px;'><i class ="  fa fa-plus-circle" ></i>&nbsp;<span>{{ __('Perfil') }}</span></a>

        <!-- Modal Structure -->
          <div id="modal16" class="modal modal-fixed-footer animated bounceInDown">
          <div id="contenido-modal">

          </div>

           <style type="text/css">
          .modal { 
            width: 40% !important 
             }

             .modal 
             { width: 70% !important ;
              max-height: 70% !important
               } 

               .modal { width: 70% !important ; max-height: 70% !important ; }
        </style> 
          <!--Termina modal -->

          <!--Footer del modal-->2
          </div>
           <div class="container">
          <div class="col s12 " id="tabla-perfiles">



          </div>
          </div>
        </div>
        </div>
      </div>
      </div>

      @endsection

      @push('custom-scripts')
<script>
   $(document).ready(function(){



      function funcionx(id, el) {



    var statusCheck = ($("#status").is(":checked")) ? 1 : 0;
    var datos = $("form#perfiles").serialize() + "&status=" + statusCheck;
    $.ajax({
      type: "POST",
      data: datos + "&id=" + id,
      url: "guardarperfiles",
      success: function (rp) {



        if (rp == "success") {

          tabla();
          el.modal("close");

        }


      },
  error: function (data) {
    if (data.status === 422) {
      var errors = $.parseJSON(data.responseText);
      $.each(errors, function (key, value) {
            // console.log(key+ " " +value);
            //$('#response').addClass("alert alert-danger");

            if ($.isPlainObject(value)) {
              $.each(value, function (key, value) {
                console.log(key + " " + value);
                alert(+ " " + value


                    )

            });
          } else {
              //$('#response').show().append(value + "<br/>"); //this is my div with messages
          }
          el.modal("close");
      });
  }
}
})
}

// peticion de la tablaCategorias
  function tabla() {


    $.ajax({

        type: "POST",
        url: "tablaperfiles",
        data: "",
        success: function (r) {

            $('#tabla-perfiles').html(r);
            $('table#perfiles').DataTable({
              paging: false,
              stateSave: true,
              "columnDefs": [ {
                "targets":3 ,
                "orderable": false,
                "searchable": false
                } ]
            });

                $('.editarperfiles').click(function () { //---BOTON  DE EDITAR---
                    var id = $(this).parents("tr").attr("k");
                    var el = $('#modal16').modal("open");
                    $('#modal16 #contenido-modal').html("");


                    $.ajax({
                        type: "POST",
                        url: "agregarperfiles",
                        data: "id="+id,
                        success: function (r) {
                            $('#modal16 #contenido-modal').html(r);
                            $(".checkEvent").on("change",function(){
                                var check = $(this).is(':checked') ? 1 : 0;
                                var element = $(this).val();

                                $.post("agregar_ruta_perfil",{"check":check, "element": element, "perfil":id},function(data,status){
                                  alert(data)

                                  $.post("traerpermisos_perfil",{"view":"true","perfil":id},function(data,status){
                                  

                                    $("div#test2").html(data);
                                  }) 
                                })
                                
                              })
                            $("#guardarperfiles").click(function (){
                              funcionx(id,el)
                              
                            })



                        }


                    });

                });
            }
        });


}
tabla()
$('.modal').modal();

// peticion de la tabla

   $('.modal').modal();

   $('#agregarperfil').click(function () {

        var el = $('#modal16').modal("open");

        $('#modal16 #contenido-modal').html("");
        $.ajax({

            type: "POST",//tipo de envio
            url: "agregarperfiles",//url de donde enviamos los datos
            data: "id=0",
            success: function (r) {


                $('#modal16 #contenido-modal').html(r);
                $("#guardarperfiles").click(function () {
                    funcionx(0,el)
                })

            }
        });

    });

});

</script>

@endpush