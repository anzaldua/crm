 <header class="page-topbar" id="header">
      <div class="navbar navbar-fixed"> 
        <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark #311b92 deep-purple darken-4 no-shadow">
          <div class="nav-wrapper">
      <!--       <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>
              <input class="header-search-input z-depth-2" type="text" name="Search" placeholder="Buscar...">
            </div> -->
            <ul class="navbar-list right">
         
              <li class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);"><i class="material-icons">settings_overscan</i></a></li>
      
              <li><a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown"><span class="avatar-status avatar-online"><img src="{{asset ('app-assets/images/avatar/avatar-1.png') }}" alt="avatar"><i></i></span></a></li>
          
            </ul>
            <!-- translation-button-->
            <ul class="dropdown-content" id="translation-dropdown">
            </ul>
       
            <!-- profile-dropdown-->
            <ul class="dropdown-content" id="profile-dropdown">
              <li><a class="grey-text text-darken-1" href="{{ url('cuenta')}}"><i class="material-icons">person_outline</i>Mi Cuenta</a></li>
              <li class="divider"></li>
              <li><a class="grey-text text-darken-1" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout').submit();"><i class="material-icons">keyboard_tab</i>{{ auth()->user()->nombre }}</a>
                               <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </ul>
          </div>
          <nav class="display-none search-sm">
            <div class="nav-wrapper">
              <form>
                <div class="input-field">
                  <input class="search-box-sm" type="search" required="">
                  <label class="label-icon" for="search"><i class="material-icons search-sm-icon">search</i></label><i class="material-icons search-sm-close">close</i>
                </div>
              </form>
            </div>
          </nav>
        </nav>
      </div>
    </header>
    <!-- END: Header-->