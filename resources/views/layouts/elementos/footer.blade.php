 <!-- BEGIN: Footer-->

    <footer class="page-footer footer footer-static footer-dark dark gradient-shadow navbar-border navbar-shadow">
      <div class="footer-copyright">
        <div class="container"><span>&copy; 2019 <a href="http://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank"></a> derechos reservados S.A de C.V .</span><span class="right hide-on-small-only">Enlaceforte2019<a href="https://pixinvent.com/"></a></span></div>
      </div>
    </footer>

    <!-- END: Footer-->