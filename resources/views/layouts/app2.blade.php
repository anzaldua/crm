<!doctype html>
<html lang="en" class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
  <meta name="author" content="ThemeSelect">

  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Grupo Enlaceforte</title>
  <link rel="apple-touch-icon" href="{{ asset('app-assets/images/favicon/crm.jfif') }}">
  <link rel="shortcut icon" type="image/x-icon" href="{{asset ('app-assets/images/favicon/crm.jfif') }}">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
  <link rel ="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
  <link rel ="stylesheet"href = "https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
  <link rel ="stylesheet" type="text/css" href="{{ asset ('app-assets/vendors/fullcalendar/css/fullcalendar.min.css') }}">


  <style>

  input:not([type]), input[type="text"]:not(.browser-default), input[type="password"]:not(.browser-default), input[type="email"]:not(.browser-default), input[type="url"]:not(.browser-default), input[type="time"]:not(.browser-default), input[type="date"]:not(.browser-default), input[type="datetime"]:not(.browser-default), input[type="datetime-local"]:not(.browser-default), input[type="tel"]:not(.browser-default), input[type="number"]:not(.browser-default), input[type="search"]:not(.browser-default), textarea.materialize-textarea {
    font-size: 1.3rem!important;
    }
    .input-field > label {
    font-size: 1.3rem!important;
    }
    .input-field > label.active {
    font-size: 1.2rem!important;
    }

    .select-wrapper + label {
        top: 0!important;
    }
  </style>


  <script src="https://kit.fontawesome.com/c9e7a16c6b.js" crossorigin="anonymous"></script>



  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/vendors/vendors.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/vendors/animate-css/animate.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/vendors/chartist-js/chartist.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/vendors/chartist-js/chartist-plugin-tooltip.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/sweetalert/sweetalert.css') }}">
  <link rel="stylesheet" href = "{{asset('dist/min/jquery.sweet-modal.min.css') }}">
  <link rel="stylesheet" type="text/css" href=" {{ asset ('app-assets/vendors/fullcalendar/css/fullcalendar.min.css') }}">




  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/css/themes/vertical-modern-menu-template/materialize.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/css/themes/vertical-modern-menu-template/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/css/pages/dashboard-modern.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/css/pages/page-search.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/css/pages/user-profile-page.css') }}">
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/css/pages/app-calendar.css') }}">


  <!-- END: Page Level CSS-->
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="{{asset ('app-assets/css/custom/custom.css') }}">
  <!-- END: Custom CSS-->

  <style type="text/css">
    .titlebox h4, .titlebox a{
      display: inline-flex;
    }

  </style>

  @stack('styles')


</head>
<!-- END: Head-->
<body class="vertical-layout vertical-menu-collapsible page-header-dark vertical-modern-menu 2-columns" data-open="click" data-menu="vertical-modern-menu" data-col="2-columns">

  @include('layouts.elementos.header')
  @include('layouts.elementos.sidebar2')
  <!-- @include('layouts.elementos.intro')-->

  <div id="main">
    <div class="row">
      <div class="content-wrapper-before gradient-45deg-indigo-purple"></div>
      <div class="col s12">
        <div class="container">
          <!-- Aqui se indica que ira en el contenido que cambia -->
          @yield('container')
        </div>
      </div>
    </div>
  </div>



<div id="modal1" class="modal">
  <div class="modal-content">

  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
  </div>
</div>




  <footer class="page-footer footer footer-static footer-dark gradient-45deg-indigo-purple gradient-shadow navbar-border navbar-shadow">
    <div class="footer-copyright">
      <div class="container"><span>© 2019 <a href="http://themeforest.net/user/pixinvent/portfolio?ref=pixinvent" target="_blank">PIXINVENT</a> All rights reserved.</span><span class="right hide-on-small-only">Design and Developed by <a href="https://pixinvent.com/">PIXINVENT</a></span></div>
    </div>
  </footer>



  <!-- BEGIN VENDOR JS-->
  <script src="{{asset ('app-assets/js/vendors.min.js') }}" type="text/javascript"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="{{asset ('app-assets/vendors/chartjs/chart.min.js') }}" type="text/javascript"></script>
  <script src="{{asset ('app-assets/vendors/chartist-js/chartist.min.js') }}" type="text/javascript"></script>
  <script src="{{asset ('app-assets/vendors/chartist-js/chartist-plugin-fill-donut.min.js') }}" type="text/javascript"></script>
  <script src="{{asset ('app-assets/vendors/sweetalert/sweetalert.min.js') }}"></script>
  <script src="{{asset ('app-assets/vendors/fullcalendar/lib/jquery-ui.min.js') }}"></script>
  <script src="{{asset ('app-assets/vendors/fullcalendar/lib/moment.min.js') }}"></script>
  <script src="{{asset ('app-assets/vendors/fullcalendar/js/fullcalendar.min.js') }}"></script>
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN THEME  JS-->
  <script src="{{asset ('app-assets/js/plugins.js') }}" type="text/javascript"></script>
  <script src="{{asset ('app-assets/js/custom/custom-script.js') }}" type="text/javascript"></script>
  <script src="{{asset ('app-assets/js/scripts/extra-components-sweetalert.js') }}" type="text/javascript"></script>
  <!-- END THEME  JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <script src="{{ asset('app-assets/js/scripts/form-layouts.js') }}" type="text/javascript"></script>
  <script src ="{{asset('dist/min/jquery.sweet-modal.min.js') }}" > </script>
  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
  <script src="{{ asset('app-assets/js/scripts/app-calendar.js') }}" type="text/javascript"></script>









  <script type="text/javascript">
    $(document).ready(function(){
     $.ajaxSetup({
      headers:{"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")}
    });
   });

   $(window).on("load",function(){

      $.ajaxSetup({
          headers:{"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")}
      });
   })
 </script>



 <!-- END PAGE LEVEL JS-->
 <!-- se importan las librerias de cada seccion  -->
@stack('custom-scripts')

</body>
</html>