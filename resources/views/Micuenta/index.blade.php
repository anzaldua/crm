    @extends('layouts.app')

    @section('container')
    <div class="col s12">

      <div id="icon-prefixes" class="card card-tabs">
        <div class="card-content">
          <h5>Management Panel de Control</h5>
          <hr>
          <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
          <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
          <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>

          <div class = "row">
            <div class = "col 12">
              <ul class = "tabs">
                <li class = "tab col s4"><a  class = "hoverable" href = "#Perfil">Interfaz</a></li>
                <li class = "tab col s4"><a  class ="hoverable" href = "#modulos">Fondos</a></li>
                <li class = "tab col s4"><a  class="hoverable" href = "#usuarios">Restaurar</a></li>
                <li class = "tab col s4"><a  class="hoverable" href = "#Datos">Datos</a></li>

              </ul>
            </div>
          </div>

          <div id = "Datos" class = "col s12">
            <div class="container">
              <section>
              <div class=" row titlebox">
           
          </div>
        </section>
        <aside class=" right">
            <img class="responsive-img circle z-depth-5 right" width="200" src="{{ asset('app-assets/images/user/12.jpg') }}" alt="">
          

        </aside>


            </div>
          <div class="row">
    <form class="col s10 ">
      <div class="row">
        <div class="input-field col s3">
          <input placeholder="nombre" id="nombre" type="text" class="validate">
          <label for="nombre"><h6>Nombre</h6></label>
        </div>
        <div class="input-field col s3">
          <input placeholder ="paterno"id="paterno" type="text" class="validate">
          <label for="paterno"><h6>Paterno</h6></label>
        </div>
        <div class="input-field col s3">
          <input placeholder="materno" id="materno" type="text" class="validate">
          <label for="materno"><h6>Materno</h6></label>
        </div>
      </div>
      <div class="row">
          <div class="input-field col s3">
          <input placeholder="nombreusuario" id="nombreusuario" type="text" class="validate">
          <label for="nombreusuario"><h6>NombreUsuario</h6></label>
        </div>

        <div class="input-field col s3">
          <input id="fechanac" type="date" class="validate">
          <label for="fechanac"><h6>Fecha Nac.</h6></label>
        </div>


        <div class="input-field col s3">
          <input id="fechaalt" type="date" class="validate">
          <label for="fechanalt"><h6>Fecha Alta.</h6></label>
        </div>
      </div>
      <div class="row">
        
            <div class="input-field col s3">
                <label for="departamento"></label>
                <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                    <option value="" disabled selected><h6>Departamento</h6></option>
                    <option value="1">Enlaceforte</option>
                    <option value="2">Holidein Inn</option>
                    <option value="3">Crown Plaza</option>
                </select>
                <div class="input-field">
                    <div class="errorTxt6"></div>
                </div>
            </div>
   
     
            <div class="input-field col s3">
                <label for="pueso"></label>
                <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                    <option value="" disabled selected><h6>Puesto</h6></option>
                    <option value="1">Enlaceforte</option>
                    <option value="2">Holidein Inn</option>
                    <option value="3">Crown Plaza</option>
                </select>
                <div class="input-field">
                    <div class="errorTxt6"></div>
                </div>
            </div>

                <div class="input-field col s3">
                <label for="pueso"></label>
                <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                    <option value="" disabled selected><h6>Supervisado por</h6></option>
                    <option value="1">Enlaceforte</option>
                    <option value="2">Holidein Inn</option>
                    <option value="3">Crown Plaza</option>
                </select>
                <div class="input-field">
                    <div class="errorTxt6"></div>
                </div>
            </div>
      </div>


      <div class="row">
         <div class="input-field col s3">
          <input placeholder="E-mail" id="email" type="email" class="validate">
          <label for="email"><h6>E-mail</h6></label>
        </div>

          <div class="input-field col s3">
          <input placeholder="confirmatucorreo" id="confirmatucorreo" type="email" class="validate">
          <label for="confirmatucorreo"><h6>Confirma tu Correo</h6></label>
        </div>
      </div>

      <div class="row">

         <div class="input-field col s3">
          <input placeholder="Tel-Oficina" id="teloficina" type="text" class="validate">
          <label for="teloficina"><h6>Tel.Oficina</h6></label>
        </div>

          <div class="input-field col s3">
          <input placeholder="ext" id="ext" type="text" class="validate">
          <label for="ext"><h6>Ext.</h6></label>
          </div>

            <div class="input-field col s3">
            <input placeholder="celular" id="celular" type="text" class="validate">
            <label for="celular"><h6>Celular</h6></label>
            </div>
        
      </div>

      <div class="row">

      <div class="input-field col s">
                <label for="pueso"></label>
                <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                    <option value="" disabled selected><h6>Status</h6></option>
                    <option value="1">Activo</option>
                    <option value="2">Inactivo</option>
                </select>
                </div>

                 <div class="input-field col s">
                <label for="pueso"></label>
                <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                    <option value="" disabled selected><h6>Perfil</h6></option>
                    <option value="1">Administrador General</option>
                    <option value="2">Gerente de Cuenta</option>
                    <option value="3">In House</option>
                    <option value="4">Supervisor</option>

                </select>
                </div>
            </div>
      </div>
    </form>
  </div>
  <style>

    h6{
      color: #000FFF;
    }
  </style>
            </div>
            <div class="row">
           
                              </div>
                          </div>



                                <div class="card-title">
                                  <div class="row">
                                    <div class="row">
                                    </div>
                                  </div>
                                </div>        
                              </div>
                            </div>
                          </div>
                        </div>  

                      @endsection
