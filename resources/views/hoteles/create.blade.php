
<h4>Agregar Hotel</h4>
                <div class="row">
                  <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">local_hotel</i>
                      <input id="icon_prefix1" type="text" class="form-control @error('nombre') is-invalid @enderror" id= "nombre" name="nombre"  required  autocomplete="nombre">
                      <label for="icon_prefix1"  class="center-align">{{ __('Nombre') }}</label>
                      @error('nombre')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                  <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">location_on</i>
                      <input id="icon_prefix2" type="text" class="form-control @error('direccionComercial') is-invalid @enderror" id= "direccionComercial" name="direccionComercial"  required autocomplete="direccionComercial">
                      <label for="icon_prefix2"  class="center-align">{{ __('Direccion Comercial') }}</label>
                      @error('Direccion Comercial')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">store</i>
                      <input id="icon_prefix3" type="text" class="form-control @error('colonia') is-invalid @enderror" id= "colonia" name="colonia"  required autocomplete="colonia">
                      <label for="icon_prefix3"  class="center-align">{{ __('Colonia') }}</label>
                      @error('Colonia')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>


                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">mail</i>
                      <input id="icon_prefix4" type="text" class="form-control @error('colonia') is-invalid @enderror" id= "codigoH" name="codigoH"  required autocomplete="codigoH">
                      <label for="icon_prefix4"  class="center-align">{{ __('Codigo H') }}</label>
                      @error('codigoH')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>


                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">flag</i>
                      <select>
                      <option value="" disabled selected>Seleccione Pais</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Pais</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">location_city</i>
                      <select>
                      <option value="" disabled selected>Seleccione Estado</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Estado</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">business</i>
                      <select>
                      <option value="" disabled selected>Seleccione Ciudad</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Ciudad</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">flag</i>
                      <select>
                      <option value="" disabled selected>Seleccione Pais Fiscal</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Pais Fiscal</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">location_city</i>
                      <select>
                      <option value="" disabled selected>Seleccione Estado Fiscal</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Estado Fiscal</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">business</i>
                      <select>
                      <option value="" disabled selected>Seleccione Ciudad Fiscal</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Ciudad Fiscal</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">people</i>
                      <input id="icon_prefix11" type="text" class="form-control @error('razonSocial') is-invalid @enderror" id= "razonSocial" name="razonSocial"  required autocomplete="razonSocial">
                      <label for="icon_prefix11"  class="center-align">{{ __('Razón Social') }}</label>
                      @error('razonSocial')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                     <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">place</i>
                      <input id="icon_prefix12" type="text" class="form-control @error('direccion') is-invalid @enderror" id= "direccion" name="direccion"  required autocomplete="direccion">
                      <label for="icon_prefix12"  class="center-align">{{ __('Direccion C/N') }}</label>
                      @error('direccion')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">phone</i>
                      <input id="icon_prefix16" type="text" class="form-control @error('teldirecto') is-invalid @enderror" id= "teldirecto" name="teldirecto"  required autocomplete="teldirecto" >
                      <label for="icon_prefix16"  class="center-align">{{ __('Tel.Directo') }}</label>
                      @error('teldirecto')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>


                         <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">phone</i>
                      <input id="icon_prefix13" type="text" class="form-control @error('telsecundario') is-invalid @enderror" id= "telsecundario" name="telsecundario"  required autocomplete="telsecundario">
                      <label for="icon_prefix13"  class="center-align">{{ __('Tel.Secundario') }}</label>
                      @error('telsecundario')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                     <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">phone_iphone</i>
                      <input id="icon_prefix14" type="text" class="form-control @error('telcelular') is-invalid @enderror" id= "telcelular" name="telcelular"  required autocomplete="telcelular">
                      <label for="icon_prefix14"  class="center-align">{{ __('Tel.Celular') }}</label>
                      @error('telcelular')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                      <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">assignment</i>
                      <input id="icon_prefix15" type="text" class="form-control @error('rfc') is-invalid @enderror" id= "rfc" name="rfc"  required autocomplete="rfc">
                      <label for="icon_prefix15"  class="center-align">{{ __('R.F.C') }}</label>
                      @error('rfc')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">description</i>
                      <input id="icon_prefix15" type="text" class="form-control @error('rfc') is-invalid @enderror" id= "codigopostal" name="codigopostal"  required autocomplete="codigopostal">
                      <label for="icon_prefix15"  class="center-align">{{ __('Codigo Postal') }}</label>
                      @error('codigopostal')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                    <div class="col s6">
                      <div class="switch">
                      <label>
                      Status
                      <input id ="status" name ="status" type="checkbox" checked="true">
                      <span class="lever"></span>   
                      </label>
                      </div>  
                    </div>
                </div><h4>Agregar Hotel</h4>
                <div class="row">
                  <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">local_hotel</i>
                      <input id="icon_prefix1" type="text" class="form-control @error('nombre') is-invalid @enderror" id= "nombre" name="nombre"  required autocomplete="nombre">
                      <label for="icon_prefix1"  class="center-align">{{ __('Nombre') }}</label>
                      @error('nombre')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                  <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">location_on</i>
                      <input id="icon_prefix2" type="text" class="form-control @error('direccionComercial') is-invalid @enderror" id= "direccionComercial" name="direccionComercial"  required autocomplete="direccionComercial">
                      <label for="icon_prefix2"  class="center-align">{{ __('Direccion Comercial') }}</label>
                      @error('Direccion Comercial')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">store</i>
                      <input id="icon_prefix3" type="text" class="form-control @error('colonia') is-invalid @enderror" id= "colonia" name="colonia"  required autocomplete="colonia">
                      <label for="icon_prefix3"  class="center-align">{{ __('Colonia') }}</label>
                      @error('Colonia')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>


                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">mail</i>
                      <input id="icon_prefix4" type="text" class="form-control @error('colonia') is-invalid @enderror" id= "codigoH" name="codigoH"  required autocomplete="codigoH">
                      <label for="icon_prefix4"  class="center-align">{{ __('Codigo H') }}</label>
                      @error('codigoH')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>


                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">flag</i>
                      <select>
                      <option value="" disabled selected>Seleccione Pais</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Pais</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">location_city</i>
                      <select>
                      <option value="" disabled selected>Seleccione Estado</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Estado</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">business</i>
                      <select>
                      <option value="" disabled selected>Seleccione Ciudad</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Ciudad</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">flag</i>
                      <select>
                      <option value="" disabled selected>Seleccione Pais Fiscal</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Pais Fiscal</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">location_city</i>
                      <select>
                      <option value="" disabled selected>Seleccione Estado Fiscal</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Estado Fiscal</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">business</i>
                      <select>
                      <option value="" disabled selected>Seleccione Ciudad Fiscal</option>
                      <option value="1"></option>
                      <option value="2"></option>
                      <option value="3"></option>
                      </select>
                      <label>Ciudad Fiscal</label>
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">people</i>
                      <input id="icon_prefix11" type="text" class="form-control @error('razonSocial') is-invalid @enderror" id= "razonSocial" name="razonSocial"  required autocomplete="razonSocial">
                      <label for="icon_prefix11"  class="center-align">{{ __('Razón Social') }}</label>
                      @error('razonSocial')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                     <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">place</i>
                      <input id="icon_prefix12" type="text" class="form-control @error('direccion') is-invalid @enderror" id= "direccion" name="direccion"  required autocomplete="direccion">
                      <label for="icon_prefix12"  class="center-align">{{ __('Direccion C/N') }}</label>
                      @error('direccion')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">phone</i>
                      <input id="icon_prefix16" type="text" class="form-control @error('teldirecto') is-invalid @enderror" id= "teldirecto" name="teldirecto"  required autocomplete="teldirecto" >
                      <label for="icon_prefix16"  class="center-align">{{ __('Tel.Directo') }}</label>
                      @error('teldirecto')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>


                         <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">phone</i>
                      <input id="icon_prefix13" type="text" class="form-control @error('telsecundario') is-invalid @enderror" id= "telsecundario" name="telsecundario"  required autocomplete="telsecundario">
                      <label for="icon_prefix13"  class="center-align">{{ __('Tel.Secundario') }}</label>
                      @error('telsecundario')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                     <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">phone_iphone</i>
                      <input id="icon_prefix14" type="text" class="form-control @error('telcelular') is-invalid @enderror" id= "telcelular" name="telcelular"  required autocomplete="telcelular">
                      <label for="icon_prefix14"  class="center-align">{{ __('Tel.Celular') }}</label>
                      @error('telcelular')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                      <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">assignment</i>
                      <input id="icon_prefix15" type="text" class="form-control @error('rfc') is-invalid @enderror" id= "rfc" name="rfc"  required autocomplete="rfc">
                      <label for="icon_prefix15"  class="center-align">{{ __('R.F.C') }}</label>
                      @error('rfc')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                    <div class="input-field col s6">
                      <i class="material-icons prefix pt-2">description</i>
                      <input id="icon_prefix15" type="text" class="form-control @error('rfc') is-invalid @enderror" id= "codigopostal" name="codigopostal"  required autocomplete="codigopostal">
                      <label for="icon_prefix15"  class="center-align">{{ __('Codigo Postal') }}</label>
                      @error('codigopostal')
                      <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                      </span>
                      @enderror         
                    </div>

                    <div class="col s6">
                      <div class="switch">
                      <label>
                      Status
                      <input id ="status" name ="status" type="checkbox" checked="true">
                      <span class="lever"></span>   
                      </label>
                      </div>  
                    </div>
                </div>