<?php
//print_r($hoteles);
$count = count($hoteles);

$active = ($count)? "active" : "";

$nombrehotel = ($count)? $hoteles[0]->nombrehotel : "" ;
$direccioncomercial= ($count)? $hoteles[0]->direccioncomercial:"" ;
$colonia= ($count)? $hoteles[0]->colonia:"" ;
$codigohotel= ($count)? $hoteles[0]->codigohotel:"" ;
$cp= ($count)? $hoteles[0]->cp: "" ;
$pais= ($count)? $hoteles[0]->pais: "" ;
$estado= ($count)? $hoteles[0]->estado: "" ;
$ciudad=($count)? $hoteles[0]->ciudad: "";
$idpaisfiscal=($count)? $hoteles[0]->idpaisfiscal: "";
$idestadofiscal=($count)? $hoteles[0]->idestadofiscal: "";
$idciudadfiscal=($count)? $hoteles[0]->idciudadfiscal: "";
$calleynumero=($count)? $hoteles[0]->calleynumero:  "";
$telefonodirecto=($count)? $hoteles[0]->telefonodirecto:  "";
$telefono2=($count)? $hoteles[0]->telefono2:  "";

$rfc=($count)? $hoteles[0]->rfc:  "";
$cp=($count)? $hoteles[0]->cp:  "";
$razonsocial=($count)? $hoteles[0]->razonsocial:  "";
$web=($count)? $hoteles[0]->web:  "";
$principalcontacto=($count)? $hoteles[0]->principalcontacto: "";
$incContrato=($count)? $hoteles[0]->incContrato: "";
$finContrato=($count)?$hoteles[0]->finContrato:"";
$notas=($count)?$hoteles[0]->notas:"";
$logohotel=($count)?$hoteles[0]->logohotel:"";
$idcorporativo=($count)?$hoteles[0]->idcorporativo:"";


?>
<!--Modal de editar-->
<div class="modal-content">
   <h4>Grupo EnlaceForte</h4>
   <hr>
   <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
   <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
   <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>  
   <div class = "row">
      <div class = "col s12">
        <ul class = "tabs" bgcolor="#00AAE4">
         <li class = "tab col s3"><a  class = "active" href = "#hotel">Hotel</a></li>
         <li class = "tab col s3"><a  href = "#detalle">Detalle</a></li>
         <li class = "tab col s3"><a href = "#contrato">Plan de Contrato</a></li>
         <li class="  tab col s3"><a href="#contacto">Contacto</a></li>
     </ul>
 </div>
 <div id = "contrato" class = "col s12">Sent</div>
</div> 


<form id='datosHotel' class="formValidate" id="formValidate" method="get">
    <br>

    
    @csrf
    <div class="row" id="hotel">

        <div class="input-field col s6">
            <input id="icon_prefix1" type="text" id="nombre" class="validate" name="nombrehotel" 
            value='{{$nombrehotel}}' required aria-required="true" placeholder="nombrehotel" required autocomplete="nombrehotel">
            <label for="icon_prefix1" class="center-align {{$active}}"><h6>{{ __('NombreHotel') }}</h6></label>
            <p class='errornombrehotel text-center alert alert-danger hidden'></p>
        </div>

        <div class="input-field col s6">
            <input id="icon_prefix3" type="text" id="direccioncomercial" class="validate" name="direccioncomercial"
            value="{{$direccioncomercial}}" placeholder="direccioncomercial" required autocomplete="direccioncomercial">
            <label for="icon_prefix3" class="center-align {{$active}}"><h6>{{ __('Direccion Comercial') }}</h6></label>
        </div>


        <div class="input-field col s6">        
            <input id="icon_prefix4" type="text" id="colonia" class="validate" name="colonia"
            value="{{$colonia}}" placeholder="colonia" required autocomplete="colonia" >
            <label for="icon_prefix4" class="center-align {{$active}}"><h6>{{ __('Colonia') }}</h6></label>
        </div>


        <div class="input-field col s6">
            <input id="icon_prefix5" type="text" id="codigohotel"  class="validate" name="codigohotel" required autocomplete="codigohotel" 
            value="{{$codigohotel}}" placeholder="codigohotel">
            <label for="icon_prefix5" class="center-align {{$active}}"><h6>{{ __('Codigo H') }}</h6></label>
        </div>
        

        <div class="col s6">
            <label for="pais">{{$pais}}</label>
            <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                <option value="" disabled selected>{{ __('seleccione Pais') }}</option>
                <option value="1">{{ __('México') }}</option>
                <option value="2">España</option>
                <option value="3">Argentina</option>
            </select>
            <div class="input-field">
                <div class="errorTxt6"></div>
            </div>
        </div>

        <div class="col s6">
            <label for="estado">{{$estado}}</label>
            <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                <option value="" disabled selected>seleccione Estado</option>
                <option value="1">Nuevo León</option>
                <option value="2">San Luis Potosí</option>
                <option value="3">Tamaulipas</option>
            </select>
            <div class="input-field">
                <div class="errorTxt6"></div>
            </div>
        </div>

        <div class="col s6">
            <label for="ciudad">{{$ciudad}}</label>
            <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                <option value="" disabled selected>seleccione ciudad</option>
                <option value="1">Monterrey</option>
                <option value="2">San Luis Potosí</option>
                <option value="3">Tampico</option>
            </select>
            <div class="input-field">
                <div class="errorTxt6"></div>
            </div>
        </div>

        <div class="col s6">
            <label for="idpaisfiscal">{{$idpaisfiscal}}</label>
            <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                <option value="" disabled selected>País Fiscal</option>
                <option value="1">México</option>
                <option value="2">España</option>
                <option value="3">Argentina</option>
            </select>
            <div class="input-field">
                <div class="errorTxt6"></div>
            </div>
        </div>


        <div class="col s6">
            <label for="idestadofiscal">{{$idestadofiscal}}</label>
            <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                <option value="" disabled selected>Estado Fiscal</option>
                <option value="1">Nuevo León</option>
                <option value="2">San Luis Potosí</option>
                <option value="3">Tamaulipas</option>
            </select>
            <div class="input-field">
                <div class="errorTxt6"></div>
            </div>
        </div>


        <div class="col s6">
            <label for="idciudadfiscal">{{$idciudadfiscal}}</label>
            <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                <option value="" disabled selected>Ciudad Fiscal</option>
                <option value="1">Monterrey</option>
                <option value="2">San Luis Potosí</option>
                <option value="3">Tampico</option>
            </select>
            <div class="input-field">
                <div class="errorTxt6"></div>
            </div>
        </div>

        <div class="input-field col s6">
         
            <input id="icon_prefix11" type="text" id="razonsocial" class="validate" name="razonsocial" required autocomplete="razonsocial"
            value="{{$razonsocial}}" placeholder="razonsocial">
            <label for="icon_prefix11" class="center-align"><h6>{{ __('Razón Social') }}<h6></label>
            </div>

            <div class="input-field col s6">
                <input id="icon_prefix12" type="text" id="direccion" class="validate" name="calleynumero" required autocomplete="direccion" 
                value="{{$calleynumero}}" placeholder="calle y numero">
                <label for="icon_prefix12" class="center-align"><h6>{{ __('Direccion C/N') }}</h6></label>
            </div>

            <div class="input-field col s6">
             
                <input id="icon_prefix16" type="text" id="telefonodirecto" class="validate" name="telefonodirecto" 
                value="{{$telefonodirecto}}" placeholder="Tel.Directo">
                <label for="icon_prefix16" class="center-align"><h6>{{ __('Tel.Directo') }}</h6></label>              
            </div>


            <div class="input-field col s6">
                <input id="icon_prefix13" type="text" id="telefono2" class="validate" name="telefono2" required autocomplete="telefono2" 
                value="{{$telefono2}}" placeholder="Telefono2">
                <label for="icon_prefix13" class="center-align"><h6>{{ __('Tel.Secundario') }}</h6></label>
            </div>

            

            <div class="input-field col s6">
                <input id="icon_prefix15" type="text" id="rfc" class="validate" name="rfc" required autocomplete="rfc" 
                value="{{$rfc}}" placeholder="rfc">
                <label for="icon_prefix15" class="center-align"><h6>{{ __('R.F.C') }}</h6></label>
            </div>

            <div class="input-field col s6">
                <input id="icon_prefix50" type="text" id="cp" class="validate" name="cp" required autocomplete="cp"
                value="{{$cp}}" placeholder="cp">
                <label for="icon_prefix50" class="center-align"><h6>{{ __('Codigo Postal') }}</h6></label>
            </div>

            <div class="col s6">
                <div class="switch">
                    <label>
                        Status
                        <input id="status"  type="checkbox" >
                        <span class="lever"></span>
                    </label>
                </div>
            </div>
        </div>


        <!--div del detalle-->


        
        <div class="row" id="detalle">

            <div class="input-field col s6">
                <input id="icon_prefix23" type="text" id="web" class="validate" name="web" required autocomplete="nombre" 
                value='{{$web}}' placeholder="sitio web">
                <label for="icon_prefix23" class="center-align {{$active}}"><h6>{{ __('Sitio web') }}</h6></label>
            </div>


            <div class="input-field col s6">
                <label for="idciudadfiscal">{{$idcorporativo}}</label>
                <select class="error browser-default" id="crole" name="crole" data-error=".errorTxt6">
                    <option value="" disabled selected>Corporativo</option>
                    <option value="1">Enlaceforte</option>
                    <option value="2">Holidein Inn</option>
                    <option value="3">Crown Plaza</option>
                </select>
                <div class="input-field">
                    <div class="errorTxt6"></div>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <textarea id="notas" class="materialize-textarea" data-length="120" class="validate" name="notas" 
                    value="{{$notas}}" placeholder="notas"></textarea>
                    <label for="textarea2"><h6>{{ __('Nota') }}</h6></label>
                </div>
            </div>
            
        </div>

        <!--div del contacto-->

        <div id = "contacto" class = "col s12">

            <div class="input-field col s6">
                
                <input id="icon_prefix24" type="text" id="principalcontacto" class="validate" name="principalcontacto" required autocomplete="principalcontacto"
                value="{{$principalcontacto}}" placeholder="principalcontacto">
                <label for="icon_prefix24" class="center-align {{$active}}"><h6>{{ __('Principal Contacto') }}</h6></label>
            </div>


            


            <div class="input-field col s6">
                
                <input id="icon_prefix21" type="date" id="incContrato" class="validate" name="incContrato" required autocomplete="incContrato" 
                value="{{$incContrato}}" placeholder="incContrato">
                <label for="icon_prefix21" class="center-align"><h6>{{ __('Inicio de Contrato') }}</h6></label>
                
            </div>

            <div class="input-field col s6">
                <input id="icon_prefix122" type="date" id="finContrato" class="validate" name="finContrato" required autocomplete="finContrato" 
                value="{{$finContrato}}" placeholder="finContrato">
                <label for="icon_prefix22" class="center-align">{{ __('Fin de contrato') }}</label>
            </div> 

        </div>

    </form>
</div>
<div class="modal-footer">



    <input type="submit"  class="waves-effect #00e676 green accent-3 btn-small" id='guardarHoteles' value="agregar">


    <label>
        <a href="{{url('hoteles')}}" class="waves-effect blue btn-small">Cancelar</a>
    </label>
</div>
