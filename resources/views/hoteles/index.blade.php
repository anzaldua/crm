@extends('layouts.app')

@push('styles')
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.15.4/dist/bootstrap-table.min.css">

@endpush

@push('scripts')
<script src="https://unpkg.com/bootstrap-table@1.15.4/dist/bootstrap-table.min.js"></script>
<script src="{{asset ('bootstrap/js/bootstrap-table.min.js') }}"></script>

@endpush

@section('container')


<div class="row">
 <div class="row">
  <div class="col s12">

    <div id="icon-prefixes" class="card card-tabs">
      <div class="card-content">

        <!-- Botton del Modal-->


        <button type="button" class="waves-effect #311b92 deep-purple darken-4 btn-small" data-toggle="modal" id="agregarhotel">
          Agregar
        </button>

        <!-- Modal Structure -->
        <div id="modal1" class="modal modal-fixed-footer">
          <div id="contenidomodal">


          </div>
          <!--Termina modal -->
          <!--Footer del modal-->

        </div>
        <!--Termina footer-->
                  <!--<div class=" row justify-content-end pb-2">
                  <a  href = "{{ route('hoteles.create')}}" class="btn-floating btn-large waves-effect  green accent-3"><i class="material-icons">person_add</i>Agregar</a>
                </div>-->


                <h4 class="card-title">{{__(' Lista de Hoteles')}}</h4>
                <!--ID DE LA TABLA -->
                <div class="col s12 " id="tabla">


                  <!-- Modal Structure -->
                  <div id="modal2" class="modal modal-fixed-footer">
                    <div id="contenidomodal">

                    </div>
                    <!--Termina modal -->
                    <!--Footer del modal-->

                  </div>
                  <!--Termina footer-->

                  <div id="modal3" class="modal modal-fixed-footer">
                    <div id="contenidomodal">

                    </div>
                    <!--Termina modal -->
                    <!--Footer del modal-->

                  </div>
                </div>






              </div></center>
            </div>
          </div>
        </div>
      </div>

      @endsection



