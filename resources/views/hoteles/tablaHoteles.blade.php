<script>
  $(function(){
    $('#modal2').modal();
  });




</script>
       
<div class="card-title">
  <div class="row">
    <div class="row">
      <table id="reservas-table" class="display" cellspacing="0" width="100%" class="border striped hoverable responsive-table">
       <thead>  
        <tr bgcolor="#000080">
          <th><span style="color: #FFFFFF;">#id</span></th>
          <th><span style ="color: #FFFFFF;">Imagen</span></th>
          <th><span style="color: #FFFFFF;">Nombre</span></th>
          <th><span style="color: #FFFFFF;">Usuario</span></th>                 
          <th><span style="color: #FFFFFF;">inc.Contrato</span></th>
          <th><span style="color: #FFFFFF;">Fin Contrato</span></th>
          <th><span style="color: #FFFFFF;">Registrado</span></th>
          <th><span style="color: #FFFFFF;">status</span></th>
          <th><span style="color: #FFFFFF;">opciones</span></th>
        </tr>
      </thead>
      <tbody>
        @foreach($hoteles as $hotel)
        <tr k="{{$hotel->id}}">
          <td>{{$hotel->id}}</td>
          <td><img width="150px" src=" Storage::url($hotel->foto) "></td>
          <td>{{$hotel->nombrehotel}}</td>
          <td><!-- Modal Trigger -->
            <a class="waves-effect waves-light btn  modal-trigger #311b92 deep-purple darken-4 lighten-2" href="#modal2">N.U</a>

            <!-- Modal Structure -->
            <div id="modal2" class="modal">
              <div id="contenido-modal" >
                <div class="modal-body">
                  <div class="table-responsive">
                   <h4> Grupo EnlaceForte</h4>
                   <table  id="departamentos-table"  class="table table-bordered">
                    <thead>
                      <tr bgcolor="#000080">
                        <th><span style="color: #FFFFFF;">#id</span></th>
                        <th><span style="color: #FFFFFF;">Nombre </span></th>
                        <th><span style="color: #FFFFFF;">Estatus</span></th>
                      </tr>
                    </thead>
                    <tbody>
                      <td>1</td>
                      <td>alejandro lozano</td>
                      <td>activo</td>
                    </tbody>
                    <tbody>
                      <td>2</td>
                      <td>Arnufo perez</td>
                      <td>activo</td>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </td>
    <td>{{$hotel->incContrato}}</td>
    <td>{{$hotel->finContrato}}</td>
    <td>{{$hotel->registrado}}</td>

    <td> <button type="submit" name="usuario" value="agree" disabled id="usuario" class="btn btn-status"><span style="color: #FFFFFF;">{{$hotel->status? 'Activo' : 'Inactivo'}}</span></td></button>

    <td>
     <div class="container section">
      <a class="btn editarhotel waves-effect #40c4ff light-blue accent-2 btn-small"><i class="material-icons">edit</i></a> 

      <a class="waves-effect waves-light btn modal-trigger #ffa726 orange lighten-1" href="#modal3"><i class="  fa fa-file-pdf-o"></i></a>

      <!-- Modal Structure -->
      <div id="modal3" class="modal">
        <div class="modal-content">
          <h4>Grupo EnlaceForte</h4>
          <hr>
          <meta name = "viewport" content = "width = device-width, initial-scale = 1">            
          <script src = "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
          <div class = "row">
            <div class = "col 7">
              <ul class = "tabs">
               <li class = "tab col s3"><a  class = "active" href = "#historia">Historial</a></li>
               <li class = "tab col s3"><a  href = "#nuevo">nuevo</a></li>

             </ul>
           </div>

           <div id = "historia" class = "col s12">
            <table id="data-table-simple" class="display">
              <thead>
                <tr  bgcolor="#000080">
                  <th><span style="color: #FFFFFF;">Inicio</span></th>
                  <th><span style="color: #FFFFFF;">Fin</span></th>
                  <th><span style="color: #FFFFFF;">Meses</span></th>
                  <th><span style="color: #FFFFFF;">Estatus</span></th>
                  <th><span style="color: #FFFFFF;">Costo C.</span></th>
                  <th><span style="color: #FFFFFF;">Costo A.</span></th>
                  <th><span style="color: #FFFFFF;">Costo M.</span></th>
                  <th><span style="color: #FFFFFF;">Costo T.</span></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>

                  <td>07-03-2017</td>
                  <td>07-03-2017</td>
                  <td>6</td>
                  <td><a class="waves-effect  btn #00c853 green accent-4
 teal darken-2 " ><i class="material-icons"></i>activo</a></td>
                  <td>0</td>
                  <td>0</td>
                  <td>0</td>
                  <td>
                    <div class="content">
                      <a class="waves-effect waves-light btn #bf360c deep-orange darken-4
" href="{{ url('hoteles.modal')}}"><i class="fa fa-pencil"></i></a>
                      <a class="waves-effect waves-light btn #01579b light-blue darken-4
"><i class="material-icons">email</i></a>
                    </div> 
                  </td>
                </tr>
              </tbody>
            </table>


          </div>
        </div> 
      </div>
      <div id="nuevo">
        <div class="row">
          <form class="col s12" id="form1">
            <div class="row">
              <div class="input-field col s3">

                <input id="icon_prefix21" type="date" id="incContrato" class="validate" name="incContrato" required autocomplete="incContrato" 
                value="" placeholder="incContrato">
                <label for="icon_prefix21" class="center-align"><h6>{{ __('Inicio de Contrato') }}</h6></label>
              </div>


              <div class="col s3">
                <label for="plan">plan</label>
                <select class="error browser-default" id="meses" name="meses" required="meses">
                  <option value="opcion 0" selected="">Seleccione Plan</option>
                  <option value="1">3 Meses</option>
                  <option value="2">6 Meses</option>
                  <option value="3">12 Meses</option>
                </select>
              </div>

              <div class="input-field col s3">

                <input id="icon_prefix23" type="date" id="incContrato" class="validate" name="incContrato" required autocomplete="incContrato" 
                value="" placeholder="incContrato">
                <label for="icon_prefix23" class="center-align"><h6>{{ __('Fin de Contrato') }}</h6></label>

              </div>

              <div class="input-field col s3">

                <input id="icon_prefix24" type="number" id="costo" class="validate" name="costo" required autocomplete="costo" 
                value="" placeholder="0">
                <label for="icon_prefix24" class="center-align"><h6>{{ __('Costo por mes') }}</h6></label>

              </div>
              <div class="input-field col s3">
                <input id="icon_prefix25" type="number" id="descuento" class="validate" name="descuento" required autocomplete="descuento" 
                value="" placeholder="descuento">
                <label for="icon_prefix25" class="center-align"><h6>{{ __('%Descuento') }}</h6></label>
              </div>
              <div class="input-field col s3">

                <input id="icon_prefix25" type="number" id="total" class="validate" name="total" required autocomplete="total" 
                value="" placeholder="total">
                <label for="icon_prefix25" class="center-align"><h6>{{ __('Total') }}</h6></label>
              </div>
            </div>

            <div class="row">
              <div class="input-field col s4">
                <p>
                  <label>
                    <p>costo de apertura</p>
                    <input type="checkbox" checked="checked" value="1" id="form1" />
                    <span>Si</span>
                    <input type="checkbox" checked="checked" value="0" id="form2" />
                    <span>No</span>
                  </label>
                </p>   
              </div>

              <div class="input-field col s4">
                <p>
                  <
                  <label>
                    <p>costo de mantenimiento</p>
                    <input type="checkbox"  checked="checked"/>
                    <span>Si</span>
                    <input type="checkbox" checked="checked"/>
                    <span>No</span>
                  </label>
                </p>   
              </div>
            </div>
            <div class="row">
              <div class="input-field col s2">
                <input id="icon_prefix25" type="number" id="costo" class="validate" name="costo" required autocomplete="costo" 
                value="" placeholder="costo">
                <label for="icon_prefix25" class="center-align"><h6>{{ __('Costo') }}</h6></label>
              </div>


              <div class="input-field col s2">
                <input id="icon_prefix25" type="number" id="Descuento" class="validate" name="Descuento" required autocomplete="Descuento" 
                value="" placeholder="Descuento">
                <label for="icon_prefix25" class="center-align"><h6>{{ __('Descuento') }}</h6></label>
              </div>

              <div class="input-field col s2">
                <input id="icon_prefix25" type="number" id="costo" class="validate" name="costo" required autocomplete="costo" 
                value="" placeholder="costo">
                <label for="icon_prefix25" class="center-align"><h6>{{ __('Costo') }}</h6></label>
              </div>
              <div class="input-field col s2">
                <input id="icon_prefix25" type="number" id="Descuento" class="validate" name="Descuento" required autocomplete="Descuento" 
                value="" placeholder="Descuento">
                <label for="icon_prefix25" class="center-align"><h6>{{ __('Descuento') }}</h6></label>
              </div>

              <div class="col s4">
                <table class="table">
                  <thead>
                    <tr bgcolor="#light-blue darken-4">
                      <th><span style="color: #FFFFFF;">Cargo</span></th>
                      <th width="60%"><span style="color: #FFFFFF">Subtotal</span></th>
                      <th><span style="color: #FFFFFF;">Total</th>
                      </tr>
                    </thead>
                    <tbody class="items_totales">
                      <td>Contrato</td>
                      <td>$0+16%</td>
                      <td>$0</td>
                    </tbody>
                    <tbody>
                     <td>Contrato</td>
                     <td>$0+16%</td>
                     <td>$0</td>
                   </tbody>
                 </table>
               </div>
             </div>



             <div class="row">
              <div class="input-field col s3">
                <input id="icon_prefix25" type="number" id="Total" class="validate" name="Total" required autocomplete="Total" 
                value="" placeholder="Total">
                <label for="icon_prefix25" class="center-align"><h6>{{ __('Total') }}</h6></label>
              </div>
              <div class="input-field col s3">
                <input id="icon_prefix25" type="number" id="Total" class="validate" name="Total" required autocomplete="Total" 
                value="" placeholder="Total">
                <label for="icon_prefix25" class="center-align"><h6>{{ __('Total') }}</h6></label>
              </div>
            </div>

            <div class="row">
             <div class="input-field col s12">

              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Enviar</button>
              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Cancelar</button>
            </div>

          </div>
        </form>
      </div>
    </td>
  </tr>
  @endforeach 
</tbody>
</table>
</div>
</div>
</div>  


<script>
  $(document).ready(function(){
    $('#modal3').modal();
  });

</script>
<script>
//boton de buscar
// document.addEventListener('DOMContentLoaded', function() {

    // data: {
        // "html": null,
        // "css": null,
        // "angular": 'https://placehold.it/250x250'
      // },
    // var elems = document.querySelectorAll('.autocomplete');
    // var instances = M.Autocomplete.init(elems, options);
  // });

