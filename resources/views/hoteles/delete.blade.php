<form action="{{ route('hoteles.destroy' ,$hotel->id) }}" method="POST" style="display:inline-block;">

@method('DELETE')
@csrf

<button  type ="submit" class ="waves-effect red accent-4 btn-small" onclick="return confirm()"> <i class="material-icons left">delete</i>Eliminar</button>

<script>
function myFunction() {
  alert("desea eliminar?");
}
</script>
</form>