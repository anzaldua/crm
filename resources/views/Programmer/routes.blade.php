@extends('layouts.app2')
@section('container')

<div class="row">
    <div class="col s12">
        <div id="icon-prefixes" class="card card-tabs">
            <div class="card-content">

                <!-- Botton del Modal-->
                <h4>Rutas del sistema</h4>
                <a class="waves-effect waves-light btn  modal-trigger #1a237e indigo darken-4  lighten-2 right btn-plus"
                    data-target="modal" style='margin-top:15px;'><i
                        class="  fa fa-plus-circle"></i>&nbsp;<span>{{ __('Corporativo') }}</span></a>



                <!-- Modal Structure -->
                <div id="modal9" class="modal modal-fixed-footer">
                    <div id="contenido-modal">

                    </div>
                    <!--Termina modal -->

                    <!--Footer del modal-->
                </div>

                {{-- <h4 class="card-title">{{ __('Lista de Corporativo') }}</h4> --}}
                <div class="col s12 " id='divresponse'>

                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@push('custom-scripts')

<script src="{{{ asset('Assets/Js/programmer.routes.js') }}}"></script>

@endpush