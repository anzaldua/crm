	<?php

	/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
	*/

	Route::get('/', function () {

		return view('home', ["rutas"=>array()]);
	})->middleware('auth');

	Auth::Routes(['verify' => true]);/*registrará todas las rutas relacionadas con la autenticación,
	como inicio de sesión, registro y restablecimiento de contraseña. La */

	Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
	{

			// rutas globales
		Route::middleware(['web','auth'])->group(function (){
			Route::get('/perfil' , 'PerfilesController@index');

		});

		Route::get('Programmer/Routes','ProgrammerController@routes');
		Route::get('Programmer/getRoutes/{id}','ProgrammerController@getRoutes');
		Route::get('Programmer/saveRoutes','ProgrammerController@saveRoutes');
		 //routes permisos
		Route::get('permisos','PermisosController@index');
		Route::post('tablapermisos','PermisosController@tablapermisos');
		Route::post('agregarpermisos','PermisosController@agregarpermisos');
		Route::post('guardarpermisos','PermisosController@guardarActualizar');
		//rutas agregadas de Hoteles

		Route::resource('hoteles','HotelesController');
		Route::post('tablaHoteles','HotelesController@tablaHoteles');
		Route::post('agregarHoteles','HotelesController@agregarHoteles');
		Route::post('guardarHoteles','HotelesController@guardarActualizar');
		Route::post('agregareditar','HotelesController@agregareditar');
		Route::get ('verusuarios','HotelesController@verusuarios');
		Route::post('fotos','HotelesController@fotos');

		// controlador de guardar Modulos
		Route::resource('categorias','CategoriasController');
		Route::post('tablacategorias','CategoriasController@tablacategorias');
		Route::post('agregarcategorias','CategoriasController@agregarCategorias');
		Route::post('guardarcategorias','CategoriasController@guardarActualizar');
		Route::get('exportar', 'CategoriasController@export');
		Route::post('accionescategorias','CategoriasController@accionescategorias');

		// controlador de cuenta
		Route::resource('cuenta','CuentaController');
   		// controlador de sitios
		Route::resource('sitios','SitiosController');
		Route::get('sitios.index','SitiosController@Index');
		Route::post('tablasitios' ,'SitiosController@Tablasitios');
		Route::post('agregarsitios','SitiosController@Agregarsitios');
		Route::post('guardarsitios','SitiosController@Guardaractualizar');
		Route::post('agregareditar','SitiosController@Agregareditar');
		Route::get('exportar', 'SitiosController@export');

		// rutas de perfiles
		Route::get('acciones','PerfilesController@acciones');
		Route::resource('perfiles','PerfilesController');
		Route::post('tablaperfiles','PerfilesController@Tablaperfiles');
		Route::post('agregarperfiles','PerfilesController@Agregarperfiles');
		Route::post('guardarperfiles','PerfilesController@Guardaractualizar');
		Route::post('agregar_ruta_perfil','PerfilesController@agregar_ruta_perfil');
		Route::get('agregaraccion','PerfilesController@agregaracciones');
		Route::post('traerpermisos_perfil','PerfilesController@traerpermisos_perfil');

		// rutas de usuarios

		Route::get('usuarios','UsuariosController@index');
		Route::get('usuarios.index','UsuariosController@index');
		Route::get('/departamento/{id}','UsuariosController@getPuestos');
		Route::post('tablausuarios','UsuariosController@tablausuarios');
		Route::post('agregarusuarios','UsuariosController@Agregarusuarios');
		Route::post('guardarusuarios','UsuariosController@Guardaractualizar');

		//rutas de usuarios de hoteles
		Route::get('hotelusuarios','UsuariosHotelController@index');
		Route::post('Tablausuarioshotel','UsuariosHotelController@Tablausuarioshotel');
		Route::post('Agregarusuarioshotel','UsuariosHotelController@Agregarusuarios');
		Route::post('Guardarusuarioshotel','UsuariosHotelController@Guardaractualizar');


		//Rutas de departamentos
		Route::resource('Departamentos','DepartamentosController');
		Route::get('Departamentos','DepartamentosController@index');
		Route::post('Tabladepartamentos','DepartamentosController@Tabladepartamentos');
		Route::post('Agregardepartamentos','DepartamentosController@Agregardepartamentos');
		Route::post('Guardardepartamentos','DepartamentosController@Guardaractualizar');

		//ruta de puestos

		Route::get('puestos','PuestosController@index');
		Route::post('Tablapuestos','PuestosController@tablaPuestos');
		Route::post('Agregarpuestos','PuestosController@agregarPuestos');
		Route::post('Guardarpuestos','PuestosController@Guardaractualizar');

		//ruta de corporativo
		Route::get('corporativo','CorporativosController@index');
		Route::post('tablaCorporativos','CorporativosController@tablaCorporativos');
		Route::post('agregarCorporativos','CorporativosController@agregarCorporativos');
		Route::post('guardarCorporativos','CorporativosController@guardarActualizar');

		//ruta de Empresas
		Route::resource('empresas','EmpresasController');
		Route::get('girosBysegmentos/{id}','EmpresasController@bysegmento');
		Route::post('tablaempresas','EmpresasController@Tablaempresas');
		Route::post('Agregarempresas','EmpresasController@Agregarempresas');
		Route::post('Guardarempresas','EmpresasController@GuardarActualizar');


		//rutasoist
		Route::get('rutasmostrar','PermisosController@rutas');

		//contactos
		Route::resource('contactos','ContactosController');
		Route::get('contactos','ContactosController@index');
		Route::post('Tablacontactos','ContactosController@tablacontactos');
		Route::post('Agregarcontactos','ContactosController@Agregarcontactos');
		Route::post('Guardarcontactos','ContactosController@GuardarActualizar');
		//rutas de segmentos
		Route::resource('segmentos','SegmentosController');
		Route::get('segmentos','SegmentosController@index');
		Route::post('Tablasegmentacion','SegmentosController@Tablasegmentacion');
		Route::post('Agregarsegmentacion','SegmentosController@Agregarsegmentacion');
		Route::post('GuardarSegmentacion','SegmentosController@GuardarActualizar');

		//rutas de giros de la empresa

		Route::get('giros','GirosController@index');
		Route::post('Tablagiros','GirosController@Tablagiros');
		Route::post('Agregargiros','GirosController@Agregargiros');
		Route::post('Guardargiros','GirosController@GuardarActualizar');

	//rutas titulos
		Route::get('titulos','TitulosController@index');
		Route::post('Tablatitulos','TitulosController@Tablatitulos');
		Route::post('Agregartitulos','TitulosController@AgregarTitulos');
		Route::post('Guardartitulos','TitulosController@GuardarActualizar');

	//niveles

		Route::get('niveles','NivelesController@index');
		Route::post('Tablaniveles','NivelesController@Tablaniveles');
		Route::post('Agregarniveles','NivelesController@Agregarniveles');
		Route::post('Guardarniveles','NivelesController@Guardaractualizar');

   //rutas agenda

		Route::get('agenda','AgendasController@index');
		Route::post('Tablaagenda','AgendasController@Tablaagenda');
		Route::post('Agregaragenda','AgendasController@Agregaragenda');
		Route::post('Guardaragenda','AgendasController@Guardaractualizar');





	}
