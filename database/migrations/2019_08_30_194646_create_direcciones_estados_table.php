<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionesEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones_estados', function (Blueprint $table) {
            $table->bigIncrements('iddirecciones_estados');
            $table->unsignedBigInteger('iddirecciones_paises');
            $table->string('nombre');
            $table->string('codigo_estados');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones_estados');
    }
}
