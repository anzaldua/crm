<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->bigIncrements('agenda_id');
            $table->Integer('usuariohotel_id')->nullable();
            $table->Integer('contacto_id')->nullable();
            $table->Integer('hotel_id')->nullable();
            $table->Integer('empresa_id')->nullable();
            $table->string('tipodeactividad');
            $table->date('fecha');
            $table->time('hora');
            $table->string('seguimiento');
            $table->string('resultados');
            $table->tinyInteger('status')->nullable()->default(1);//status de si esta activo el usuario marca 1 y si no marca 0
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
