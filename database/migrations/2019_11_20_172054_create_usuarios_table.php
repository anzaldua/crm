<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('usuarios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('paterno');
            $table->string('materno');
            $table->string('usuario');
            $table->string('password')->nullable();
            $table->dateTime('fechanacimiento');
            $table->unsignedBigInteger('departamento_id')->unsigned()->nullable();
            $table->unsignedBigInteger('puesto_id')->unsigned()->nullable();
            $table->unsignedBigInteger('perfil_id')->unsigned()->nullable();
            $table->string('email');
            $table->string('tipo');
            $table->Integer('id_categoria')->nullable();
            $table->unsignedBigInteger('hotel_id')->unsigned()->nullable();
            $table->tinyInteger('status')->nullable()->default(1); //status de si esta activo el usuario marca 1 y si no marca 0
            $table->unsignedBigInteger('id_corporativo')->unsigned()->nullable();
            $table->string('supervisado_por');
            $table->timestamps();
        });
            Schema::table('usuarios', function($table) {
            $table->foreign('hotel_id')->references('id')->on('hotel');
            $table->foreign('id_corporativo')->references('id')->on('corporativo');
            $table->foreign('perfil_id')->references('id')->on('perfiles');
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->foreign('puesto_id')->references('id')->on('puestos');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
