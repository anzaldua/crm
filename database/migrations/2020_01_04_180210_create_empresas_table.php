<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('empresas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('id_hotel')->nullable();
            $table->Integer('id_usuariodelhotel')->nullable();
            $table->string('nombreempresa');
            $table->string('giro')->nullable();
            $table->string('direccion');
            $table->string('colonia');
            $table->Integer('cp');
            $table->Integer('ciudad_id')->nullable();
            $table->Integer('estado_id')->nullable();
            $table->Integer('pais_id')->nullable();
            $table->string('razonsocial');
            $table->string('direccionfiscal');
            $table->Integer('coloniafiscal');
            $table->Integer('cpfiscal')->nullable();
            $table->Integer('ciudadfiscal_id')->nullable();
            $table->Integer('estadofiscal_id')->nullable();
            $table->Integer('paisfiscal_id')->nullable();
            $table->string('rfc');
            $table->string('telefonoempresa');
            $table->string('logoempresa')->nullable();
            $table->string('notas')->nullable();
            $table->double('latitud')->nullable();
            $table->double('longitud')->nullable();
            $table->tinyInteger('zoom')->nullable();
            $table->string('nivelempresa')->nullable();
            $table->tinyInteger('status')->nullable()->default(1);//status de si esta activo el usuario marca 1 y si no marca 0
            $table->unsignedBigInteger('giro_id')->unsigned()->nullable();
            $table->unsignedBigInteger('segmento_id')->unsigned()->nullable();
            $table->tinyInteger('tipoempresas');
            $table->timestamps();
    
    });

            Schema::table('empresas', function($table) {
             $table->foreign('segmento_id')->references('id')->on('segmentos');
             $table->foreign('giro_id')->references('id')->on('giros');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
