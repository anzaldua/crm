<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilesRutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('perfiles_rutas', function (Blueprint $table) {
            $table->bigIncrements('perfiles_rutas_id');
            $table->string('perfil_id');
            $table->string('ruta_id');
            $table->tinyInteger('status')->nullable()->default(1);//status de si esta activo el usuario marca 1 y si no marca 0

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles_rutas');
    }
}
