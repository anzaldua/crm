<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('contactos', function (Blueprint $table) {
            $table->bigIncrements('contacto_id');
            $table->unsignedBigInteger('id_hotel')->unsigned()->nullable();
            $table->unsignedBigInteger('empresa_id')->unsigned()->nullable();
            $table->unsignedBigInteger('id_usuariosdelhotel')->unsigned()->nullable();
            $table->string('titulo');
            $table->string('nombre');
            $table->string('paterno');
            $table->string('materno');
            $table->string('estadocivil');
            $table->date('fechanacimiento');
            $table->date('fechaimportante');
            $table->string('puesto');
            $table->string('departamento');
            $table->tinyInteger('reserva');
            $table->tinyInteger('convenios');
            $table->tinyInteger('grupos');
            $table->tinyInteger('banquetes');
            $table->string('telefono');
            $table->string('celular');
            $table->string('emailempresa');
            $table->string('emailpersonal');
            $table->string('promediocn');
            $table->double('tarifa');
            $table->Integer('nivel');
            $table->Integer('ejecutivos');
            $table->Integer('programadelealtad');
            $table->mediumInteger('numeropl');
            $table->string('pasatiempo');
            $table->string('asistente');
            $table->string('felicitacion');
            $table->string('tipofe');
            $table->tinyInteger('status');

            $table->timestamps();
        });

         Schema::table('contactos', function($table) {
             $table->foreign('empresa_id')->references('id')->on('empresas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactos');
    }
}
