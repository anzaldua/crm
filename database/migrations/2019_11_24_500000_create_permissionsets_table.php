<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissionsets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',32);
            $table->string('description')->nullable();
            $table->string('perfil_id')->nullable();
            $table->tinyInteger('active')->nullable()->default(1);//1=Activo, 0=Inactivo

            //Audit fields
            
            $table->integer('owner_id')->nullable();//Propietario del registro
            $table->dateTime('last_viewed_date')->nullable();//Ultima fecha de haber sido consultado
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissionsets');
    }
}
