<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel', function (Blueprint $table) {
            $table->bigIncrements('id');
           $table->string('nombrehotel');
            $table->string(' foto')->nullable();           
            $table->string('codigohotel');          
            $table->string('nombrecomercial')->nullable();          
            $table->string('direccioncomercial');    
            $table->string('colonia');    
            $table->mediumInteger('cp');    
            $table->integer('idciudad')->nullable();    
            $table->integer('idestado')->nullable();
            $table->integer('idpais')->nullable();
            $table->string('razonsocial');
            $table->string('calleynumero');
            $table->integer('idciudadfiscal')->nullable();
            $table->integer('idestadofiscal')->nullable();
            $table->integer('idpaisfiscal')->nullable();
            $table->string('telefonodirecto');
            $table->string('telefono2');     
            $table->string('telefonocelular')->nullable();             
            $table->string('fax')->nullable();
            $table->string('web');
            $table->tinyInteger('idmarca')->nullable();                           
            $table->tinyInteger('idgrupohotelero')->nullable();                           
            $table->string('logohotel')->nullable();
            $table->string('principalcontacto');                           
            $table->tinyInteger('idpuesto')->nullable();                      
            $table->string('notas');                                 
            $table->string('nodehabitaciones')->nullable();                                 
            $table->double('impuesto')->nullable();                                 
            $table->double('ish')->nullable();                                 
            $table->tinyInteger('status');                                 
            $table->string('coloniafiscal')->nullable();                                                               
            $table->string('rfc');                                 
            $table->double('latitud')->nullable();                                                               
            $table->double('longitud')->nullable();
            $table->double('zoom')->nullable();
            $table->string('logoconvenios')->nullable();
            $table->Integer('idcorporativo')->nullable(); 
            $table->date('incContrato');                    
            $table->date('finContrato');                    
            $table->date('registrado')->nullable();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel');
    }
}
