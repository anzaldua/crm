<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionesCiudadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones_ciudades', function (Blueprint $table) {
            $table->bigIncrements('iddirecciones_ciudades');
            $table->unsignedBigInteger('iddirecciones_estados');
            $table->integer("numerociudad_estados");
            $table->string('nombre');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones_ciudades');
    }
}
