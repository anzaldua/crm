<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_hotel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
;
                        $table->string('paterno'); 
                        $table->string('materno'); 
                        $table->string('email'); 
                        $table->string('confirmacorreo'); 
                        $table->string('telefonoficina'); 
                        $table->string('ext'); 
                        $table->string('hotel'); 
                        $table->string('id_perfil')->nullable(); 
                        $table->string('departamento'); 
                        $table->string('puesto');         
                        $table->string('status'); 
                        $table->string('usuario');       
                        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_hotel');
    }
}
