<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionesColoniasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direcciones_colonias', function (Blueprint $table) {
            $table->bigIncrements('iddirecciones_colonias');
            $table->integer('iddirecciones_estados');
            $table->integer('numerociudad_estados');
            $table->integer('codigoPostal');
            $table->string('nombre');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direcciones_colonias');
    }
}
