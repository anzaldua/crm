<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGirosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('giros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('segmento_id')->unsigned()->nullable();
            $table->String('nombre');
            $table->Integer('hotel_id')->nullable();
            $table->timestamps();
        });

         Schema::table('giros', function($table) {
            $table->foreign('segmento_id')->references('id')->on('segmentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('giros');
    }
}
