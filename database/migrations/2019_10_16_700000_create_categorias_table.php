<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('categorias', function (Blueprint $table) {
            $table->bigIncrements('categoria_id');
            $table->string('nombre_categoria');
            $table->string('descripcion');
            $table->string('tipo');
            $table->string('icono');
            $table->tinyInteger('status')->nullable()->default('1');//1=Activo, 0=Inactivo
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
