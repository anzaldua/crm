<?php

use Illuminate\Database\Seeder;
use App\Perfil;
use App\User;
class PerfilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perfil= new Perfil();
        $perfil->nombre ="administrador general";
        $perfil->descripcion ="administrador";
        $perfil->save(); 

        $perfil = new Perfil();
        $perfil->nombre ="usuario";
        $perfil->descripcion="usuario";
        $perfil->save(); 

        $perfil = new perfil();
        $perfil->nombre ="gerente de cuenta";
        $perfil->descripcion="gerente de ventas";
        $perfil->save();

        $perfil = new perfil();
        $perfil->nombre="supervisor";
        $perfil->descripcion="supervisor del area";
        $perfil->save();

        $perfil = new Perfil();
        $perfil->nombre="ejecutivo de ventas";
        $perfil->descripcion="encargado de ventas";
        $perfil->save();
    }
}
