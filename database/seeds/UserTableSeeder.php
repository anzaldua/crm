<?php

use Illuminate\Database\Seeder;
use App\Perfil;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Ejecute las semillas de la base de datos.
     *
     * @return void
     */
    public function run()
    {
    //con esto le indicamos que se traega el nombre de usuarios y administrador

      //consulta mysql tabla usuarios perfil

    $perfilId =DB::table('usuarios')
        ->where('nombre','usuario')
        ->value('id');

       $perfil_user = Perfil::where('nombre','usuario')->first();
       $perfil_admin =Perfil::where('nombre','administrador general')->first();

       $user = new User();
       $user->nombre ="usuario";
       $user->email="alejandro.anzaldua@enlaceforte.com";
       $user->password = bcrypt('query');
  //guardamos
       $user->save();
  //asociamos de muchos a muchos con attach y asiganos el perfil user
       $user->perfiles()->attach($perfil_user);


       $user = new User();
       $user->nombre ="Administador general";
       $user->email="alejandro.lozano@enlaceforte.com";
       $user->password = bcrypt('query');
  //guardamos
       $user->save();
  //asociamos de muchos a muchos con attach y asiganos el perfil user
       $user->perfiles()->attach($perfil_admin);


    }
}
